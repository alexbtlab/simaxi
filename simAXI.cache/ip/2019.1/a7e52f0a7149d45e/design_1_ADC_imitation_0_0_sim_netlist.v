// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Jun  3 22:14:37 2020
// Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.v
// Design      : design_1_ADC_imitation_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
   (data_adc_deserial,
    clk_5MHz,
    s00_axi_aresetn);
  output [31:0]data_adc_deserial;
  input clk_5MHz;
  input s00_axi_aresetn;

  wire clk_5MHz;
  wire \count_clk[0]_i_1_n_0 ;
  wire \count_clk[31]_i_10_n_0 ;
  wire \count_clk[31]_i_11_n_0 ;
  wire \count_clk[31]_i_1_n_0 ;
  wire \count_clk[31]_i_3_n_0 ;
  wire \count_clk[31]_i_4_n_0 ;
  wire \count_clk[31]_i_5_n_0 ;
  wire \count_clk[31]_i_6_n_0 ;
  wire \count_clk[31]_i_7_n_0 ;
  wire \count_clk[31]_i_8_n_0 ;
  wire \count_clk[31]_i_9_n_0 ;
  wire \count_clk_reg[12]_i_1_n_0 ;
  wire \count_clk_reg[12]_i_1_n_1 ;
  wire \count_clk_reg[12]_i_1_n_2 ;
  wire \count_clk_reg[12]_i_1_n_3 ;
  wire \count_clk_reg[16]_i_1_n_0 ;
  wire \count_clk_reg[16]_i_1_n_1 ;
  wire \count_clk_reg[16]_i_1_n_2 ;
  wire \count_clk_reg[16]_i_1_n_3 ;
  wire \count_clk_reg[20]_i_1_n_0 ;
  wire \count_clk_reg[20]_i_1_n_1 ;
  wire \count_clk_reg[20]_i_1_n_2 ;
  wire \count_clk_reg[20]_i_1_n_3 ;
  wire \count_clk_reg[24]_i_1_n_0 ;
  wire \count_clk_reg[24]_i_1_n_1 ;
  wire \count_clk_reg[24]_i_1_n_2 ;
  wire \count_clk_reg[24]_i_1_n_3 ;
  wire \count_clk_reg[28]_i_1_n_0 ;
  wire \count_clk_reg[28]_i_1_n_1 ;
  wire \count_clk_reg[28]_i_1_n_2 ;
  wire \count_clk_reg[28]_i_1_n_3 ;
  wire \count_clk_reg[31]_i_2_n_2 ;
  wire \count_clk_reg[31]_i_2_n_3 ;
  wire \count_clk_reg[4]_i_1_n_0 ;
  wire \count_clk_reg[4]_i_1_n_1 ;
  wire \count_clk_reg[4]_i_1_n_2 ;
  wire \count_clk_reg[4]_i_1_n_3 ;
  wire \count_clk_reg[8]_i_1_n_0 ;
  wire \count_clk_reg[8]_i_1_n_1 ;
  wire \count_clk_reg[8]_i_1_n_2 ;
  wire \count_clk_reg[8]_i_1_n_3 ;
  wire [31:1]data0;
  wire [31:0]data_adc_deserial;
  wire s00_axi_aresetn;
  wire [3:2]\NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_count_clk_reg[31]_i_2_O_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_clk[0]_i_1 
       (.I0(data_adc_deserial[0]),
        .O(\count_clk[0]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'h0001)) 
    \count_clk[31]_i_1 
       (.I0(\count_clk[31]_i_4_n_0 ),
        .I1(\count_clk[31]_i_5_n_0 ),
        .I2(\count_clk[31]_i_6_n_0 ),
        .I3(\count_clk[31]_i_7_n_0 ),
        .O(\count_clk[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count_clk[31]_i_10 
       (.I0(data_adc_deserial[28]),
        .I1(data_adc_deserial[27]),
        .I2(data_adc_deserial[31]),
        .I3(data_adc_deserial[29]),
        .O(\count_clk[31]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count_clk[31]_i_11 
       (.I0(data_adc_deserial[10]),
        .I1(data_adc_deserial[11]),
        .I2(data_adc_deserial[8]),
        .I3(data_adc_deserial[9]),
        .O(\count_clk[31]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \count_clk[31]_i_3 
       (.I0(clk_5MHz),
        .I1(s00_axi_aresetn),
        .O(\count_clk[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \count_clk[31]_i_4 
       (.I0(data_adc_deserial[21]),
        .I1(data_adc_deserial[20]),
        .I2(data_adc_deserial[23]),
        .I3(data_adc_deserial[22]),
        .I4(\count_clk[31]_i_8_n_0 ),
        .O(\count_clk[31]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \count_clk[31]_i_5 
       (.I0(data_adc_deserial[1]),
        .I1(data_adc_deserial[2]),
        .I2(data_adc_deserial[4]),
        .I3(data_adc_deserial[0]),
        .I4(\count_clk[31]_i_9_n_0 ),
        .O(\count_clk[31]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \count_clk[31]_i_6 
       (.I0(data_adc_deserial[5]),
        .I1(data_adc_deserial[26]),
        .I2(data_adc_deserial[7]),
        .I3(data_adc_deserial[6]),
        .I4(\count_clk[31]_i_10_n_0 ),
        .O(\count_clk[31]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \count_clk[31]_i_7 
       (.I0(data_adc_deserial[13]),
        .I1(data_adc_deserial[12]),
        .I2(data_adc_deserial[15]),
        .I3(data_adc_deserial[14]),
        .I4(\count_clk[31]_i_11_n_0 ),
        .O(\count_clk[31]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count_clk[31]_i_8 
       (.I0(data_adc_deserial[18]),
        .I1(data_adc_deserial[19]),
        .I2(data_adc_deserial[16]),
        .I3(data_adc_deserial[17]),
        .O(\count_clk[31]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \count_clk[31]_i_9 
       (.I0(data_adc_deserial[3]),
        .I1(data_adc_deserial[30]),
        .I2(data_adc_deserial[24]),
        .I3(data_adc_deserial[25]),
        .O(\count_clk[31]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[0] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(\count_clk[0]_i_1_n_0 ),
        .Q(data_adc_deserial[0]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[10] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[10]),
        .Q(data_adc_deserial[10]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[11] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[11]),
        .Q(data_adc_deserial[11]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[12] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[12]),
        .Q(data_adc_deserial[12]),
        .R(\count_clk[31]_i_1_n_0 ));
  CARRY4 \count_clk_reg[12]_i_1 
       (.CI(\count_clk_reg[8]_i_1_n_0 ),
        .CO({\count_clk_reg[12]_i_1_n_0 ,\count_clk_reg[12]_i_1_n_1 ,\count_clk_reg[12]_i_1_n_2 ,\count_clk_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(data_adc_deserial[12:9]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[13] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[13]),
        .Q(data_adc_deserial[13]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[14] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[14]),
        .Q(data_adc_deserial[14]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[15] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[15]),
        .Q(data_adc_deserial[15]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[16] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[16]),
        .Q(data_adc_deserial[16]),
        .R(\count_clk[31]_i_1_n_0 ));
  CARRY4 \count_clk_reg[16]_i_1 
       (.CI(\count_clk_reg[12]_i_1_n_0 ),
        .CO({\count_clk_reg[16]_i_1_n_0 ,\count_clk_reg[16]_i_1_n_1 ,\count_clk_reg[16]_i_1_n_2 ,\count_clk_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[16:13]),
        .S(data_adc_deserial[16:13]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[17] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[17]),
        .Q(data_adc_deserial[17]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[18] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[18]),
        .Q(data_adc_deserial[18]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[19] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[19]),
        .Q(data_adc_deserial[19]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[1] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[1]),
        .Q(data_adc_deserial[1]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[20] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[20]),
        .Q(data_adc_deserial[20]),
        .R(\count_clk[31]_i_1_n_0 ));
  CARRY4 \count_clk_reg[20]_i_1 
       (.CI(\count_clk_reg[16]_i_1_n_0 ),
        .CO({\count_clk_reg[20]_i_1_n_0 ,\count_clk_reg[20]_i_1_n_1 ,\count_clk_reg[20]_i_1_n_2 ,\count_clk_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[20:17]),
        .S(data_adc_deserial[20:17]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[21] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[21]),
        .Q(data_adc_deserial[21]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[22] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[22]),
        .Q(data_adc_deserial[22]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[23] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[23]),
        .Q(data_adc_deserial[23]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[24] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[24]),
        .Q(data_adc_deserial[24]),
        .R(\count_clk[31]_i_1_n_0 ));
  CARRY4 \count_clk_reg[24]_i_1 
       (.CI(\count_clk_reg[20]_i_1_n_0 ),
        .CO({\count_clk_reg[24]_i_1_n_0 ,\count_clk_reg[24]_i_1_n_1 ,\count_clk_reg[24]_i_1_n_2 ,\count_clk_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[24:21]),
        .S(data_adc_deserial[24:21]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[25] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[25]),
        .Q(data_adc_deserial[25]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[26] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[26]),
        .Q(data_adc_deserial[26]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[27] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[27]),
        .Q(data_adc_deserial[27]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[28] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[28]),
        .Q(data_adc_deserial[28]),
        .R(\count_clk[31]_i_1_n_0 ));
  CARRY4 \count_clk_reg[28]_i_1 
       (.CI(\count_clk_reg[24]_i_1_n_0 ),
        .CO({\count_clk_reg[28]_i_1_n_0 ,\count_clk_reg[28]_i_1_n_1 ,\count_clk_reg[28]_i_1_n_2 ,\count_clk_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[28:25]),
        .S(data_adc_deserial[28:25]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[29] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[29]),
        .Q(data_adc_deserial[29]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[2] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[2]),
        .Q(data_adc_deserial[2]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[30] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[30]),
        .Q(data_adc_deserial[30]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[31] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[31]),
        .Q(data_adc_deserial[31]),
        .R(\count_clk[31]_i_1_n_0 ));
  CARRY4 \count_clk_reg[31]_i_2 
       (.CI(\count_clk_reg[28]_i_1_n_0 ),
        .CO({\NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED [3:2],\count_clk_reg[31]_i_2_n_2 ,\count_clk_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_count_clk_reg[31]_i_2_O_UNCONNECTED [3],data0[31:29]}),
        .S({1'b0,data_adc_deserial[31:29]}));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[3] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[3]),
        .Q(data_adc_deserial[3]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[4] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[4]),
        .Q(data_adc_deserial[4]),
        .R(\count_clk[31]_i_1_n_0 ));
  CARRY4 \count_clk_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\count_clk_reg[4]_i_1_n_0 ,\count_clk_reg[4]_i_1_n_1 ,\count_clk_reg[4]_i_1_n_2 ,\count_clk_reg[4]_i_1_n_3 }),
        .CYINIT(data_adc_deserial[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(data_adc_deserial[4:1]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[5] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[5]),
        .Q(data_adc_deserial[5]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[6] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[6]),
        .Q(data_adc_deserial[6]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[7] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[7]),
        .Q(data_adc_deserial[7]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[8] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[8]),
        .Q(data_adc_deserial[8]),
        .R(\count_clk[31]_i_1_n_0 ));
  CARRY4 \count_clk_reg[8]_i_1 
       (.CI(\count_clk_reg[4]_i_1_n_0 ),
        .CO({\count_clk_reg[8]_i_1_n_0 ,\count_clk_reg[8]_i_1_n_1 ,\count_clk_reg[8]_i_1_n_2 ,\count_clk_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(data_adc_deserial[8:5]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[9] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[9]),
        .Q(data_adc_deserial[9]),
        .R(\count_clk[31]_i_1_n_0 ));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC_imitation_0_0,ADC_imitation,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ADC_imitation,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_5MHz,
    s00_axi_aresetn,
    adr_bram,
    data_adc_deserial);
  input clk_5MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  output [31:0]adr_bram;
  output [31:0]data_adc_deserial;

  wire clk_5MHz;
  wire [31:0]data_adc_deserial;
  wire s00_axi_aresetn;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation inst
       (.clk_5MHz(clk_5MHz),
        .data_adc_deserial(data_adc_deserial),
        .s00_axi_aresetn(s00_axi_aresetn));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
