-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jun  7 14:06:20 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_sampleGenerator_0_0_sim_netlist.vhdl
-- Design      : design_1_sampleGenerator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  port (
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tvalid : out STD_LOGIC;
    en_AXIclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    main_clk_100MHz : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  signal \M_AXIS_TDATA[31]_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_1\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_2\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_3\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__1_n_2\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__1_n_3\ : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_1_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_2_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_3_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_4_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_1 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_2 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_3 : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_i_5_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_i_6_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_i_7_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_i_8_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_n_1\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_n_2\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__0_n_3\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__1_n_1\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__1_n_2\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__1_n_3\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__2_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__2_n_1\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__2_n_2\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg0_carry__2_n_3\ : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_i_1_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_i_2_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_i_3_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_i_4_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_i_5_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_i_6_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_i_7_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_i_8_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_n_1 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_n_2 : STD_LOGIC;
  signal M_AXIS_TVALID_reg0_carry_n_3 : STD_LOGIC;
  signal M_AXIS_TVALID_reg1 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \M_AXIS_TVALID_reg1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__0_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__0_n_1\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__0_n_2\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__0_n_3\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__1_n_1\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__1_n_2\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__1_n_3\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__2_n_2\ : STD_LOGIC;
  signal \M_AXIS_TVALID_reg1_carry__2_n_3\ : STD_LOGIC;
  signal M_AXIS_TVALID_reg1_carry_i_1_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg1_carry_i_2_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg1_carry_i_3_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg1_carry_i_4_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg1_carry_n_0 : STD_LOGIC;
  signal M_AXIS_TVALID_reg1_carry_n_1 : STD_LOGIC;
  signal M_AXIS_TVALID_reg1_carry_n_2 : STD_LOGIC;
  signal M_AXIS_TVALID_reg1_carry_n_3 : STD_LOGIC;
  signal M_AXIS_TVALID_reg_i_1_n_0 : STD_LOGIC;
  signal \^m00_axis_tlast\ : STD_LOGIC;
  signal \^m00_axis_tvalid\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal \p_0_in__0\ : STD_LOGIC;
  signal \wordCounter[7]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[7]_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter[7]_i_4_n_0\ : STD_LOGIC;
  signal wordCounter_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal NLW_M_AXIS_TLAST_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TLAST_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_M_AXIS_TLAST_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal NLW_M_AXIS_TVALID_reg0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TVALID_reg0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TVALID_reg0_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TVALID_reg0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TVALID_reg1_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_M_AXIS_TVALID_reg1_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \wordCounter[1]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \wordCounter[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \wordCounter[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \wordCounter[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \wordCounter[6]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \wordCounter[7]_i_3\ : label is "soft_lutpair1";
begin
  m00_axis_tlast <= \^m00_axis_tlast\;
  m00_axis_tvalid <= \^m00_axis_tvalid\;
\M_AXIS_TDATA[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => en_AXIclk,
      O => \M_AXIS_TDATA[31]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(0),
      Q => m00_axis_tdata(0),
      R => '0'
    );
\M_AXIS_TDATA_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(10),
      Q => m00_axis_tdata(10),
      R => '0'
    );
\M_AXIS_TDATA_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(11),
      Q => m00_axis_tdata(11),
      R => '0'
    );
\M_AXIS_TDATA_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(12),
      Q => m00_axis_tdata(12),
      R => '0'
    );
\M_AXIS_TDATA_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(13),
      Q => m00_axis_tdata(13),
      R => '0'
    );
\M_AXIS_TDATA_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(14),
      Q => m00_axis_tdata(14),
      R => '0'
    );
\M_AXIS_TDATA_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(15),
      Q => m00_axis_tdata(15),
      R => '0'
    );
\M_AXIS_TDATA_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(16),
      Q => m00_axis_tdata(16),
      R => '0'
    );
\M_AXIS_TDATA_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(17),
      Q => m00_axis_tdata(17),
      R => '0'
    );
\M_AXIS_TDATA_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(18),
      Q => m00_axis_tdata(18),
      R => '0'
    );
\M_AXIS_TDATA_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(19),
      Q => m00_axis_tdata(19),
      R => '0'
    );
\M_AXIS_TDATA_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(1),
      Q => m00_axis_tdata(1),
      R => '0'
    );
\M_AXIS_TDATA_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(20),
      Q => m00_axis_tdata(20),
      R => '0'
    );
\M_AXIS_TDATA_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(21),
      Q => m00_axis_tdata(21),
      R => '0'
    );
\M_AXIS_TDATA_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(22),
      Q => m00_axis_tdata(22),
      R => '0'
    );
\M_AXIS_TDATA_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(23),
      Q => m00_axis_tdata(23),
      R => '0'
    );
\M_AXIS_TDATA_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(24),
      Q => m00_axis_tdata(24),
      R => '0'
    );
\M_AXIS_TDATA_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(25),
      Q => m00_axis_tdata(25),
      R => '0'
    );
\M_AXIS_TDATA_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(26),
      Q => m00_axis_tdata(26),
      R => '0'
    );
\M_AXIS_TDATA_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(27),
      Q => m00_axis_tdata(27),
      R => '0'
    );
\M_AXIS_TDATA_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(28),
      Q => m00_axis_tdata(28),
      R => '0'
    );
\M_AXIS_TDATA_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(29),
      Q => m00_axis_tdata(29),
      R => '0'
    );
\M_AXIS_TDATA_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(2),
      Q => m00_axis_tdata(2),
      R => '0'
    );
\M_AXIS_TDATA_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(30),
      Q => m00_axis_tdata(30),
      R => '0'
    );
\M_AXIS_TDATA_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(31),
      Q => m00_axis_tdata(31),
      R => '0'
    );
\M_AXIS_TDATA_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(3),
      Q => m00_axis_tdata(3),
      R => '0'
    );
\M_AXIS_TDATA_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(4),
      Q => m00_axis_tdata(4),
      R => '0'
    );
\M_AXIS_TDATA_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(5),
      Q => m00_axis_tdata(5),
      R => '0'
    );
\M_AXIS_TDATA_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(6),
      Q => m00_axis_tdata(6),
      R => '0'
    );
\M_AXIS_TDATA_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(7),
      Q => m00_axis_tdata(7),
      R => '0'
    );
\M_AXIS_TDATA_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(8),
      Q => m00_axis_tdata(8),
      R => '0'
    );
\M_AXIS_TDATA_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(9),
      Q => m00_axis_tdata(9),
      R => '0'
    );
M_AXIS_TLAST_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => M_AXIS_TLAST_carry_n_0,
      CO(2) => M_AXIS_TLAST_carry_n_1,
      CO(1) => M_AXIS_TLAST_carry_n_2,
      CO(0) => M_AXIS_TLAST_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_M_AXIS_TLAST_carry_O_UNCONNECTED(3 downto 0),
      S(3) => M_AXIS_TLAST_carry_i_1_n_0,
      S(2) => M_AXIS_TLAST_carry_i_2_n_0,
      S(1) => M_AXIS_TLAST_carry_i_3_n_0,
      S(0) => M_AXIS_TLAST_carry_i_4_n_0
    );
\M_AXIS_TLAST_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => M_AXIS_TLAST_carry_n_0,
      CO(3) => \M_AXIS_TLAST_carry__0_n_0\,
      CO(2) => \M_AXIS_TLAST_carry__0_n_1\,
      CO(1) => \M_AXIS_TLAST_carry__0_n_2\,
      CO(0) => \M_AXIS_TLAST_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(2) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(1) => \M_AXIS_TLAST_carry__0_i_1_n_0\,
      S(0) => \M_AXIS_TLAST_carry__0_i_2_n_0\
    );
\M_AXIS_TLAST_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      I1 => M_AXIS_TVALID_reg1(15),
      O => \M_AXIS_TLAST_carry__0_i_1_n_0\
    );
\M_AXIS_TLAST_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(14),
      I1 => M_AXIS_TVALID_reg1(13),
      I2 => M_AXIS_TVALID_reg1(12),
      O => \M_AXIS_TLAST_carry__0_i_2_n_0\
    );
\M_AXIS_TLAST_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXIS_TLAST_carry__0_n_0\,
      CO(3) => \NLW_M_AXIS_TLAST_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \^m00_axis_tlast\,
      CO(1) => \M_AXIS_TLAST_carry__1_n_2\,
      CO(0) => \M_AXIS_TLAST_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_M_AXIS_TLAST_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(1) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(0) => \M_AXIS_TVALID_reg1_carry__2_n_0\
    );
M_AXIS_TLAST_carry_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(11),
      I1 => M_AXIS_TVALID_reg1(10),
      I2 => M_AXIS_TVALID_reg1(9),
      O => M_AXIS_TLAST_carry_i_1_n_0
    );
M_AXIS_TLAST_carry_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"09000009"
    )
        port map (
      I0 => wordCounter_reg(6),
      I1 => M_AXIS_TVALID_reg1(6),
      I2 => M_AXIS_TVALID_reg1(8),
      I3 => M_AXIS_TVALID_reg1(7),
      I4 => wordCounter_reg(7),
      O => M_AXIS_TLAST_carry_i_2_n_0
    );
M_AXIS_TLAST_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter_reg(3),
      I1 => M_AXIS_TVALID_reg1(3),
      I2 => M_AXIS_TVALID_reg1(5),
      I3 => wordCounter_reg(5),
      I4 => M_AXIS_TVALID_reg1(4),
      I5 => wordCounter_reg(4),
      O => M_AXIS_TLAST_carry_i_3_n_0
    );
M_AXIS_TLAST_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => wordCounter_reg(0),
      I1 => FrameSize(0),
      I2 => M_AXIS_TVALID_reg1(2),
      I3 => wordCounter_reg(2),
      I4 => M_AXIS_TVALID_reg1(1),
      I5 => wordCounter_reg(1),
      O => M_AXIS_TLAST_carry_i_4_n_0
    );
M_AXIS_TVALID_reg0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => M_AXIS_TVALID_reg0_carry_n_0,
      CO(2) => M_AXIS_TVALID_reg0_carry_n_1,
      CO(1) => M_AXIS_TVALID_reg0_carry_n_2,
      CO(0) => M_AXIS_TVALID_reg0_carry_n_3,
      CYINIT => '1',
      DI(3) => M_AXIS_TVALID_reg0_carry_i_1_n_0,
      DI(2) => M_AXIS_TVALID_reg0_carry_i_2_n_0,
      DI(1) => M_AXIS_TVALID_reg0_carry_i_3_n_0,
      DI(0) => M_AXIS_TVALID_reg0_carry_i_4_n_0,
      O(3 downto 0) => NLW_M_AXIS_TVALID_reg0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => M_AXIS_TVALID_reg0_carry_i_5_n_0,
      S(2) => M_AXIS_TVALID_reg0_carry_i_6_n_0,
      S(1) => M_AXIS_TVALID_reg0_carry_i_7_n_0,
      S(0) => M_AXIS_TVALID_reg0_carry_i_8_n_0
    );
\M_AXIS_TVALID_reg0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => M_AXIS_TVALID_reg0_carry_n_0,
      CO(3) => \M_AXIS_TVALID_reg0_carry__0_n_0\,
      CO(2) => \M_AXIS_TVALID_reg0_carry__0_n_1\,
      CO(1) => \M_AXIS_TVALID_reg0_carry__0_n_2\,
      CO(0) => \M_AXIS_TVALID_reg0_carry__0_n_3\,
      CYINIT => '0',
      DI(3) => \M_AXIS_TVALID_reg0_carry__0_i_1_n_0\,
      DI(2) => \M_AXIS_TVALID_reg0_carry__0_i_2_n_0\,
      DI(1) => \M_AXIS_TVALID_reg0_carry__0_i_3_n_0\,
      DI(0) => \M_AXIS_TVALID_reg0_carry__0_i_4_n_0\,
      O(3 downto 0) => \NLW_M_AXIS_TVALID_reg0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \M_AXIS_TVALID_reg0_carry__0_i_5_n_0\,
      S(2) => \M_AXIS_TVALID_reg0_carry__0_i_6_n_0\,
      S(1) => \M_AXIS_TVALID_reg0_carry__0_i_7_n_0\,
      S(0) => \M_AXIS_TVALID_reg0_carry__0_i_8_n_0\
    );
\M_AXIS_TVALID_reg0_carry__0_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(14),
      I1 => M_AXIS_TVALID_reg1(15),
      O => \M_AXIS_TVALID_reg0_carry__0_i_1_n_0\
    );
\M_AXIS_TVALID_reg0_carry__0_i_2\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(12),
      I1 => M_AXIS_TVALID_reg1(13),
      O => \M_AXIS_TVALID_reg0_carry__0_i_2_n_0\
    );
\M_AXIS_TVALID_reg0_carry__0_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(10),
      I1 => M_AXIS_TVALID_reg1(11),
      O => \M_AXIS_TVALID_reg0_carry__0_i_3_n_0\
    );
\M_AXIS_TVALID_reg0_carry__0_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(8),
      I1 => M_AXIS_TVALID_reg1(9),
      O => \M_AXIS_TVALID_reg0_carry__0_i_4_n_0\
    );
\M_AXIS_TVALID_reg0_carry__0_i_5\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(14),
      I1 => M_AXIS_TVALID_reg1(15),
      O => \M_AXIS_TVALID_reg0_carry__0_i_5_n_0\
    );
\M_AXIS_TVALID_reg0_carry__0_i_6\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(12),
      I1 => M_AXIS_TVALID_reg1(13),
      O => \M_AXIS_TVALID_reg0_carry__0_i_6_n_0\
    );
\M_AXIS_TVALID_reg0_carry__0_i_7\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(10),
      I1 => M_AXIS_TVALID_reg1(11),
      O => \M_AXIS_TVALID_reg0_carry__0_i_7_n_0\
    );
\M_AXIS_TVALID_reg0_carry__0_i_8\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(8),
      I1 => M_AXIS_TVALID_reg1(9),
      O => \M_AXIS_TVALID_reg0_carry__0_i_8_n_0\
    );
\M_AXIS_TVALID_reg0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXIS_TVALID_reg0_carry__0_n_0\,
      CO(3) => \M_AXIS_TVALID_reg0_carry__1_n_0\,
      CO(2) => \M_AXIS_TVALID_reg0_carry__1_n_1\,
      CO(1) => \M_AXIS_TVALID_reg0_carry__1_n_2\,
      CO(0) => \M_AXIS_TVALID_reg0_carry__1_n_3\,
      CYINIT => '0',
      DI(3) => \M_AXIS_TVALID_reg0_carry__1_i_1_n_0\,
      DI(2) => \M_AXIS_TVALID_reg0_carry__1_i_2_n_0\,
      DI(1) => \M_AXIS_TVALID_reg0_carry__1_i_3_n_0\,
      DI(0) => \M_AXIS_TVALID_reg0_carry__1_i_4_n_0\,
      O(3 downto 0) => \NLW_M_AXIS_TVALID_reg0_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(2) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(1) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(0) => \M_AXIS_TVALID_reg1_carry__2_n_0\
    );
\M_AXIS_TVALID_reg0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      O => \M_AXIS_TVALID_reg0_carry__1_i_1_n_0\
    );
\M_AXIS_TVALID_reg0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      O => \M_AXIS_TVALID_reg0_carry__1_i_2_n_0\
    );
\M_AXIS_TVALID_reg0_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      O => \M_AXIS_TVALID_reg0_carry__1_i_3_n_0\
    );
\M_AXIS_TVALID_reg0_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      O => \M_AXIS_TVALID_reg0_carry__1_i_4_n_0\
    );
\M_AXIS_TVALID_reg0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXIS_TVALID_reg0_carry__1_n_0\,
      CO(3) => \p_0_in__0\,
      CO(2) => \M_AXIS_TVALID_reg0_carry__2_n_1\,
      CO(1) => \M_AXIS_TVALID_reg0_carry__2_n_2\,
      CO(0) => \M_AXIS_TVALID_reg0_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => M_AXIS_TVALID_reg1(31),
      DI(2) => \M_AXIS_TVALID_reg0_carry__2_i_2_n_0\,
      DI(1) => \M_AXIS_TVALID_reg0_carry__2_i_3_n_0\,
      DI(0) => \M_AXIS_TVALID_reg0_carry__2_i_4_n_0\,
      O(3 downto 0) => \NLW_M_AXIS_TVALID_reg0_carry__2_O_UNCONNECTED\(3 downto 0),
      S(3) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(2) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(1) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      S(0) => \M_AXIS_TVALID_reg1_carry__2_n_0\
    );
\M_AXIS_TVALID_reg0_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      O => M_AXIS_TVALID_reg1(31)
    );
\M_AXIS_TVALID_reg0_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      O => \M_AXIS_TVALID_reg0_carry__2_i_2_n_0\
    );
\M_AXIS_TVALID_reg0_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      O => \M_AXIS_TVALID_reg0_carry__2_i_3_n_0\
    );
\M_AXIS_TVALID_reg0_carry__2_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      O => \M_AXIS_TVALID_reg0_carry__2_i_4_n_0\
    );
M_AXIS_TVALID_reg0_carry_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(6),
      I1 => wordCounter_reg(6),
      I2 => wordCounter_reg(7),
      I3 => M_AXIS_TVALID_reg1(7),
      O => M_AXIS_TVALID_reg0_carry_i_1_n_0
    );
M_AXIS_TVALID_reg0_carry_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(4),
      I1 => wordCounter_reg(4),
      I2 => wordCounter_reg(5),
      I3 => M_AXIS_TVALID_reg1(5),
      O => M_AXIS_TVALID_reg0_carry_i_2_n_0
    );
M_AXIS_TVALID_reg0_carry_i_3: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F02"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(2),
      I1 => wordCounter_reg(2),
      I2 => wordCounter_reg(3),
      I3 => M_AXIS_TVALID_reg1(3),
      O => M_AXIS_TVALID_reg0_carry_i_3_n_0
    );
M_AXIS_TVALID_reg0_carry_i_4: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1F01"
    )
        port map (
      I0 => wordCounter_reg(0),
      I1 => FrameSize(0),
      I2 => wordCounter_reg(1),
      I3 => M_AXIS_TVALID_reg1(1),
      O => M_AXIS_TVALID_reg0_carry_i_4_n_0
    );
M_AXIS_TVALID_reg0_carry_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(6),
      I1 => wordCounter_reg(6),
      I2 => M_AXIS_TVALID_reg1(7),
      I3 => wordCounter_reg(7),
      O => M_AXIS_TVALID_reg0_carry_i_5_n_0
    );
M_AXIS_TVALID_reg0_carry_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(4),
      I1 => wordCounter_reg(4),
      I2 => M_AXIS_TVALID_reg1(5),
      I3 => wordCounter_reg(5),
      O => M_AXIS_TVALID_reg0_carry_i_6_n_0
    );
M_AXIS_TVALID_reg0_carry_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"9009"
    )
        port map (
      I0 => M_AXIS_TVALID_reg1(2),
      I1 => wordCounter_reg(2),
      I2 => M_AXIS_TVALID_reg1(3),
      I3 => wordCounter_reg(3),
      O => M_AXIS_TVALID_reg0_carry_i_7_n_0
    );
M_AXIS_TVALID_reg0_carry_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6006"
    )
        port map (
      I0 => FrameSize(0),
      I1 => wordCounter_reg(0),
      I2 => M_AXIS_TVALID_reg1(1),
      I3 => wordCounter_reg(1),
      O => M_AXIS_TVALID_reg0_carry_i_8_n_0
    );
M_AXIS_TVALID_reg1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => M_AXIS_TVALID_reg1_carry_n_0,
      CO(2) => M_AXIS_TVALID_reg1_carry_n_1,
      CO(1) => M_AXIS_TVALID_reg1_carry_n_2,
      CO(0) => M_AXIS_TVALID_reg1_carry_n_3,
      CYINIT => FrameSize(0),
      DI(3 downto 0) => FrameSize(4 downto 1),
      O(3 downto 0) => M_AXIS_TVALID_reg1(4 downto 1),
      S(3) => M_AXIS_TVALID_reg1_carry_i_1_n_0,
      S(2) => M_AXIS_TVALID_reg1_carry_i_2_n_0,
      S(1) => M_AXIS_TVALID_reg1_carry_i_3_n_0,
      S(0) => M_AXIS_TVALID_reg1_carry_i_4_n_0
    );
\M_AXIS_TVALID_reg1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => M_AXIS_TVALID_reg1_carry_n_0,
      CO(3) => \M_AXIS_TVALID_reg1_carry__0_n_0\,
      CO(2) => \M_AXIS_TVALID_reg1_carry__0_n_1\,
      CO(1) => \M_AXIS_TVALID_reg1_carry__0_n_2\,
      CO(0) => \M_AXIS_TVALID_reg1_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => FrameSize(8 downto 5),
      O(3 downto 0) => M_AXIS_TVALID_reg1(8 downto 5),
      S(3) => \M_AXIS_TVALID_reg1_carry__0_i_1_n_0\,
      S(2) => \M_AXIS_TVALID_reg1_carry__0_i_2_n_0\,
      S(1) => \M_AXIS_TVALID_reg1_carry__0_i_3_n_0\,
      S(0) => \M_AXIS_TVALID_reg1_carry__0_i_4_n_0\
    );
\M_AXIS_TVALID_reg1_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(8),
      O => \M_AXIS_TVALID_reg1_carry__0_i_1_n_0\
    );
\M_AXIS_TVALID_reg1_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(7),
      O => \M_AXIS_TVALID_reg1_carry__0_i_2_n_0\
    );
\M_AXIS_TVALID_reg1_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(6),
      O => \M_AXIS_TVALID_reg1_carry__0_i_3_n_0\
    );
\M_AXIS_TVALID_reg1_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(5),
      O => \M_AXIS_TVALID_reg1_carry__0_i_4_n_0\
    );
\M_AXIS_TVALID_reg1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXIS_TVALID_reg1_carry__0_n_0\,
      CO(3) => \M_AXIS_TVALID_reg1_carry__1_n_0\,
      CO(2) => \M_AXIS_TVALID_reg1_carry__1_n_1\,
      CO(1) => \M_AXIS_TVALID_reg1_carry__1_n_2\,
      CO(0) => \M_AXIS_TVALID_reg1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => FrameSize(12 downto 9),
      O(3 downto 0) => M_AXIS_TVALID_reg1(12 downto 9),
      S(3) => \M_AXIS_TVALID_reg1_carry__1_i_1_n_0\,
      S(2) => \M_AXIS_TVALID_reg1_carry__1_i_2_n_0\,
      S(1) => \M_AXIS_TVALID_reg1_carry__1_i_3_n_0\,
      S(0) => \M_AXIS_TVALID_reg1_carry__1_i_4_n_0\
    );
\M_AXIS_TVALID_reg1_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(12),
      O => \M_AXIS_TVALID_reg1_carry__1_i_1_n_0\
    );
\M_AXIS_TVALID_reg1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(11),
      O => \M_AXIS_TVALID_reg1_carry__1_i_2_n_0\
    );
\M_AXIS_TVALID_reg1_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(10),
      O => \M_AXIS_TVALID_reg1_carry__1_i_3_n_0\
    );
\M_AXIS_TVALID_reg1_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(9),
      O => \M_AXIS_TVALID_reg1_carry__1_i_4_n_0\
    );
\M_AXIS_TVALID_reg1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXIS_TVALID_reg1_carry__1_n_0\,
      CO(3) => \M_AXIS_TVALID_reg1_carry__2_n_0\,
      CO(2) => \NLW_M_AXIS_TVALID_reg1_carry__2_CO_UNCONNECTED\(2),
      CO(1) => \M_AXIS_TVALID_reg1_carry__2_n_2\,
      CO(0) => \M_AXIS_TVALID_reg1_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => FrameSize(15 downto 13),
      O(3) => \NLW_M_AXIS_TVALID_reg1_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => M_AXIS_TVALID_reg1(15 downto 13),
      S(3) => '1',
      S(2) => \M_AXIS_TVALID_reg1_carry__2_i_1_n_0\,
      S(1) => \M_AXIS_TVALID_reg1_carry__2_i_2_n_0\,
      S(0) => \M_AXIS_TVALID_reg1_carry__2_i_3_n_0\
    );
\M_AXIS_TVALID_reg1_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(15),
      O => \M_AXIS_TVALID_reg1_carry__2_i_1_n_0\
    );
\M_AXIS_TVALID_reg1_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(14),
      O => \M_AXIS_TVALID_reg1_carry__2_i_2_n_0\
    );
\M_AXIS_TVALID_reg1_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(13),
      O => \M_AXIS_TVALID_reg1_carry__2_i_3_n_0\
    );
M_AXIS_TVALID_reg1_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(4),
      O => M_AXIS_TVALID_reg1_carry_i_1_n_0
    );
M_AXIS_TVALID_reg1_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(3),
      O => M_AXIS_TVALID_reg1_carry_i_2_n_0
    );
M_AXIS_TVALID_reg1_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(2),
      O => M_AXIS_TVALID_reg1_carry_i_3_n_0
    );
M_AXIS_TVALID_reg1_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(1),
      O => M_AXIS_TVALID_reg1_carry_i_4_n_0
    );
M_AXIS_TVALID_reg_i_1: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0CAA"
    )
        port map (
      I0 => \^m00_axis_tvalid\,
      I1 => \p_0_in__0\,
      I2 => en_AXIclk,
      I3 => m00_axis_aresetn,
      O => M_AXIS_TVALID_reg_i_1_n_0
    );
M_AXIS_TVALID_reg_reg: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => M_AXIS_TVALID_reg_i_1_n_0,
      Q => \^m00_axis_tvalid\,
      R => '0'
    );
\wordCounter[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wordCounter_reg(0),
      O => p_0_in(0)
    );
\wordCounter[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => wordCounter_reg(0),
      I1 => wordCounter_reg(1),
      O => p_0_in(1)
    );
\wordCounter[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => wordCounter_reg(0),
      I1 => wordCounter_reg(1),
      I2 => wordCounter_reg(2),
      O => p_0_in(2)
    );
\wordCounter[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => wordCounter_reg(1),
      I1 => wordCounter_reg(0),
      I2 => wordCounter_reg(2),
      I3 => wordCounter_reg(3),
      O => p_0_in(3)
    );
\wordCounter[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => wordCounter_reg(2),
      I1 => wordCounter_reg(0),
      I2 => wordCounter_reg(1),
      I3 => wordCounter_reg(3),
      I4 => wordCounter_reg(4),
      O => p_0_in(4)
    );
\wordCounter[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => wordCounter_reg(3),
      I1 => wordCounter_reg(1),
      I2 => wordCounter_reg(0),
      I3 => wordCounter_reg(2),
      I4 => wordCounter_reg(4),
      I5 => wordCounter_reg(5),
      O => p_0_in(5)
    );
\wordCounter[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \wordCounter[7]_i_4_n_0\,
      I1 => wordCounter_reg(6),
      O => p_0_in(6)
    );
\wordCounter[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => en_AXIclk,
      I1 => \^m00_axis_tlast\,
      I2 => m00_axis_aresetn,
      O => \wordCounter[7]_i_1_n_0\
    );
\wordCounter[7]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => en_AXIclk,
      O => \wordCounter[7]_i_2_n_0\
    );
\wordCounter[7]_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \wordCounter[7]_i_4_n_0\,
      I1 => wordCounter_reg(6),
      I2 => wordCounter_reg(7),
      O => p_0_in(7)
    );
\wordCounter[7]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => wordCounter_reg(5),
      I1 => wordCounter_reg(3),
      I2 => wordCounter_reg(1),
      I3 => wordCounter_reg(0),
      I4 => wordCounter_reg(2),
      I5 => wordCounter_reg(4),
      O => \wordCounter[7]_i_4_n_0\
    );
\wordCounter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[7]_i_2_n_0\,
      D => p_0_in(0),
      Q => wordCounter_reg(0),
      R => \wordCounter[7]_i_1_n_0\
    );
\wordCounter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[7]_i_2_n_0\,
      D => p_0_in(1),
      Q => wordCounter_reg(1),
      R => \wordCounter[7]_i_1_n_0\
    );
\wordCounter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[7]_i_2_n_0\,
      D => p_0_in(2),
      Q => wordCounter_reg(2),
      R => \wordCounter[7]_i_1_n_0\
    );
\wordCounter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[7]_i_2_n_0\,
      D => p_0_in(3),
      Q => wordCounter_reg(3),
      R => \wordCounter[7]_i_1_n_0\
    );
\wordCounter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[7]_i_2_n_0\,
      D => p_0_in(4),
      Q => wordCounter_reg(4),
      R => \wordCounter[7]_i_1_n_0\
    );
\wordCounter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[7]_i_2_n_0\,
      D => p_0_in(5),
      Q => wordCounter_reg(5),
      R => \wordCounter[7]_i_1_n_0\
    );
\wordCounter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[7]_i_2_n_0\,
      D => p_0_in(6),
      Q => wordCounter_reg(6),
      R => \wordCounter[7]_i_1_n_0\
    );
\wordCounter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \wordCounter[7]_i_2_n_0\,
      D => p_0_in(7),
      Q => wordCounter_reg(7),
      R => \wordCounter[7]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
  port (
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tvalid : out STD_LOGIC;
    en_AXIclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    main_clk_100MHz : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
begin
samplGeneraror_v1_0_M00_AXIS_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      data_from_ram(31 downto 0) => data_from_ram(31 downto 0),
      en_AXIclk => en_AXIclk,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tvalid => m00_axis_tvalid,
      main_clk_100MHz => main_clk_100MHz
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    en_AXIclk : in STD_LOGIC;
    main_clk_100MHz : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_sampleGenerator_0_0,sampleGenerator_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sampleGenerator_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      data_from_ram(31 downto 0) => data_from_ram(31 downto 0),
      en_AXIclk => en_AXIclk,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tvalid => m00_axis_tvalid,
      main_clk_100MHz => main_clk_100MHz
    );
end STRUCTURE;
