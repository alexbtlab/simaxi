-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jun  7 20:00:37 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_imitation_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  port (
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_100Mhz_AXI_send : out STD_LOGIC;
    write_reg_reg_0 : out STD_LOGIC;
    clk_5MHzWR_100MhzRD : out STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  signal FLAG_state : STD_LOGIC;
  signal FLAG_state_i_1_n_0 : STD_LOGIC;
  signal count_clk0 : STD_LOGIC;
  signal count_clk04_out : STD_LOGIC;
  signal \count_clk0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__0_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__0_n_1\ : STD_LOGIC;
  signal \count_clk0_carry__0_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__0_n_3\ : STD_LOGIC;
  signal \count_clk0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__1_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__1_n_3\ : STD_LOGIC;
  signal count_clk0_carry_i_1_n_0 : STD_LOGIC;
  signal count_clk0_carry_i_2_n_0 : STD_LOGIC;
  signal count_clk0_carry_i_3_n_0 : STD_LOGIC;
  signal count_clk0_carry_i_4_n_0 : STD_LOGIC;
  signal count_clk0_carry_n_0 : STD_LOGIC;
  signal count_clk0_carry_n_1 : STD_LOGIC;
  signal count_clk0_carry_n_2 : STD_LOGIC;
  signal count_clk0_carry_n_3 : STD_LOGIC;
  signal \count_clk[3]_i_2_n_0\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \^data\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal write_reg_i_1_n_0 : STD_LOGIC;
  signal \^write_reg_reg_0\ : STD_LOGIC;
  signal NLW_count_clk0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count_clk0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count_clk0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_count_clk0_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_count_clk_reg[31]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of FLAG_state_i_1 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of clk_100Mhz_AXI_send_INST_0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of clk_5MHzWR_100MhzRD_INST_0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of write_reg_i_1 : label is "soft_lutpair1";
begin
  data(31 downto 0) <= \^data\(31 downto 0);
  write_reg_reg_0 <= \^write_reg_reg_0\;
FLAG_state_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => count_clk0,
      I1 => FLAG_state,
      O => FLAG_state_i_1_n_0
    );
FLAG_state_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => FLAG_state_i_1_n_0,
      Q => FLAG_state,
      R => '0'
    );
clk_100Mhz_AXI_send_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^write_reg_reg_0\,
      I1 => clk_100MHz,
      O => clk_100Mhz_AXI_send
    );
clk_5MHzWR_100MhzRD_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => clk_5MHz,
      I1 => \^write_reg_reg_0\,
      I2 => clk_100MHz,
      O => clk_5MHzWR_100MhzRD
    );
count_clk0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count_clk0_carry_n_0,
      CO(2) => count_clk0_carry_n_1,
      CO(1) => count_clk0_carry_n_2,
      CO(0) => count_clk0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_count_clk0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => count_clk0_carry_i_1_n_0,
      S(2) => count_clk0_carry_i_2_n_0,
      S(1) => count_clk0_carry_i_3_n_0,
      S(0) => count_clk0_carry_i_4_n_0
    );
\count_clk0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count_clk0_carry_n_0,
      CO(3) => \count_clk0_carry__0_n_0\,
      CO(2) => \count_clk0_carry__0_n_1\,
      CO(1) => \count_clk0_carry__0_n_2\,
      CO(0) => \count_clk0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_count_clk0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \count_clk0_carry__0_i_1_n_0\,
      S(2) => \count_clk0_carry__0_i_2_n_0\,
      S(1) => \count_clk0_carry__0_i_3_n_0\,
      S(0) => \count_clk0_carry__0_i_4_n_0\
    );
\count_clk0_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^data\(23),
      I1 => \^data\(22),
      I2 => \^data\(21),
      O => \count_clk0_carry__0_i_1_n_0\
    );
\count_clk0_carry__0_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^data\(20),
      I1 => \^data\(19),
      I2 => \^data\(18),
      O => \count_clk0_carry__0_i_2_n_0\
    );
\count_clk0_carry__0_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0009"
    )
        port map (
      I0 => \^data\(15),
      I1 => FrameSize(15),
      I2 => \^data\(17),
      I3 => \^data\(16),
      O => \count_clk0_carry__0_i_3_n_0\
    );
\count_clk0_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^data\(12),
      I1 => FrameSize(12),
      I2 => FrameSize(14),
      I3 => \^data\(14),
      I4 => FrameSize(13),
      I5 => \^data\(13),
      O => \count_clk0_carry__0_i_4_n_0\
    );
\count_clk0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk0_carry__0_n_0\,
      CO(3) => \NLW_count_clk0_carry__1_CO_UNCONNECTED\(3),
      CO(2) => count_clk0,
      CO(1) => \count_clk0_carry__1_n_2\,
      CO(0) => \count_clk0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_count_clk0_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \count_clk0_carry__1_i_1_n_0\,
      S(1) => \count_clk0_carry__1_i_2_n_0\,
      S(0) => \count_clk0_carry__1_i_3_n_0\
    );
\count_clk0_carry__1_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data\(30),
      I1 => \^data\(31),
      O => \count_clk0_carry__1_i_1_n_0\
    );
\count_clk0_carry__1_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^data\(29),
      I1 => \^data\(28),
      I2 => \^data\(27),
      O => \count_clk0_carry__1_i_2_n_0\
    );
\count_clk0_carry__1_i_3\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \^data\(26),
      I1 => \^data\(25),
      I2 => \^data\(24),
      O => \count_clk0_carry__1_i_3_n_0\
    );
count_clk0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^data\(9),
      I1 => FrameSize(9),
      I2 => FrameSize(11),
      I3 => \^data\(11),
      I4 => FrameSize(10),
      I5 => \^data\(10),
      O => count_clk0_carry_i_1_n_0
    );
count_clk0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^data\(6),
      I1 => FrameSize(6),
      I2 => FrameSize(8),
      I3 => \^data\(8),
      I4 => FrameSize(7),
      I5 => \^data\(7),
      O => count_clk0_carry_i_2_n_0
    );
count_clk0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^data\(3),
      I1 => FrameSize(3),
      I2 => FrameSize(5),
      I3 => \^data\(5),
      I4 => FrameSize(4),
      I5 => \^data\(4),
      O => count_clk0_carry_i_3_n_0
    );
count_clk0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => \^data\(0),
      I1 => FrameSize(0),
      I2 => FrameSize(2),
      I3 => \^data\(2),
      I4 => FrameSize(1),
      I5 => \^data\(1),
      O => count_clk0_carry_i_4_n_0
    );
\count_clk[31]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E200"
    )
        port map (
      I0 => clk_100MHz,
      I1 => \^write_reg_reg_0\,
      I2 => clk_5MHz,
      I3 => s00_axi_aresetn,
      O => count_clk04_out
    );
\count_clk[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data\(0),
      O => \count_clk[3]_i_2_n_0\
    );
\count_clk_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[3]_i_1_n_7\,
      Q => \^data\(0),
      R => count_clk0
    );
\count_clk_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[11]_i_1_n_5\,
      Q => \^data\(10),
      R => count_clk0
    );
\count_clk_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[11]_i_1_n_4\,
      Q => \^data\(11),
      R => count_clk0
    );
\count_clk_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[7]_i_1_n_0\,
      CO(3) => \count_clk_reg[11]_i_1_n_0\,
      CO(2) => \count_clk_reg[11]_i_1_n_1\,
      CO(1) => \count_clk_reg[11]_i_1_n_2\,
      CO(0) => \count_clk_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk_reg[11]_i_1_n_4\,
      O(2) => \count_clk_reg[11]_i_1_n_5\,
      O(1) => \count_clk_reg[11]_i_1_n_6\,
      O(0) => \count_clk_reg[11]_i_1_n_7\,
      S(3 downto 0) => \^data\(11 downto 8)
    );
\count_clk_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[15]_i_1_n_7\,
      Q => \^data\(12),
      R => count_clk0
    );
\count_clk_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[15]_i_1_n_6\,
      Q => \^data\(13),
      R => count_clk0
    );
\count_clk_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[15]_i_1_n_5\,
      Q => \^data\(14),
      R => count_clk0
    );
\count_clk_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[15]_i_1_n_4\,
      Q => \^data\(15),
      R => count_clk0
    );
\count_clk_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[11]_i_1_n_0\,
      CO(3) => \count_clk_reg[15]_i_1_n_0\,
      CO(2) => \count_clk_reg[15]_i_1_n_1\,
      CO(1) => \count_clk_reg[15]_i_1_n_2\,
      CO(0) => \count_clk_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk_reg[15]_i_1_n_4\,
      O(2) => \count_clk_reg[15]_i_1_n_5\,
      O(1) => \count_clk_reg[15]_i_1_n_6\,
      O(0) => \count_clk_reg[15]_i_1_n_7\,
      S(3 downto 0) => \^data\(15 downto 12)
    );
\count_clk_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[19]_i_1_n_7\,
      Q => \^data\(16),
      R => count_clk0
    );
\count_clk_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[19]_i_1_n_6\,
      Q => \^data\(17),
      R => count_clk0
    );
\count_clk_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[19]_i_1_n_5\,
      Q => \^data\(18),
      R => count_clk0
    );
\count_clk_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[19]_i_1_n_4\,
      Q => \^data\(19),
      R => count_clk0
    );
\count_clk_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[15]_i_1_n_0\,
      CO(3) => \count_clk_reg[19]_i_1_n_0\,
      CO(2) => \count_clk_reg[19]_i_1_n_1\,
      CO(1) => \count_clk_reg[19]_i_1_n_2\,
      CO(0) => \count_clk_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk_reg[19]_i_1_n_4\,
      O(2) => \count_clk_reg[19]_i_1_n_5\,
      O(1) => \count_clk_reg[19]_i_1_n_6\,
      O(0) => \count_clk_reg[19]_i_1_n_7\,
      S(3 downto 0) => \^data\(19 downto 16)
    );
\count_clk_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[3]_i_1_n_6\,
      Q => \^data\(1),
      R => count_clk0
    );
\count_clk_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[23]_i_1_n_7\,
      Q => \^data\(20),
      R => count_clk0
    );
\count_clk_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[23]_i_1_n_6\,
      Q => \^data\(21),
      R => count_clk0
    );
\count_clk_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[23]_i_1_n_5\,
      Q => \^data\(22),
      R => count_clk0
    );
\count_clk_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[23]_i_1_n_4\,
      Q => \^data\(23),
      R => count_clk0
    );
\count_clk_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[19]_i_1_n_0\,
      CO(3) => \count_clk_reg[23]_i_1_n_0\,
      CO(2) => \count_clk_reg[23]_i_1_n_1\,
      CO(1) => \count_clk_reg[23]_i_1_n_2\,
      CO(0) => \count_clk_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk_reg[23]_i_1_n_4\,
      O(2) => \count_clk_reg[23]_i_1_n_5\,
      O(1) => \count_clk_reg[23]_i_1_n_6\,
      O(0) => \count_clk_reg[23]_i_1_n_7\,
      S(3 downto 0) => \^data\(23 downto 20)
    );
\count_clk_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[27]_i_1_n_7\,
      Q => \^data\(24),
      R => count_clk0
    );
\count_clk_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[27]_i_1_n_6\,
      Q => \^data\(25),
      R => count_clk0
    );
\count_clk_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[27]_i_1_n_5\,
      Q => \^data\(26),
      R => count_clk0
    );
\count_clk_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[27]_i_1_n_4\,
      Q => \^data\(27),
      R => count_clk0
    );
\count_clk_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[23]_i_1_n_0\,
      CO(3) => \count_clk_reg[27]_i_1_n_0\,
      CO(2) => \count_clk_reg[27]_i_1_n_1\,
      CO(1) => \count_clk_reg[27]_i_1_n_2\,
      CO(0) => \count_clk_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk_reg[27]_i_1_n_4\,
      O(2) => \count_clk_reg[27]_i_1_n_5\,
      O(1) => \count_clk_reg[27]_i_1_n_6\,
      O(0) => \count_clk_reg[27]_i_1_n_7\,
      S(3 downto 0) => \^data\(27 downto 24)
    );
\count_clk_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[31]_i_1_n_7\,
      Q => \^data\(28),
      R => count_clk0
    );
\count_clk_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[31]_i_1_n_6\,
      Q => \^data\(29),
      R => count_clk0
    );
\count_clk_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[3]_i_1_n_5\,
      Q => \^data\(2),
      R => count_clk0
    );
\count_clk_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[31]_i_1_n_5\,
      Q => \^data\(30),
      R => count_clk0
    );
\count_clk_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[31]_i_1_n_4\,
      Q => \^data\(31),
      R => count_clk0
    );
\count_clk_reg[31]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[27]_i_1_n_0\,
      CO(3) => \NLW_count_clk_reg[31]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \count_clk_reg[31]_i_1_n_1\,
      CO(1) => \count_clk_reg[31]_i_1_n_2\,
      CO(0) => \count_clk_reg[31]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk_reg[31]_i_1_n_4\,
      O(2) => \count_clk_reg[31]_i_1_n_5\,
      O(1) => \count_clk_reg[31]_i_1_n_6\,
      O(0) => \count_clk_reg[31]_i_1_n_7\,
      S(3 downto 0) => \^data\(31 downto 28)
    );
\count_clk_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[3]_i_1_n_4\,
      Q => \^data\(3),
      R => count_clk0
    );
\count_clk_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_clk_reg[3]_i_1_n_0\,
      CO(2) => \count_clk_reg[3]_i_1_n_1\,
      CO(1) => \count_clk_reg[3]_i_1_n_2\,
      CO(0) => \count_clk_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \count_clk_reg[3]_i_1_n_4\,
      O(2) => \count_clk_reg[3]_i_1_n_5\,
      O(1) => \count_clk_reg[3]_i_1_n_6\,
      O(0) => \count_clk_reg[3]_i_1_n_7\,
      S(3 downto 1) => \^data\(3 downto 1),
      S(0) => \count_clk[3]_i_2_n_0\
    );
\count_clk_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[7]_i_1_n_7\,
      Q => \^data\(4),
      R => count_clk0
    );
\count_clk_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[7]_i_1_n_6\,
      Q => \^data\(5),
      R => count_clk0
    );
\count_clk_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[7]_i_1_n_5\,
      Q => \^data\(6),
      R => count_clk0
    );
\count_clk_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[7]_i_1_n_4\,
      Q => \^data\(7),
      R => count_clk0
    );
\count_clk_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[3]_i_1_n_0\,
      CO(3) => \count_clk_reg[7]_i_1_n_0\,
      CO(2) => \count_clk_reg[7]_i_1_n_1\,
      CO(1) => \count_clk_reg[7]_i_1_n_2\,
      CO(0) => \count_clk_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk_reg[7]_i_1_n_4\,
      O(2) => \count_clk_reg[7]_i_1_n_5\,
      O(1) => \count_clk_reg[7]_i_1_n_6\,
      O(0) => \count_clk_reg[7]_i_1_n_7\,
      S(3 downto 0) => \^data\(7 downto 4)
    );
\count_clk_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[11]_i_1_n_7\,
      Q => \^data\(8),
      R => count_clk0
    );
\count_clk_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => \count_clk_reg[11]_i_1_n_6\,
      Q => \^data\(9),
      R => count_clk0
    );
write_reg_i_1: unisim.vcomponents.LUT3
    generic map(
      INIT => X"3A"
    )
        port map (
      I0 => \^write_reg_reg_0\,
      I1 => FLAG_state,
      I2 => count_clk0,
      O => write_reg_i_1_n_0
    );
write_reg_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk04_out,
      CE => '1',
      D => write_reg_i_1_n_0,
      Q => \^write_reg_reg_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_5MHz : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    clk_5MHzWR_100MhzRD : out STD_LOGIC;
    clk_100Mhz_AXI_send : out STD_LOGIC;
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    write : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_imitation_0_0,ADC_imitation,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_imitation,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^data\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  adr_bram(31 downto 0) <= \^data\(31 downto 0);
  data(31 downto 0) <= \^data\(31 downto 0);
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      clk_100MHz => clk_100MHz,
      clk_100Mhz_AXI_send => clk_100Mhz_AXI_send,
      clk_5MHz => clk_5MHz,
      clk_5MHzWR_100MhzRD => clk_5MHzWR_100MhzRD,
      data(31 downto 0) => \^data\(31 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      write_reg_reg_0 => write
    );
end STRUCTURE;
