// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sun Jun  7 20:00:37 2020
// Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.v
// Design      : design_1_ADC_imitation_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
   (data,
    clk_100Mhz_AXI_send,
    write_reg_reg_0,
    clk_5MHzWR_100MhzRD,
    clk_100MHz,
    clk_5MHz,
    s00_axi_aresetn,
    FrameSize);
  output [31:0]data;
  output clk_100Mhz_AXI_send;
  output write_reg_reg_0;
  output clk_5MHzWR_100MhzRD;
  input clk_100MHz;
  input clk_5MHz;
  input s00_axi_aresetn;
  input [15:0]FrameSize;

  wire FLAG_state;
  wire FLAG_state_i_1_n_0;
  wire [15:0]FrameSize;
  wire clk_100MHz;
  wire clk_100Mhz_AXI_send;
  wire clk_5MHz;
  wire clk_5MHzWR_100MhzRD;
  wire count_clk0;
  wire count_clk04_out;
  wire count_clk0_carry__0_i_1_n_0;
  wire count_clk0_carry__0_i_2_n_0;
  wire count_clk0_carry__0_i_3_n_0;
  wire count_clk0_carry__0_i_4_n_0;
  wire count_clk0_carry__0_n_0;
  wire count_clk0_carry__0_n_1;
  wire count_clk0_carry__0_n_2;
  wire count_clk0_carry__0_n_3;
  wire count_clk0_carry__1_i_1_n_0;
  wire count_clk0_carry__1_i_2_n_0;
  wire count_clk0_carry__1_i_3_n_0;
  wire count_clk0_carry__1_n_2;
  wire count_clk0_carry__1_n_3;
  wire count_clk0_carry_i_1_n_0;
  wire count_clk0_carry_i_2_n_0;
  wire count_clk0_carry_i_3_n_0;
  wire count_clk0_carry_i_4_n_0;
  wire count_clk0_carry_n_0;
  wire count_clk0_carry_n_1;
  wire count_clk0_carry_n_2;
  wire count_clk0_carry_n_3;
  wire \count_clk[3]_i_2_n_0 ;
  wire \count_clk_reg[11]_i_1_n_0 ;
  wire \count_clk_reg[11]_i_1_n_1 ;
  wire \count_clk_reg[11]_i_1_n_2 ;
  wire \count_clk_reg[11]_i_1_n_3 ;
  wire \count_clk_reg[11]_i_1_n_4 ;
  wire \count_clk_reg[11]_i_1_n_5 ;
  wire \count_clk_reg[11]_i_1_n_6 ;
  wire \count_clk_reg[11]_i_1_n_7 ;
  wire \count_clk_reg[15]_i_1_n_0 ;
  wire \count_clk_reg[15]_i_1_n_1 ;
  wire \count_clk_reg[15]_i_1_n_2 ;
  wire \count_clk_reg[15]_i_1_n_3 ;
  wire \count_clk_reg[15]_i_1_n_4 ;
  wire \count_clk_reg[15]_i_1_n_5 ;
  wire \count_clk_reg[15]_i_1_n_6 ;
  wire \count_clk_reg[15]_i_1_n_7 ;
  wire \count_clk_reg[19]_i_1_n_0 ;
  wire \count_clk_reg[19]_i_1_n_1 ;
  wire \count_clk_reg[19]_i_1_n_2 ;
  wire \count_clk_reg[19]_i_1_n_3 ;
  wire \count_clk_reg[19]_i_1_n_4 ;
  wire \count_clk_reg[19]_i_1_n_5 ;
  wire \count_clk_reg[19]_i_1_n_6 ;
  wire \count_clk_reg[19]_i_1_n_7 ;
  wire \count_clk_reg[23]_i_1_n_0 ;
  wire \count_clk_reg[23]_i_1_n_1 ;
  wire \count_clk_reg[23]_i_1_n_2 ;
  wire \count_clk_reg[23]_i_1_n_3 ;
  wire \count_clk_reg[23]_i_1_n_4 ;
  wire \count_clk_reg[23]_i_1_n_5 ;
  wire \count_clk_reg[23]_i_1_n_6 ;
  wire \count_clk_reg[23]_i_1_n_7 ;
  wire \count_clk_reg[27]_i_1_n_0 ;
  wire \count_clk_reg[27]_i_1_n_1 ;
  wire \count_clk_reg[27]_i_1_n_2 ;
  wire \count_clk_reg[27]_i_1_n_3 ;
  wire \count_clk_reg[27]_i_1_n_4 ;
  wire \count_clk_reg[27]_i_1_n_5 ;
  wire \count_clk_reg[27]_i_1_n_6 ;
  wire \count_clk_reg[27]_i_1_n_7 ;
  wire \count_clk_reg[31]_i_1_n_1 ;
  wire \count_clk_reg[31]_i_1_n_2 ;
  wire \count_clk_reg[31]_i_1_n_3 ;
  wire \count_clk_reg[31]_i_1_n_4 ;
  wire \count_clk_reg[31]_i_1_n_5 ;
  wire \count_clk_reg[31]_i_1_n_6 ;
  wire \count_clk_reg[31]_i_1_n_7 ;
  wire \count_clk_reg[3]_i_1_n_0 ;
  wire \count_clk_reg[3]_i_1_n_1 ;
  wire \count_clk_reg[3]_i_1_n_2 ;
  wire \count_clk_reg[3]_i_1_n_3 ;
  wire \count_clk_reg[3]_i_1_n_4 ;
  wire \count_clk_reg[3]_i_1_n_5 ;
  wire \count_clk_reg[3]_i_1_n_6 ;
  wire \count_clk_reg[3]_i_1_n_7 ;
  wire \count_clk_reg[7]_i_1_n_0 ;
  wire \count_clk_reg[7]_i_1_n_1 ;
  wire \count_clk_reg[7]_i_1_n_2 ;
  wire \count_clk_reg[7]_i_1_n_3 ;
  wire \count_clk_reg[7]_i_1_n_4 ;
  wire \count_clk_reg[7]_i_1_n_5 ;
  wire \count_clk_reg[7]_i_1_n_6 ;
  wire \count_clk_reg[7]_i_1_n_7 ;
  wire [31:0]data;
  wire s00_axi_aresetn;
  wire write_reg_i_1_n_0;
  wire write_reg_reg_0;
  wire [3:0]NLW_count_clk0_carry_O_UNCONNECTED;
  wire [3:0]NLW_count_clk0_carry__0_O_UNCONNECTED;
  wire [3:3]NLW_count_clk0_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_count_clk0_carry__1_O_UNCONNECTED;
  wire [3:3]\NLW_count_clk_reg[31]_i_1_CO_UNCONNECTED ;

  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h6)) 
    FLAG_state_i_1
       (.I0(count_clk0),
        .I1(FLAG_state),
        .O(FLAG_state_i_1_n_0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    FLAG_state_reg
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(FLAG_state_i_1_n_0),
        .Q(FLAG_state),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hB)) 
    clk_100Mhz_AXI_send_INST_0
       (.I0(write_reg_reg_0),
        .I1(clk_100MHz),
        .O(clk_100Mhz_AXI_send));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    clk_5MHzWR_100MhzRD_INST_0
       (.I0(clk_5MHz),
        .I1(write_reg_reg_0),
        .I2(clk_100MHz),
        .O(clk_5MHzWR_100MhzRD));
  CARRY4 count_clk0_carry
       (.CI(1'b0),
        .CO({count_clk0_carry_n_0,count_clk0_carry_n_1,count_clk0_carry_n_2,count_clk0_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_count_clk0_carry_O_UNCONNECTED[3:0]),
        .S({count_clk0_carry_i_1_n_0,count_clk0_carry_i_2_n_0,count_clk0_carry_i_3_n_0,count_clk0_carry_i_4_n_0}));
  CARRY4 count_clk0_carry__0
       (.CI(count_clk0_carry_n_0),
        .CO({count_clk0_carry__0_n_0,count_clk0_carry__0_n_1,count_clk0_carry__0_n_2,count_clk0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_count_clk0_carry__0_O_UNCONNECTED[3:0]),
        .S({count_clk0_carry__0_i_1_n_0,count_clk0_carry__0_i_2_n_0,count_clk0_carry__0_i_3_n_0,count_clk0_carry__0_i_4_n_0}));
  LUT3 #(
    .INIT(8'h01)) 
    count_clk0_carry__0_i_1
       (.I0(data[23]),
        .I1(data[22]),
        .I2(data[21]),
        .O(count_clk0_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_clk0_carry__0_i_2
       (.I0(data[20]),
        .I1(data[19]),
        .I2(data[18]),
        .O(count_clk0_carry__0_i_2_n_0));
  LUT4 #(
    .INIT(16'h0009)) 
    count_clk0_carry__0_i_3
       (.I0(data[15]),
        .I1(FrameSize[15]),
        .I2(data[17]),
        .I3(data[16]),
        .O(count_clk0_carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    count_clk0_carry__0_i_4
       (.I0(data[12]),
        .I1(FrameSize[12]),
        .I2(FrameSize[14]),
        .I3(data[14]),
        .I4(FrameSize[13]),
        .I5(data[13]),
        .O(count_clk0_carry__0_i_4_n_0));
  CARRY4 count_clk0_carry__1
       (.CI(count_clk0_carry__0_n_0),
        .CO({NLW_count_clk0_carry__1_CO_UNCONNECTED[3],count_clk0,count_clk0_carry__1_n_2,count_clk0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_count_clk0_carry__1_O_UNCONNECTED[3:0]),
        .S({1'b0,count_clk0_carry__1_i_1_n_0,count_clk0_carry__1_i_2_n_0,count_clk0_carry__1_i_3_n_0}));
  LUT2 #(
    .INIT(4'h1)) 
    count_clk0_carry__1_i_1
       (.I0(data[30]),
        .I1(data[31]),
        .O(count_clk0_carry__1_i_1_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_clk0_carry__1_i_2
       (.I0(data[29]),
        .I1(data[28]),
        .I2(data[27]),
        .O(count_clk0_carry__1_i_2_n_0));
  LUT3 #(
    .INIT(8'h01)) 
    count_clk0_carry__1_i_3
       (.I0(data[26]),
        .I1(data[25]),
        .I2(data[24]),
        .O(count_clk0_carry__1_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    count_clk0_carry_i_1
       (.I0(data[9]),
        .I1(FrameSize[9]),
        .I2(FrameSize[11]),
        .I3(data[11]),
        .I4(FrameSize[10]),
        .I5(data[10]),
        .O(count_clk0_carry_i_1_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    count_clk0_carry_i_2
       (.I0(data[6]),
        .I1(FrameSize[6]),
        .I2(FrameSize[8]),
        .I3(data[8]),
        .I4(FrameSize[7]),
        .I5(data[7]),
        .O(count_clk0_carry_i_2_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    count_clk0_carry_i_3
       (.I0(data[3]),
        .I1(FrameSize[3]),
        .I2(FrameSize[5]),
        .I3(data[5]),
        .I4(FrameSize[4]),
        .I5(data[4]),
        .O(count_clk0_carry_i_3_n_0));
  LUT6 #(
    .INIT(64'h9009000000009009)) 
    count_clk0_carry_i_4
       (.I0(data[0]),
        .I1(FrameSize[0]),
        .I2(FrameSize[2]),
        .I3(data[2]),
        .I4(FrameSize[1]),
        .I5(data[1]),
        .O(count_clk0_carry_i_4_n_0));
  LUT4 #(
    .INIT(16'hE200)) 
    \count_clk[31]_i_2 
       (.I0(clk_100MHz),
        .I1(write_reg_reg_0),
        .I2(clk_5MHz),
        .I3(s00_axi_aresetn),
        .O(count_clk04_out));
  LUT1 #(
    .INIT(2'h1)) 
    \count_clk[3]_i_2 
       (.I0(data[0]),
        .O(\count_clk[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[0] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[3]_i_1_n_7 ),
        .Q(data[0]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[10] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[11]_i_1_n_5 ),
        .Q(data[10]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[11] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[11]_i_1_n_4 ),
        .Q(data[11]),
        .R(count_clk0));
  CARRY4 \count_clk_reg[11]_i_1 
       (.CI(\count_clk_reg[7]_i_1_n_0 ),
        .CO({\count_clk_reg[11]_i_1_n_0 ,\count_clk_reg[11]_i_1_n_1 ,\count_clk_reg[11]_i_1_n_2 ,\count_clk_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk_reg[11]_i_1_n_4 ,\count_clk_reg[11]_i_1_n_5 ,\count_clk_reg[11]_i_1_n_6 ,\count_clk_reg[11]_i_1_n_7 }),
        .S(data[11:8]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[12] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[15]_i_1_n_7 ),
        .Q(data[12]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[13] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[15]_i_1_n_6 ),
        .Q(data[13]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[14] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[15]_i_1_n_5 ),
        .Q(data[14]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[15] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[15]_i_1_n_4 ),
        .Q(data[15]),
        .R(count_clk0));
  CARRY4 \count_clk_reg[15]_i_1 
       (.CI(\count_clk_reg[11]_i_1_n_0 ),
        .CO({\count_clk_reg[15]_i_1_n_0 ,\count_clk_reg[15]_i_1_n_1 ,\count_clk_reg[15]_i_1_n_2 ,\count_clk_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk_reg[15]_i_1_n_4 ,\count_clk_reg[15]_i_1_n_5 ,\count_clk_reg[15]_i_1_n_6 ,\count_clk_reg[15]_i_1_n_7 }),
        .S(data[15:12]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[16] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[19]_i_1_n_7 ),
        .Q(data[16]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[17] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[19]_i_1_n_6 ),
        .Q(data[17]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[18] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[19]_i_1_n_5 ),
        .Q(data[18]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[19] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[19]_i_1_n_4 ),
        .Q(data[19]),
        .R(count_clk0));
  CARRY4 \count_clk_reg[19]_i_1 
       (.CI(\count_clk_reg[15]_i_1_n_0 ),
        .CO({\count_clk_reg[19]_i_1_n_0 ,\count_clk_reg[19]_i_1_n_1 ,\count_clk_reg[19]_i_1_n_2 ,\count_clk_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk_reg[19]_i_1_n_4 ,\count_clk_reg[19]_i_1_n_5 ,\count_clk_reg[19]_i_1_n_6 ,\count_clk_reg[19]_i_1_n_7 }),
        .S(data[19:16]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[1] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[3]_i_1_n_6 ),
        .Q(data[1]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[20] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[23]_i_1_n_7 ),
        .Q(data[20]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[21] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[23]_i_1_n_6 ),
        .Q(data[21]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[22] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[23]_i_1_n_5 ),
        .Q(data[22]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[23] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[23]_i_1_n_4 ),
        .Q(data[23]),
        .R(count_clk0));
  CARRY4 \count_clk_reg[23]_i_1 
       (.CI(\count_clk_reg[19]_i_1_n_0 ),
        .CO({\count_clk_reg[23]_i_1_n_0 ,\count_clk_reg[23]_i_1_n_1 ,\count_clk_reg[23]_i_1_n_2 ,\count_clk_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk_reg[23]_i_1_n_4 ,\count_clk_reg[23]_i_1_n_5 ,\count_clk_reg[23]_i_1_n_6 ,\count_clk_reg[23]_i_1_n_7 }),
        .S(data[23:20]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[24] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[27]_i_1_n_7 ),
        .Q(data[24]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[25] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[27]_i_1_n_6 ),
        .Q(data[25]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[26] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[27]_i_1_n_5 ),
        .Q(data[26]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[27] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[27]_i_1_n_4 ),
        .Q(data[27]),
        .R(count_clk0));
  CARRY4 \count_clk_reg[27]_i_1 
       (.CI(\count_clk_reg[23]_i_1_n_0 ),
        .CO({\count_clk_reg[27]_i_1_n_0 ,\count_clk_reg[27]_i_1_n_1 ,\count_clk_reg[27]_i_1_n_2 ,\count_clk_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk_reg[27]_i_1_n_4 ,\count_clk_reg[27]_i_1_n_5 ,\count_clk_reg[27]_i_1_n_6 ,\count_clk_reg[27]_i_1_n_7 }),
        .S(data[27:24]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[28] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[31]_i_1_n_7 ),
        .Q(data[28]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[29] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[31]_i_1_n_6 ),
        .Q(data[29]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[2] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[3]_i_1_n_5 ),
        .Q(data[2]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[30] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[31]_i_1_n_5 ),
        .Q(data[30]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[31] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[31]_i_1_n_4 ),
        .Q(data[31]),
        .R(count_clk0));
  CARRY4 \count_clk_reg[31]_i_1 
       (.CI(\count_clk_reg[27]_i_1_n_0 ),
        .CO({\NLW_count_clk_reg[31]_i_1_CO_UNCONNECTED [3],\count_clk_reg[31]_i_1_n_1 ,\count_clk_reg[31]_i_1_n_2 ,\count_clk_reg[31]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk_reg[31]_i_1_n_4 ,\count_clk_reg[31]_i_1_n_5 ,\count_clk_reg[31]_i_1_n_6 ,\count_clk_reg[31]_i_1_n_7 }),
        .S(data[31:28]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[3] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[3]_i_1_n_4 ),
        .Q(data[3]),
        .R(count_clk0));
  CARRY4 \count_clk_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\count_clk_reg[3]_i_1_n_0 ,\count_clk_reg[3]_i_1_n_1 ,\count_clk_reg[3]_i_1_n_2 ,\count_clk_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\count_clk_reg[3]_i_1_n_4 ,\count_clk_reg[3]_i_1_n_5 ,\count_clk_reg[3]_i_1_n_6 ,\count_clk_reg[3]_i_1_n_7 }),
        .S({data[3:1],\count_clk[3]_i_2_n_0 }));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[4] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[7]_i_1_n_7 ),
        .Q(data[4]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[5] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[7]_i_1_n_6 ),
        .Q(data[5]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[6] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[7]_i_1_n_5 ),
        .Q(data[6]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[7] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[7]_i_1_n_4 ),
        .Q(data[7]),
        .R(count_clk0));
  CARRY4 \count_clk_reg[7]_i_1 
       (.CI(\count_clk_reg[3]_i_1_n_0 ),
        .CO({\count_clk_reg[7]_i_1_n_0 ,\count_clk_reg[7]_i_1_n_1 ,\count_clk_reg[7]_i_1_n_2 ,\count_clk_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk_reg[7]_i_1_n_4 ,\count_clk_reg[7]_i_1_n_5 ,\count_clk_reg[7]_i_1_n_6 ,\count_clk_reg[7]_i_1_n_7 }),
        .S(data[7:4]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[8] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[11]_i_1_n_7 ),
        .Q(data[8]),
        .R(count_clk0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[9] 
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(\count_clk_reg[11]_i_1_n_6 ),
        .Q(data[9]),
        .R(count_clk0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h3A)) 
    write_reg_i_1
       (.I0(write_reg_reg_0),
        .I1(FLAG_state),
        .I2(count_clk0),
        .O(write_reg_i_1_n_0));
  FDRE #(
    .IS_C_INVERTED(1'b1)) 
    write_reg_reg
       (.C(count_clk04_out),
        .CE(1'b1),
        .D(write_reg_i_1_n_0),
        .Q(write_reg_reg_0),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC_imitation_0_0,ADC_imitation,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ADC_imitation,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_5MHz,
    clk_100MHz,
    s00_axi_aresetn,
    FrameSize,
    clk_5MHzWR_100MhzRD,
    clk_100Mhz_AXI_send,
    adr_bram,
    data,
    write);
  input clk_5MHz;
  input clk_100MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  input [15:0]FrameSize;
  output clk_5MHzWR_100MhzRD;
  output clk_100Mhz_AXI_send;
  output [31:0]adr_bram;
  output [31:0]data;
  output write;

  wire [15:0]FrameSize;
  wire clk_100MHz;
  wire clk_100Mhz_AXI_send;
  wire clk_5MHz;
  wire clk_5MHzWR_100MhzRD;
  wire [31:0]data;
  wire s00_axi_aresetn;
  wire write;

  assign adr_bram[31:0] = data;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation inst
       (.FrameSize(FrameSize),
        .clk_100MHz(clk_100MHz),
        .clk_100Mhz_AXI_send(clk_100Mhz_AXI_send),
        .clk_5MHz(clk_5MHz),
        .clk_5MHzWR_100MhzRD(clk_5MHzWR_100MhzRD),
        .data(data),
        .s00_axi_aresetn(s00_axi_aresetn),
        .write_reg_reg_0(write));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
