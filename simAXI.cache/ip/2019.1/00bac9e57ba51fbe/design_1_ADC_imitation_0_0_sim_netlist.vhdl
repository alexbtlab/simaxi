-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jun  7 14:29:57 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_imitation_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  port (
    clk_100Mhz_AXI_send : out STD_LOGIC;
    write_reg_reg_0 : out STD_LOGIC;
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_5MHzWR_100MhzRD : out STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  signal FLAG_state : STD_LOGIC;
  signal FLAG_state_0 : STD_LOGIC;
  signal FLAG_state_i_1_n_0 : STD_LOGIC;
  signal \^adr_bram\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal count_clk : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \count_clk[31]_i_10_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_3_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_4_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_5_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_6_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_7_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_8_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_9_n_0\ : STD_LOGIC;
  signal \count_clk_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \count_clk_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal write_reg_i_1_n_0 : STD_LOGIC;
  signal \^write_reg_reg_0\ : STD_LOGIC;
  signal \NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_clk_reg[31]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of clk_100Mhz_AXI_send_INST_0 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of clk_5MHzWR_100MhzRD_INST_0 : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \count_clk[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count_clk[31]_i_8\ : label is "soft_lutpair0";
begin
  adr_bram(31 downto 0) <= \^adr_bram\(31 downto 0);
  write_reg_reg_0 <= \^write_reg_reg_0\;
FLAG_state_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFEF0010"
    )
        port map (
      I0 => \count_clk[31]_i_7_n_0\,
      I1 => \count_clk[31]_i_6_n_0\,
      I2 => \count_clk[31]_i_5_n_0\,
      I3 => \count_clk[31]_i_4_n_0\,
      I4 => FLAG_state,
      O => FLAG_state_i_1_n_0
    );
FLAG_state_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => FLAG_state_i_1_n_0,
      Q => FLAG_state,
      R => '0'
    );
clk_100Mhz_AXI_send_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^write_reg_reg_0\,
      I1 => clk_100MHz,
      O => clk_100Mhz_AXI_send
    );
clk_5MHzWR_100MhzRD_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => clk_5MHz,
      I1 => \^write_reg_reg_0\,
      I2 => clk_100MHz,
      O => clk_5MHzWR_100MhzRD
    );
\count_clk[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^adr_bram\(0),
      O => count_clk(0)
    );
\count_clk[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0004"
    )
        port map (
      I0 => \count_clk[31]_i_4_n_0\,
      I1 => \count_clk[31]_i_5_n_0\,
      I2 => \count_clk[31]_i_6_n_0\,
      I3 => \count_clk[31]_i_7_n_0\,
      O => FLAG_state_0
    );
\count_clk[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^adr_bram\(31),
      I1 => \^adr_bram\(19),
      I2 => \^adr_bram\(25),
      I3 => \^adr_bram\(20),
      O => \count_clk[31]_i_10_n_0\
    );
\count_clk[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E200"
    )
        port map (
      I0 => clk_100MHz,
      I1 => \^write_reg_reg_0\,
      I2 => clk_5MHz,
      I3 => s00_axi_aresetn,
      O => \count_clk[31]_i_3_n_0\
    );
\count_clk[31]_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFF7FF"
    )
        port map (
      I0 => \^adr_bram\(8),
      I1 => \^adr_bram\(9),
      I2 => \^adr_bram\(22),
      I3 => \^adr_bram\(3),
      I4 => \count_clk[31]_i_8_n_0\,
      I5 => \count_clk[31]_i_9_n_0\,
      O => \count_clk[31]_i_4_n_0\
    );
\count_clk[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000004"
    )
        port map (
      I0 => \^adr_bram\(15),
      I1 => \^adr_bram\(1),
      I2 => \^adr_bram\(21),
      I3 => \^adr_bram\(26),
      I4 => \count_clk[31]_i_10_n_0\,
      O => \count_clk[31]_i_5_n_0\
    );
\count_clk[31]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFEFFFFFF"
    )
        port map (
      I0 => \^adr_bram\(10),
      I1 => \^adr_bram\(11),
      I2 => \^adr_bram\(27),
      I3 => \^adr_bram\(2),
      I4 => \^adr_bram\(6),
      I5 => \^adr_bram\(24),
      O => \count_clk[31]_i_6_n_0\
    );
\count_clk[31]_i_7\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => \^adr_bram\(14),
      I1 => \^adr_bram\(13),
      I2 => \^adr_bram\(16),
      I3 => \^adr_bram\(29),
      I4 => \^adr_bram\(17),
      I5 => \^adr_bram\(28),
      O => \count_clk[31]_i_7_n_0\
    );
\count_clk[31]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FF7F"
    )
        port map (
      I0 => \^adr_bram\(7),
      I1 => \^adr_bram\(0),
      I2 => \^adr_bram\(5),
      I3 => \^adr_bram\(30),
      O => \count_clk[31]_i_8_n_0\
    );
\count_clk[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => \^adr_bram\(23),
      I1 => \^adr_bram\(12),
      I2 => \^adr_bram\(4),
      I3 => \^adr_bram\(18),
      O => \count_clk[31]_i_9_n_0\
    );
\count_clk_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => count_clk(0),
      Q => \^adr_bram\(0),
      R => '0'
    );
\count_clk_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(10),
      Q => \^adr_bram\(10),
      R => FLAG_state_0
    );
\count_clk_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(11),
      Q => \^adr_bram\(11),
      R => FLAG_state_0
    );
\count_clk_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(12),
      Q => \^adr_bram\(12),
      R => FLAG_state_0
    );
\count_clk_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[8]_i_1_n_0\,
      CO(3) => \count_clk_reg[12]_i_1_n_0\,
      CO(2) => \count_clk_reg[12]_i_1_n_1\,
      CO(1) => \count_clk_reg[12]_i_1_n_2\,
      CO(0) => \count_clk_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => \^adr_bram\(12 downto 9)
    );
\count_clk_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(13),
      Q => \^adr_bram\(13),
      R => FLAG_state_0
    );
\count_clk_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(14),
      Q => \^adr_bram\(14),
      R => FLAG_state_0
    );
\count_clk_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(15),
      Q => \^adr_bram\(15),
      R => FLAG_state_0
    );
\count_clk_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(16),
      Q => \^adr_bram\(16),
      R => FLAG_state_0
    );
\count_clk_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[12]_i_1_n_0\,
      CO(3) => \count_clk_reg[16]_i_1_n_0\,
      CO(2) => \count_clk_reg[16]_i_1_n_1\,
      CO(1) => \count_clk_reg[16]_i_1_n_2\,
      CO(0) => \count_clk_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3 downto 0) => \^adr_bram\(16 downto 13)
    );
\count_clk_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(17),
      Q => \^adr_bram\(17),
      R => FLAG_state_0
    );
\count_clk_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(18),
      Q => \^adr_bram\(18),
      R => FLAG_state_0
    );
\count_clk_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(19),
      Q => \^adr_bram\(19),
      R => FLAG_state_0
    );
\count_clk_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(1),
      Q => \^adr_bram\(1),
      R => FLAG_state_0
    );
\count_clk_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(20),
      Q => \^adr_bram\(20),
      R => FLAG_state_0
    );
\count_clk_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[16]_i_1_n_0\,
      CO(3) => \count_clk_reg[20]_i_1_n_0\,
      CO(2) => \count_clk_reg[20]_i_1_n_1\,
      CO(1) => \count_clk_reg[20]_i_1_n_2\,
      CO(0) => \count_clk_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(20 downto 17),
      S(3 downto 0) => \^adr_bram\(20 downto 17)
    );
\count_clk_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(21),
      Q => \^adr_bram\(21),
      R => FLAG_state_0
    );
\count_clk_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(22),
      Q => \^adr_bram\(22),
      R => FLAG_state_0
    );
\count_clk_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(23),
      Q => \^adr_bram\(23),
      R => FLAG_state_0
    );
\count_clk_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(24),
      Q => \^adr_bram\(24),
      R => FLAG_state_0
    );
\count_clk_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[20]_i_1_n_0\,
      CO(3) => \count_clk_reg[24]_i_1_n_0\,
      CO(2) => \count_clk_reg[24]_i_1_n_1\,
      CO(1) => \count_clk_reg[24]_i_1_n_2\,
      CO(0) => \count_clk_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(24 downto 21),
      S(3 downto 0) => \^adr_bram\(24 downto 21)
    );
\count_clk_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(25),
      Q => \^adr_bram\(25),
      R => FLAG_state_0
    );
\count_clk_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(26),
      Q => \^adr_bram\(26),
      R => FLAG_state_0
    );
\count_clk_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(27),
      Q => \^adr_bram\(27),
      R => FLAG_state_0
    );
\count_clk_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(28),
      Q => \^adr_bram\(28),
      R => FLAG_state_0
    );
\count_clk_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[24]_i_1_n_0\,
      CO(3) => \count_clk_reg[28]_i_1_n_0\,
      CO(2) => \count_clk_reg[28]_i_1_n_1\,
      CO(1) => \count_clk_reg[28]_i_1_n_2\,
      CO(0) => \count_clk_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(28 downto 25),
      S(3 downto 0) => \^adr_bram\(28 downto 25)
    );
\count_clk_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(29),
      Q => \^adr_bram\(29),
      R => FLAG_state_0
    );
\count_clk_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(2),
      Q => \^adr_bram\(2),
      R => FLAG_state_0
    );
\count_clk_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(30),
      Q => \^adr_bram\(30),
      R => FLAG_state_0
    );
\count_clk_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(31),
      Q => \^adr_bram\(31),
      R => FLAG_state_0
    );
\count_clk_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[28]_i_1_n_0\,
      CO(3 downto 2) => \NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count_clk_reg[31]_i_2_n_2\,
      CO(0) => \count_clk_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_count_clk_reg[31]_i_2_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => \^adr_bram\(31 downto 29)
    );
\count_clk_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(3),
      Q => \^adr_bram\(3),
      R => FLAG_state_0
    );
\count_clk_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(4),
      Q => \^adr_bram\(4),
      R => FLAG_state_0
    );
\count_clk_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_clk_reg[4]_i_1_n_0\,
      CO(2) => \count_clk_reg[4]_i_1_n_1\,
      CO(1) => \count_clk_reg[4]_i_1_n_2\,
      CO(0) => \count_clk_reg[4]_i_1_n_3\,
      CYINIT => \^adr_bram\(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => \^adr_bram\(4 downto 1)
    );
\count_clk_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(5),
      Q => \^adr_bram\(5),
      R => FLAG_state_0
    );
\count_clk_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(6),
      Q => \^adr_bram\(6),
      R => FLAG_state_0
    );
\count_clk_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(7),
      Q => \^adr_bram\(7),
      R => FLAG_state_0
    );
\count_clk_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(8),
      Q => \^adr_bram\(8),
      R => FLAG_state_0
    );
\count_clk_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[4]_i_1_n_0\,
      CO(3) => \count_clk_reg[8]_i_1_n_0\,
      CO(2) => \count_clk_reg[8]_i_1_n_1\,
      CO(1) => \count_clk_reg[8]_i_1_n_2\,
      CO(0) => \count_clk_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => \^adr_bram\(8 downto 5)
    );
\count_clk_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(9),
      Q => \^adr_bram\(9),
      R => FLAG_state_0
    );
write_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAAAAA8AAAAAAABA"
    )
        port map (
      I0 => \^write_reg_reg_0\,
      I1 => \count_clk[31]_i_4_n_0\,
      I2 => \count_clk[31]_i_5_n_0\,
      I3 => \count_clk[31]_i_6_n_0\,
      I4 => \count_clk[31]_i_7_n_0\,
      I5 => FLAG_state,
      O => write_reg_i_1_n_0
    );
write_reg_reg: unisim.vcomponents.FDRE
    generic map(
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => write_reg_i_1_n_0,
      Q => \^write_reg_reg_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_5MHz : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    clk_5MHzWR_100MhzRD : out STD_LOGIC;
    clk_100Mhz_AXI_send : out STD_LOGIC;
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    write : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_imitation_0_0,ADC_imitation,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_imitation,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^adr_bram\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  adr_bram(31 downto 0) <= \^adr_bram\(31 downto 0);
  data(31 downto 0) <= \^adr_bram\(31 downto 0);
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
     port map (
      adr_bram(31 downto 0) => \^adr_bram\(31 downto 0),
      clk_100MHz => clk_100MHz,
      clk_100Mhz_AXI_send => clk_100Mhz_AXI_send,
      clk_5MHz => clk_5MHz,
      clk_5MHzWR_100MhzRD => clk_5MHzWR_100MhzRD,
      s00_axi_aresetn => s00_axi_aresetn,
      write_reg_reg_0 => write
    );
end STRUCTURE;
