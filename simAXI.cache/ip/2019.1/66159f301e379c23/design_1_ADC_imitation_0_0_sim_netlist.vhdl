-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Thu Jun  4 10:30:34 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_imitation_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  port (
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    write : out STD_LOGIC;
    clk_5MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  signal count_clk : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \count_clk0_carry__0_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__0_n_1\ : STD_LOGIC;
  signal \count_clk0_carry__0_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__0_n_3\ : STD_LOGIC;
  signal \count_clk0_carry__1_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__1_n_1\ : STD_LOGIC;
  signal \count_clk0_carry__1_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__1_n_3\ : STD_LOGIC;
  signal \count_clk0_carry__2_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__2_n_1\ : STD_LOGIC;
  signal \count_clk0_carry__2_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__2_n_3\ : STD_LOGIC;
  signal \count_clk0_carry__3_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__3_n_1\ : STD_LOGIC;
  signal \count_clk0_carry__3_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__3_n_3\ : STD_LOGIC;
  signal \count_clk0_carry__4_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__4_n_1\ : STD_LOGIC;
  signal \count_clk0_carry__4_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__4_n_3\ : STD_LOGIC;
  signal \count_clk0_carry__5_n_0\ : STD_LOGIC;
  signal \count_clk0_carry__5_n_1\ : STD_LOGIC;
  signal \count_clk0_carry__5_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__5_n_3\ : STD_LOGIC;
  signal \count_clk0_carry__6_n_2\ : STD_LOGIC;
  signal \count_clk0_carry__6_n_3\ : STD_LOGIC;
  signal count_clk0_carry_n_0 : STD_LOGIC;
  signal count_clk0_carry_n_1 : STD_LOGIC;
  signal count_clk0_carry_n_2 : STD_LOGIC;
  signal count_clk0_carry_n_3 : STD_LOGIC;
  signal \count_clk[31]_i_1_n_0\ : STD_LOGIC;
  signal count_clk_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \data_adc_deserial[31]_i_1_n_0\ : STD_LOGIC;
  signal \^write\ : STD_LOGIC;
  signal write_reg_i_1_n_0 : STD_LOGIC;
  signal write_reg_i_2_n_0 : STD_LOGIC;
  signal write_reg_i_3_n_0 : STD_LOGIC;
  signal write_reg_i_4_n_0 : STD_LOGIC;
  signal write_reg_i_5_n_0 : STD_LOGIC;
  signal write_reg_i_6_n_0 : STD_LOGIC;
  signal write_reg_i_7_n_0 : STD_LOGIC;
  signal write_reg_i_8_n_0 : STD_LOGIC;
  signal write_reg_i_9_n_0 : STD_LOGIC;
  signal \NLW_count_clk0_carry__6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_clk0_carry__6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count_clk[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of write_reg_i_1 : label is "soft_lutpair0";
begin
  write <= \^write\;
count_clk0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => count_clk0_carry_n_0,
      CO(2) => count_clk0_carry_n_1,
      CO(1) => count_clk0_carry_n_2,
      CO(0) => count_clk0_carry_n_3,
      CYINIT => count_clk(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => count_clk(4 downto 1)
    );
\count_clk0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => count_clk0_carry_n_0,
      CO(3) => \count_clk0_carry__0_n_0\,
      CO(2) => \count_clk0_carry__0_n_1\,
      CO(1) => \count_clk0_carry__0_n_2\,
      CO(0) => \count_clk0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => count_clk(8 downto 5)
    );
\count_clk0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk0_carry__0_n_0\,
      CO(3) => \count_clk0_carry__1_n_0\,
      CO(2) => \count_clk0_carry__1_n_1\,
      CO(1) => \count_clk0_carry__1_n_2\,
      CO(0) => \count_clk0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => count_clk(12 downto 9)
    );
\count_clk0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk0_carry__1_n_0\,
      CO(3) => \count_clk0_carry__2_n_0\,
      CO(2) => \count_clk0_carry__2_n_1\,
      CO(1) => \count_clk0_carry__2_n_2\,
      CO(0) => \count_clk0_carry__2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3 downto 0) => count_clk(16 downto 13)
    );
\count_clk0_carry__3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk0_carry__2_n_0\,
      CO(3) => \count_clk0_carry__3_n_0\,
      CO(2) => \count_clk0_carry__3_n_1\,
      CO(1) => \count_clk0_carry__3_n_2\,
      CO(0) => \count_clk0_carry__3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(20 downto 17),
      S(3 downto 0) => count_clk(20 downto 17)
    );
\count_clk0_carry__4\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk0_carry__3_n_0\,
      CO(3) => \count_clk0_carry__4_n_0\,
      CO(2) => \count_clk0_carry__4_n_1\,
      CO(1) => \count_clk0_carry__4_n_2\,
      CO(0) => \count_clk0_carry__4_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(24 downto 21),
      S(3 downto 0) => count_clk(24 downto 21)
    );
\count_clk0_carry__5\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk0_carry__4_n_0\,
      CO(3) => \count_clk0_carry__5_n_0\,
      CO(2) => \count_clk0_carry__5_n_1\,
      CO(1) => \count_clk0_carry__5_n_2\,
      CO(0) => \count_clk0_carry__5_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(28 downto 25),
      S(3 downto 0) => count_clk(28 downto 25)
    );
\count_clk0_carry__6\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk0_carry__5_n_0\,
      CO(3 downto 2) => \NLW_count_clk0_carry__6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count_clk0_carry__6_n_2\,
      CO(0) => \count_clk0_carry__6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_count_clk0_carry__6_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => count_clk(31 downto 29)
    );
\count_clk[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"00FE"
    )
        port map (
      I0 => write_reg_i_4_n_0,
      I1 => write_reg_i_3_n_0,
      I2 => write_reg_i_2_n_0,
      I3 => count_clk(0),
      O => count_clk_0(0)
    );
\count_clk[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => count_clk(0),
      I1 => write_reg_i_2_n_0,
      I2 => write_reg_i_3_n_0,
      I3 => write_reg_i_4_n_0,
      O => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk_0(0),
      Q => count_clk(0),
      R => '0'
    );
\count_clk_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(10),
      Q => count_clk(10),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(11),
      Q => count_clk(11),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(12),
      Q => count_clk(12),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(13),
      Q => count_clk(13),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(14),
      Q => count_clk(14),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(15),
      Q => count_clk(15),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(16),
      Q => count_clk(16),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(17),
      Q => count_clk(17),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(18),
      Q => count_clk(18),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(19),
      Q => count_clk(19),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(1),
      Q => count_clk(1),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(20),
      Q => count_clk(20),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(21),
      Q => count_clk(21),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(22),
      Q => count_clk(22),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(23),
      Q => count_clk(23),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(24),
      Q => count_clk(24),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(25),
      Q => count_clk(25),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(26),
      Q => count_clk(26),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(27),
      Q => count_clk(27),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(28),
      Q => count_clk(28),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(29),
      Q => count_clk(29),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(2),
      Q => count_clk(2),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(30),
      Q => count_clk(30),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(31),
      Q => count_clk(31),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(3),
      Q => count_clk(3),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(4),
      Q => count_clk(4),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(5),
      Q => count_clk(5),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(6),
      Q => count_clk(6),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(7),
      Q => count_clk(7),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(8),
      Q => count_clk(8),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => data0(9),
      Q => count_clk(9),
      R => \count_clk[31]_i_1_n_0\
    );
\data_adc_deserial[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_5MHz,
      I1 => s00_axi_aresetn,
      O => \data_adc_deserial[31]_i_1_n_0\
    );
\data_adc_deserial_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(0),
      Q => data(0),
      R => '0'
    );
\data_adc_deserial_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(10),
      Q => data(10),
      R => '0'
    );
\data_adc_deserial_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(11),
      Q => data(11),
      R => '0'
    );
\data_adc_deserial_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(12),
      Q => data(12),
      R => '0'
    );
\data_adc_deserial_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(13),
      Q => data(13),
      R => '0'
    );
\data_adc_deserial_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(14),
      Q => data(14),
      R => '0'
    );
\data_adc_deserial_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(15),
      Q => data(15),
      R => '0'
    );
\data_adc_deserial_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(16),
      Q => data(16),
      R => '0'
    );
\data_adc_deserial_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(17),
      Q => data(17),
      R => '0'
    );
\data_adc_deserial_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(18),
      Q => data(18),
      R => '0'
    );
\data_adc_deserial_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(19),
      Q => data(19),
      R => '0'
    );
\data_adc_deserial_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(1),
      Q => data(1),
      R => '0'
    );
\data_adc_deserial_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(20),
      Q => data(20),
      R => '0'
    );
\data_adc_deserial_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(21),
      Q => data(21),
      R => '0'
    );
\data_adc_deserial_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(22),
      Q => data(22),
      R => '0'
    );
\data_adc_deserial_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(23),
      Q => data(23),
      R => '0'
    );
\data_adc_deserial_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(24),
      Q => data(24),
      R => '0'
    );
\data_adc_deserial_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(25),
      Q => data(25),
      R => '0'
    );
\data_adc_deserial_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(26),
      Q => data(26),
      R => '0'
    );
\data_adc_deserial_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(27),
      Q => data(27),
      R => '0'
    );
\data_adc_deserial_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(28),
      Q => data(28),
      R => '0'
    );
\data_adc_deserial_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(29),
      Q => data(29),
      R => '0'
    );
\data_adc_deserial_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(2),
      Q => data(2),
      R => '0'
    );
\data_adc_deserial_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(30),
      Q => data(30),
      R => '0'
    );
\data_adc_deserial_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(31),
      Q => data(31),
      R => '0'
    );
\data_adc_deserial_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(3),
      Q => data(3),
      R => '0'
    );
\data_adc_deserial_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(4),
      Q => data(4),
      R => '0'
    );
\data_adc_deserial_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(5),
      Q => data(5),
      R => '0'
    );
\data_adc_deserial_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(6),
      Q => data(6),
      R => '0'
    );
\data_adc_deserial_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(7),
      Q => data(7),
      R => '0'
    );
\data_adc_deserial_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(8),
      Q => data(8),
      R => '0'
    );
\data_adc_deserial_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => count_clk(9),
      Q => data(9),
      R => '0'
    );
write_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => count_clk(0),
      I1 => write_reg_i_2_n_0,
      I2 => write_reg_i_3_n_0,
      I3 => write_reg_i_4_n_0,
      I4 => \^write\,
      O => write_reg_i_1_n_0
    );
write_reg_i_2: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFBFFF"
    )
        port map (
      I0 => count_clk(13),
      I1 => count_clk(12),
      I2 => count_clk(10),
      I3 => count_clk(11),
      I4 => write_reg_i_5_n_0,
      O => write_reg_i_2_n_0
    );
write_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => count_clk(4),
      I1 => count_clk(5),
      I2 => count_clk(2),
      I3 => count_clk(3),
      I4 => write_reg_i_6_n_0,
      O => write_reg_i_3_n_0
    );
write_reg_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFFFFFE"
    )
        port map (
      I0 => write_reg_i_7_n_0,
      I1 => write_reg_i_8_n_0,
      I2 => count_clk(31),
      I3 => count_clk(30),
      I4 => count_clk(1),
      I5 => write_reg_i_9_n_0,
      O => write_reg_i_4_n_0
    );
write_reg_i_5: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk(15),
      I1 => count_clk(14),
      I2 => count_clk(17),
      I3 => count_clk(16),
      O => write_reg_i_5_n_0
    );
write_reg_i_6: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => count_clk(6),
      I1 => count_clk(7),
      I2 => count_clk(9),
      I3 => count_clk(8),
      O => write_reg_i_6_n_0
    );
write_reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk(23),
      I1 => count_clk(22),
      I2 => count_clk(25),
      I3 => count_clk(24),
      O => write_reg_i_7_n_0
    );
write_reg_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk(19),
      I1 => count_clk(18),
      I2 => count_clk(21),
      I3 => count_clk(20),
      O => write_reg_i_8_n_0
    );
write_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk(27),
      I1 => count_clk(26),
      I2 => count_clk(29),
      I3 => count_clk(28),
      O => write_reg_i_9_n_0
    );
write_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => \data_adc_deserial[31]_i_1_n_0\,
      CE => '1',
      D => write_reg_i_1_n_0,
      Q => \^write\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_5MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    write : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_imitation_0_0,ADC_imitation,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_imitation,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^data\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  adr_bram(31 downto 0) <= \^data\(31 downto 0);
  data(31 downto 0) <= \^data\(31 downto 0);
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
     port map (
      clk_5MHz => clk_5MHz,
      data(31 downto 0) => \^data\(31 downto 0),
      s00_axi_aresetn => s00_axi_aresetn,
      write => write
    );
end STRUCTURE;
