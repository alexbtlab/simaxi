// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Jun  4 18:07:12 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.v
// Design      : design_1_ADC_imitation_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
   (UNCONN_OUT,
    adr_bram,
    data,
    last_word_transmit,
    clk_100MHz,
    s00_axi_aresetn,
    clk_5MHz);
  output UNCONN_OUT;
  output [31:0]adr_bram;
  output [31:0]data;
  input last_word_transmit;
  input clk_100MHz;
  input s00_axi_aresetn;
  input clk_5MHz;

  wire UNCONN_OUT;
  wire [31:0]adr_bram;
  wire clk_100MHz;
  wire clk_5MHz;
  wire [31:0]count_clk100;
  wire count_clk1000;
  wire \count_clk100[0]_i_1_n_0 ;
  wire \count_clk100[31]_i_10_n_0 ;
  wire \count_clk100[31]_i_11_n_0 ;
  wire \count_clk100[31]_i_12_n_0 ;
  wire \count_clk100[31]_i_1_n_0 ;
  wire \count_clk100[31]_i_5_n_0 ;
  wire \count_clk100[31]_i_6_n_0 ;
  wire \count_clk100[31]_i_7_n_0 ;
  wire \count_clk100[31]_i_8_n_0 ;
  wire \count_clk100[31]_i_9_n_0 ;
  wire \count_clk100_reg[12]_i_1_n_0 ;
  wire \count_clk100_reg[12]_i_1_n_1 ;
  wire \count_clk100_reg[12]_i_1_n_2 ;
  wire \count_clk100_reg[12]_i_1_n_3 ;
  wire \count_clk100_reg[16]_i_1_n_0 ;
  wire \count_clk100_reg[16]_i_1_n_1 ;
  wire \count_clk100_reg[16]_i_1_n_2 ;
  wire \count_clk100_reg[16]_i_1_n_3 ;
  wire \count_clk100_reg[20]_i_1_n_0 ;
  wire \count_clk100_reg[20]_i_1_n_1 ;
  wire \count_clk100_reg[20]_i_1_n_2 ;
  wire \count_clk100_reg[20]_i_1_n_3 ;
  wire \count_clk100_reg[24]_i_1_n_0 ;
  wire \count_clk100_reg[24]_i_1_n_1 ;
  wire \count_clk100_reg[24]_i_1_n_2 ;
  wire \count_clk100_reg[24]_i_1_n_3 ;
  wire \count_clk100_reg[28]_i_1_n_0 ;
  wire \count_clk100_reg[28]_i_1_n_1 ;
  wire \count_clk100_reg[28]_i_1_n_2 ;
  wire \count_clk100_reg[28]_i_1_n_3 ;
  wire \count_clk100_reg[31]_i_3_n_2 ;
  wire \count_clk100_reg[31]_i_3_n_3 ;
  wire \count_clk100_reg[4]_i_1_n_0 ;
  wire \count_clk100_reg[4]_i_1_n_1 ;
  wire \count_clk100_reg[4]_i_1_n_2 ;
  wire \count_clk100_reg[4]_i_1_n_3 ;
  wire \count_clk100_reg[8]_i_1_n_0 ;
  wire \count_clk100_reg[8]_i_1_n_1 ;
  wire \count_clk100_reg[8]_i_1_n_2 ;
  wire \count_clk100_reg[8]_i_1_n_3 ;
  wire [31:0]count_clk5;
  wire \count_clk5[31]_i_1_n_0 ;
  wire [0:0]count_clk5_0;
  wire \count_clk5_reg[12]_i_1_n_0 ;
  wire \count_clk5_reg[12]_i_1_n_1 ;
  wire \count_clk5_reg[12]_i_1_n_2 ;
  wire \count_clk5_reg[12]_i_1_n_3 ;
  wire \count_clk5_reg[12]_i_1_n_4 ;
  wire \count_clk5_reg[12]_i_1_n_5 ;
  wire \count_clk5_reg[12]_i_1_n_6 ;
  wire \count_clk5_reg[12]_i_1_n_7 ;
  wire \count_clk5_reg[16]_i_1_n_0 ;
  wire \count_clk5_reg[16]_i_1_n_1 ;
  wire \count_clk5_reg[16]_i_1_n_2 ;
  wire \count_clk5_reg[16]_i_1_n_3 ;
  wire \count_clk5_reg[16]_i_1_n_4 ;
  wire \count_clk5_reg[16]_i_1_n_5 ;
  wire \count_clk5_reg[16]_i_1_n_6 ;
  wire \count_clk5_reg[16]_i_1_n_7 ;
  wire \count_clk5_reg[20]_i_1_n_0 ;
  wire \count_clk5_reg[20]_i_1_n_1 ;
  wire \count_clk5_reg[20]_i_1_n_2 ;
  wire \count_clk5_reg[20]_i_1_n_3 ;
  wire \count_clk5_reg[20]_i_1_n_4 ;
  wire \count_clk5_reg[20]_i_1_n_5 ;
  wire \count_clk5_reg[20]_i_1_n_6 ;
  wire \count_clk5_reg[20]_i_1_n_7 ;
  wire \count_clk5_reg[24]_i_1_n_0 ;
  wire \count_clk5_reg[24]_i_1_n_1 ;
  wire \count_clk5_reg[24]_i_1_n_2 ;
  wire \count_clk5_reg[24]_i_1_n_3 ;
  wire \count_clk5_reg[24]_i_1_n_4 ;
  wire \count_clk5_reg[24]_i_1_n_5 ;
  wire \count_clk5_reg[24]_i_1_n_6 ;
  wire \count_clk5_reg[24]_i_1_n_7 ;
  wire \count_clk5_reg[28]_i_1_n_0 ;
  wire \count_clk5_reg[28]_i_1_n_1 ;
  wire \count_clk5_reg[28]_i_1_n_2 ;
  wire \count_clk5_reg[28]_i_1_n_3 ;
  wire \count_clk5_reg[28]_i_1_n_4 ;
  wire \count_clk5_reg[28]_i_1_n_5 ;
  wire \count_clk5_reg[28]_i_1_n_6 ;
  wire \count_clk5_reg[28]_i_1_n_7 ;
  wire \count_clk5_reg[31]_i_2_n_2 ;
  wire \count_clk5_reg[31]_i_2_n_3 ;
  wire \count_clk5_reg[31]_i_2_n_5 ;
  wire \count_clk5_reg[31]_i_2_n_6 ;
  wire \count_clk5_reg[31]_i_2_n_7 ;
  wire \count_clk5_reg[4]_i_1_n_0 ;
  wire \count_clk5_reg[4]_i_1_n_1 ;
  wire \count_clk5_reg[4]_i_1_n_2 ;
  wire \count_clk5_reg[4]_i_1_n_3 ;
  wire \count_clk5_reg[4]_i_1_n_4 ;
  wire \count_clk5_reg[4]_i_1_n_5 ;
  wire \count_clk5_reg[4]_i_1_n_6 ;
  wire \count_clk5_reg[4]_i_1_n_7 ;
  wire \count_clk5_reg[8]_i_1_n_0 ;
  wire \count_clk5_reg[8]_i_1_n_1 ;
  wire \count_clk5_reg[8]_i_1_n_2 ;
  wire \count_clk5_reg[8]_i_1_n_3 ;
  wire \count_clk5_reg[8]_i_1_n_4 ;
  wire \count_clk5_reg[8]_i_1_n_5 ;
  wire \count_clk5_reg[8]_i_1_n_6 ;
  wire \count_clk5_reg[8]_i_1_n_7 ;
  wire [31:0]data;
  wire [31:1]data0;
  wire [31:0]data_adc_deserial;
  wire data_adc_deserial0;
  wire last_word_transmit;
  wire p_0_in;
  wire s00_axi_aresetn;
  wire write_reg_i_10_n_0;
  wire write_reg_i_1_n_0;
  wire write_reg_i_3_n_0;
  wire write_reg_i_4_n_0;
  wire write_reg_i_5_n_0;
  wire write_reg_i_6_n_0;
  wire write_reg_i_7_n_0;
  wire write_reg_i_8_n_0;
  wire write_reg_i_9_n_0;
  wire [3:2]\NLW_count_clk100_reg[31]_i_3_CO_UNCONNECTED ;
  wire [3:3]\NLW_count_clk100_reg[31]_i_3_O_UNCONNECTED ;
  wire [3:2]\NLW_count_clk5_reg[31]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_count_clk5_reg[31]_i_2_O_UNCONNECTED ;

  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[0]_INST_0 
       (.I0(data_adc_deserial[0]),
        .I1(count_clk100[0]),
        .I2(UNCONN_OUT),
        .O(adr_bram[0]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[10]_INST_0 
       (.I0(data_adc_deserial[10]),
        .I1(count_clk100[10]),
        .I2(UNCONN_OUT),
        .O(adr_bram[10]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[11]_INST_0 
       (.I0(data_adc_deserial[11]),
        .I1(count_clk100[11]),
        .I2(UNCONN_OUT),
        .O(adr_bram[11]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[12]_INST_0 
       (.I0(data_adc_deserial[12]),
        .I1(count_clk100[12]),
        .I2(UNCONN_OUT),
        .O(adr_bram[12]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[13]_INST_0 
       (.I0(data_adc_deserial[13]),
        .I1(count_clk100[13]),
        .I2(UNCONN_OUT),
        .O(adr_bram[13]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[14]_INST_0 
       (.I0(data_adc_deserial[14]),
        .I1(count_clk100[14]),
        .I2(UNCONN_OUT),
        .O(adr_bram[14]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[15]_INST_0 
       (.I0(data_adc_deserial[15]),
        .I1(count_clk100[15]),
        .I2(UNCONN_OUT),
        .O(adr_bram[15]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[16]_INST_0 
       (.I0(data_adc_deserial[16]),
        .I1(count_clk100[16]),
        .I2(UNCONN_OUT),
        .O(adr_bram[16]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[17]_INST_0 
       (.I0(data_adc_deserial[17]),
        .I1(count_clk100[17]),
        .I2(UNCONN_OUT),
        .O(adr_bram[17]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[18]_INST_0 
       (.I0(data_adc_deserial[18]),
        .I1(count_clk100[18]),
        .I2(UNCONN_OUT),
        .O(adr_bram[18]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[19]_INST_0 
       (.I0(data_adc_deserial[19]),
        .I1(count_clk100[19]),
        .I2(UNCONN_OUT),
        .O(adr_bram[19]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[1]_INST_0 
       (.I0(data_adc_deserial[1]),
        .I1(count_clk100[1]),
        .I2(UNCONN_OUT),
        .O(adr_bram[1]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[20]_INST_0 
       (.I0(data_adc_deserial[20]),
        .I1(count_clk100[20]),
        .I2(UNCONN_OUT),
        .O(adr_bram[20]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[21]_INST_0 
       (.I0(data_adc_deserial[21]),
        .I1(count_clk100[21]),
        .I2(UNCONN_OUT),
        .O(adr_bram[21]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[22]_INST_0 
       (.I0(data_adc_deserial[22]),
        .I1(count_clk100[22]),
        .I2(UNCONN_OUT),
        .O(adr_bram[22]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[23]_INST_0 
       (.I0(data_adc_deserial[23]),
        .I1(count_clk100[23]),
        .I2(UNCONN_OUT),
        .O(adr_bram[23]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[24]_INST_0 
       (.I0(data_adc_deserial[24]),
        .I1(count_clk100[24]),
        .I2(UNCONN_OUT),
        .O(adr_bram[24]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[25]_INST_0 
       (.I0(data_adc_deserial[25]),
        .I1(count_clk100[25]),
        .I2(UNCONN_OUT),
        .O(adr_bram[25]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[26]_INST_0 
       (.I0(data_adc_deserial[26]),
        .I1(count_clk100[26]),
        .I2(UNCONN_OUT),
        .O(adr_bram[26]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[27]_INST_0 
       (.I0(data_adc_deserial[27]),
        .I1(count_clk100[27]),
        .I2(UNCONN_OUT),
        .O(adr_bram[27]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[28]_INST_0 
       (.I0(data_adc_deserial[28]),
        .I1(count_clk100[28]),
        .I2(UNCONN_OUT),
        .O(adr_bram[28]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[29]_INST_0 
       (.I0(data_adc_deserial[29]),
        .I1(count_clk100[29]),
        .I2(UNCONN_OUT),
        .O(adr_bram[29]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[2]_INST_0 
       (.I0(data_adc_deserial[2]),
        .I1(count_clk100[2]),
        .I2(UNCONN_OUT),
        .O(adr_bram[2]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[30]_INST_0 
       (.I0(data_adc_deserial[30]),
        .I1(count_clk100[30]),
        .I2(UNCONN_OUT),
        .O(adr_bram[30]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[31]_INST_0 
       (.I0(data_adc_deserial[31]),
        .I1(count_clk100[31]),
        .I2(UNCONN_OUT),
        .O(adr_bram[31]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[3]_INST_0 
       (.I0(data_adc_deserial[3]),
        .I1(count_clk100[3]),
        .I2(UNCONN_OUT),
        .O(adr_bram[3]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[4]_INST_0 
       (.I0(data_adc_deserial[4]),
        .I1(count_clk100[4]),
        .I2(UNCONN_OUT),
        .O(adr_bram[4]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[5]_INST_0 
       (.I0(data_adc_deserial[5]),
        .I1(count_clk100[5]),
        .I2(UNCONN_OUT),
        .O(adr_bram[5]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[6]_INST_0 
       (.I0(data_adc_deserial[6]),
        .I1(count_clk100[6]),
        .I2(UNCONN_OUT),
        .O(adr_bram[6]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[7]_INST_0 
       (.I0(data_adc_deserial[7]),
        .I1(count_clk100[7]),
        .I2(UNCONN_OUT),
        .O(adr_bram[7]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[8]_INST_0 
       (.I0(data_adc_deserial[8]),
        .I1(count_clk100[8]),
        .I2(UNCONN_OUT),
        .O(adr_bram[8]));
  LUT3 #(
    .INIT(8'hAC)) 
    \adr_bram[9]_INST_0 
       (.I0(data_adc_deserial[9]),
        .I1(count_clk100[9]),
        .I2(UNCONN_OUT),
        .O(adr_bram[9]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_clk100[0]_i_1 
       (.I0(count_clk100[0]),
        .O(\count_clk100[0]_i_1_n_0 ));
  LUT5 #(
    .INIT(32'h00000001)) 
    \count_clk100[31]_i_1 
       (.I0(\count_clk100[31]_i_5_n_0 ),
        .I1(\count_clk100[31]_i_6_n_0 ),
        .I2(\count_clk100[31]_i_7_n_0 ),
        .I3(\count_clk100[31]_i_8_n_0 ),
        .I4(UNCONN_OUT),
        .O(\count_clk100[31]_i_1_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count_clk100[31]_i_10 
       (.I0(count_clk100[28]),
        .I1(count_clk100[27]),
        .I2(count_clk100[31]),
        .I3(count_clk100[29]),
        .O(\count_clk100[31]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \count_clk100[31]_i_11 
       (.I0(count_clk100[4]),
        .I1(count_clk100[3]),
        .I2(count_clk100[6]),
        .I3(count_clk100[5]),
        .O(\count_clk100[31]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \count_clk100[31]_i_12 
       (.I0(count_clk100[11]),
        .I1(count_clk100[30]),
        .I2(count_clk100[16]),
        .I3(count_clk100[17]),
        .O(\count_clk100[31]_i_12_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \count_clk100[31]_i_2 
       (.I0(UNCONN_OUT),
        .O(p_0_in));
  LUT2 #(
    .INIT(4'h8)) 
    \count_clk100[31]_i_4 
       (.I0(clk_100MHz),
        .I1(s00_axi_aresetn),
        .O(count_clk1000));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \count_clk100[31]_i_5 
       (.I0(count_clk100[13]),
        .I1(count_clk100[18]),
        .I2(count_clk100[15]),
        .I3(count_clk100[14]),
        .I4(\count_clk100[31]_i_9_n_0 ),
        .O(\count_clk100[31]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \count_clk100[31]_i_6 
       (.I0(count_clk100[25]),
        .I1(count_clk100[26]),
        .I2(count_clk100[23]),
        .I3(count_clk100[24]),
        .I4(\count_clk100[31]_i_10_n_0 ),
        .O(\count_clk100[31]_i_6_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \count_clk100[31]_i_7 
       (.I0(count_clk100[1]),
        .I1(count_clk100[2]),
        .I2(count_clk100[12]),
        .I3(count_clk100[0]),
        .I4(\count_clk100[31]_i_11_n_0 ),
        .O(\count_clk100[31]_i_7_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \count_clk100[31]_i_8 
       (.I0(count_clk100[9]),
        .I1(count_clk100[10]),
        .I2(count_clk100[7]),
        .I3(count_clk100[8]),
        .I4(\count_clk100[31]_i_12_n_0 ),
        .O(\count_clk100[31]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count_clk100[31]_i_9 
       (.I0(count_clk100[20]),
        .I1(count_clk100[19]),
        .I2(count_clk100[22]),
        .I3(count_clk100[21]),
        .O(\count_clk100[31]_i_9_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[0] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(\count_clk100[0]_i_1_n_0 ),
        .Q(count_clk100[0]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[10] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[10]),
        .Q(count_clk100[10]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[11] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[11]),
        .Q(count_clk100[11]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[12] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[12]),
        .Q(count_clk100[12]),
        .R(\count_clk100[31]_i_1_n_0 ));
  CARRY4 \count_clk100_reg[12]_i_1 
       (.CI(\count_clk100_reg[8]_i_1_n_0 ),
        .CO({\count_clk100_reg[12]_i_1_n_0 ,\count_clk100_reg[12]_i_1_n_1 ,\count_clk100_reg[12]_i_1_n_2 ,\count_clk100_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(count_clk100[12:9]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[13] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[13]),
        .Q(count_clk100[13]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[14] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[14]),
        .Q(count_clk100[14]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[15] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[15]),
        .Q(count_clk100[15]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[16] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[16]),
        .Q(count_clk100[16]),
        .R(\count_clk100[31]_i_1_n_0 ));
  CARRY4 \count_clk100_reg[16]_i_1 
       (.CI(\count_clk100_reg[12]_i_1_n_0 ),
        .CO({\count_clk100_reg[16]_i_1_n_0 ,\count_clk100_reg[16]_i_1_n_1 ,\count_clk100_reg[16]_i_1_n_2 ,\count_clk100_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[16:13]),
        .S(count_clk100[16:13]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[17] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[17]),
        .Q(count_clk100[17]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[18] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[18]),
        .Q(count_clk100[18]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[19] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[19]),
        .Q(count_clk100[19]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[1] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[1]),
        .Q(count_clk100[1]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[20] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[20]),
        .Q(count_clk100[20]),
        .R(\count_clk100[31]_i_1_n_0 ));
  CARRY4 \count_clk100_reg[20]_i_1 
       (.CI(\count_clk100_reg[16]_i_1_n_0 ),
        .CO({\count_clk100_reg[20]_i_1_n_0 ,\count_clk100_reg[20]_i_1_n_1 ,\count_clk100_reg[20]_i_1_n_2 ,\count_clk100_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[20:17]),
        .S(count_clk100[20:17]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[21] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[21]),
        .Q(count_clk100[21]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[22] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[22]),
        .Q(count_clk100[22]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[23] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[23]),
        .Q(count_clk100[23]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[24] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[24]),
        .Q(count_clk100[24]),
        .R(\count_clk100[31]_i_1_n_0 ));
  CARRY4 \count_clk100_reg[24]_i_1 
       (.CI(\count_clk100_reg[20]_i_1_n_0 ),
        .CO({\count_clk100_reg[24]_i_1_n_0 ,\count_clk100_reg[24]_i_1_n_1 ,\count_clk100_reg[24]_i_1_n_2 ,\count_clk100_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[24:21]),
        .S(count_clk100[24:21]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[25] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[25]),
        .Q(count_clk100[25]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[26] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[26]),
        .Q(count_clk100[26]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[27] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[27]),
        .Q(count_clk100[27]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[28] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[28]),
        .Q(count_clk100[28]),
        .R(\count_clk100[31]_i_1_n_0 ));
  CARRY4 \count_clk100_reg[28]_i_1 
       (.CI(\count_clk100_reg[24]_i_1_n_0 ),
        .CO({\count_clk100_reg[28]_i_1_n_0 ,\count_clk100_reg[28]_i_1_n_1 ,\count_clk100_reg[28]_i_1_n_2 ,\count_clk100_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[28:25]),
        .S(count_clk100[28:25]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[29] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[29]),
        .Q(count_clk100[29]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[2] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[2]),
        .Q(count_clk100[2]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[30] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[30]),
        .Q(count_clk100[30]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[31] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[31]),
        .Q(count_clk100[31]),
        .R(\count_clk100[31]_i_1_n_0 ));
  CARRY4 \count_clk100_reg[31]_i_3 
       (.CI(\count_clk100_reg[28]_i_1_n_0 ),
        .CO({\NLW_count_clk100_reg[31]_i_3_CO_UNCONNECTED [3:2],\count_clk100_reg[31]_i_3_n_2 ,\count_clk100_reg[31]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_count_clk100_reg[31]_i_3_O_UNCONNECTED [3],data0[31:29]}),
        .S({1'b0,count_clk100[31:29]}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[3] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[3]),
        .Q(count_clk100[3]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[4] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[4]),
        .Q(count_clk100[4]),
        .R(\count_clk100[31]_i_1_n_0 ));
  CARRY4 \count_clk100_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\count_clk100_reg[4]_i_1_n_0 ,\count_clk100_reg[4]_i_1_n_1 ,\count_clk100_reg[4]_i_1_n_2 ,\count_clk100_reg[4]_i_1_n_3 }),
        .CYINIT(count_clk100[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(count_clk100[4:1]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[5] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[5]),
        .Q(count_clk100[5]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[6] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[6]),
        .Q(count_clk100[6]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[7] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[7]),
        .Q(count_clk100[7]),
        .R(\count_clk100[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[8] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[8]),
        .Q(count_clk100[8]),
        .R(\count_clk100[31]_i_1_n_0 ));
  CARRY4 \count_clk100_reg[8]_i_1 
       (.CI(\count_clk100_reg[4]_i_1_n_0 ),
        .CO({\count_clk100_reg[8]_i_1_n_0 ,\count_clk100_reg[8]_i_1_n_1 ,\count_clk100_reg[8]_i_1_n_2 ,\count_clk100_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(count_clk100[8:5]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk100_reg[9] 
       (.C(count_clk1000),
        .CE(p_0_in),
        .D(data0[9]),
        .Q(count_clk100[9]),
        .R(\count_clk100[31]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \count_clk5[0]_i_1 
       (.I0(count_clk5[0]),
        .O(count_clk5_0));
  LUT4 #(
    .INIT(16'h0001)) 
    \count_clk5[31]_i_1 
       (.I0(write_reg_i_3_n_0),
        .I1(write_reg_i_4_n_0),
        .I2(write_reg_i_5_n_0),
        .I3(write_reg_i_6_n_0),
        .O(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[0] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5_0),
        .Q(count_clk5[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[10] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[12]_i_1_n_6 ),
        .Q(count_clk5[10]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[11] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[12]_i_1_n_5 ),
        .Q(count_clk5[11]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[12] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[12]_i_1_n_4 ),
        .Q(count_clk5[12]),
        .R(\count_clk5[31]_i_1_n_0 ));
  CARRY4 \count_clk5_reg[12]_i_1 
       (.CI(\count_clk5_reg[8]_i_1_n_0 ),
        .CO({\count_clk5_reg[12]_i_1_n_0 ,\count_clk5_reg[12]_i_1_n_1 ,\count_clk5_reg[12]_i_1_n_2 ,\count_clk5_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk5_reg[12]_i_1_n_4 ,\count_clk5_reg[12]_i_1_n_5 ,\count_clk5_reg[12]_i_1_n_6 ,\count_clk5_reg[12]_i_1_n_7 }),
        .S(count_clk5[12:9]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[13] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[16]_i_1_n_7 ),
        .Q(count_clk5[13]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[14] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[16]_i_1_n_6 ),
        .Q(count_clk5[14]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[15] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[16]_i_1_n_5 ),
        .Q(count_clk5[15]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[16] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[16]_i_1_n_4 ),
        .Q(count_clk5[16]),
        .R(\count_clk5[31]_i_1_n_0 ));
  CARRY4 \count_clk5_reg[16]_i_1 
       (.CI(\count_clk5_reg[12]_i_1_n_0 ),
        .CO({\count_clk5_reg[16]_i_1_n_0 ,\count_clk5_reg[16]_i_1_n_1 ,\count_clk5_reg[16]_i_1_n_2 ,\count_clk5_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk5_reg[16]_i_1_n_4 ,\count_clk5_reg[16]_i_1_n_5 ,\count_clk5_reg[16]_i_1_n_6 ,\count_clk5_reg[16]_i_1_n_7 }),
        .S(count_clk5[16:13]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[17] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[20]_i_1_n_7 ),
        .Q(count_clk5[17]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[18] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[20]_i_1_n_6 ),
        .Q(count_clk5[18]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[19] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[20]_i_1_n_5 ),
        .Q(count_clk5[19]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[1] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[4]_i_1_n_7 ),
        .Q(count_clk5[1]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[20] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[20]_i_1_n_4 ),
        .Q(count_clk5[20]),
        .R(\count_clk5[31]_i_1_n_0 ));
  CARRY4 \count_clk5_reg[20]_i_1 
       (.CI(\count_clk5_reg[16]_i_1_n_0 ),
        .CO({\count_clk5_reg[20]_i_1_n_0 ,\count_clk5_reg[20]_i_1_n_1 ,\count_clk5_reg[20]_i_1_n_2 ,\count_clk5_reg[20]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk5_reg[20]_i_1_n_4 ,\count_clk5_reg[20]_i_1_n_5 ,\count_clk5_reg[20]_i_1_n_6 ,\count_clk5_reg[20]_i_1_n_7 }),
        .S(count_clk5[20:17]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[21] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[24]_i_1_n_7 ),
        .Q(count_clk5[21]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[22] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[24]_i_1_n_6 ),
        .Q(count_clk5[22]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[23] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[24]_i_1_n_5 ),
        .Q(count_clk5[23]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[24] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[24]_i_1_n_4 ),
        .Q(count_clk5[24]),
        .R(\count_clk5[31]_i_1_n_0 ));
  CARRY4 \count_clk5_reg[24]_i_1 
       (.CI(\count_clk5_reg[20]_i_1_n_0 ),
        .CO({\count_clk5_reg[24]_i_1_n_0 ,\count_clk5_reg[24]_i_1_n_1 ,\count_clk5_reg[24]_i_1_n_2 ,\count_clk5_reg[24]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk5_reg[24]_i_1_n_4 ,\count_clk5_reg[24]_i_1_n_5 ,\count_clk5_reg[24]_i_1_n_6 ,\count_clk5_reg[24]_i_1_n_7 }),
        .S(count_clk5[24:21]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[25] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[28]_i_1_n_7 ),
        .Q(count_clk5[25]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[26] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[28]_i_1_n_6 ),
        .Q(count_clk5[26]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[27] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[28]_i_1_n_5 ),
        .Q(count_clk5[27]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[28] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[28]_i_1_n_4 ),
        .Q(count_clk5[28]),
        .R(\count_clk5[31]_i_1_n_0 ));
  CARRY4 \count_clk5_reg[28]_i_1 
       (.CI(\count_clk5_reg[24]_i_1_n_0 ),
        .CO({\count_clk5_reg[28]_i_1_n_0 ,\count_clk5_reg[28]_i_1_n_1 ,\count_clk5_reg[28]_i_1_n_2 ,\count_clk5_reg[28]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk5_reg[28]_i_1_n_4 ,\count_clk5_reg[28]_i_1_n_5 ,\count_clk5_reg[28]_i_1_n_6 ,\count_clk5_reg[28]_i_1_n_7 }),
        .S(count_clk5[28:25]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[29] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[31]_i_2_n_7 ),
        .Q(count_clk5[29]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[2] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[4]_i_1_n_6 ),
        .Q(count_clk5[2]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[30] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[31]_i_2_n_6 ),
        .Q(count_clk5[30]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[31] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[31]_i_2_n_5 ),
        .Q(count_clk5[31]),
        .R(\count_clk5[31]_i_1_n_0 ));
  CARRY4 \count_clk5_reg[31]_i_2 
       (.CI(\count_clk5_reg[28]_i_1_n_0 ),
        .CO({\NLW_count_clk5_reg[31]_i_2_CO_UNCONNECTED [3:2],\count_clk5_reg[31]_i_2_n_2 ,\count_clk5_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_count_clk5_reg[31]_i_2_O_UNCONNECTED [3],\count_clk5_reg[31]_i_2_n_5 ,\count_clk5_reg[31]_i_2_n_6 ,\count_clk5_reg[31]_i_2_n_7 }),
        .S({1'b0,count_clk5[31:29]}));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[3] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[4]_i_1_n_5 ),
        .Q(count_clk5[3]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[4] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[4]_i_1_n_4 ),
        .Q(count_clk5[4]),
        .R(\count_clk5[31]_i_1_n_0 ));
  CARRY4 \count_clk5_reg[4]_i_1 
       (.CI(1'b0),
        .CO({\count_clk5_reg[4]_i_1_n_0 ,\count_clk5_reg[4]_i_1_n_1 ,\count_clk5_reg[4]_i_1_n_2 ,\count_clk5_reg[4]_i_1_n_3 }),
        .CYINIT(count_clk5[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk5_reg[4]_i_1_n_4 ,\count_clk5_reg[4]_i_1_n_5 ,\count_clk5_reg[4]_i_1_n_6 ,\count_clk5_reg[4]_i_1_n_7 }),
        .S(count_clk5[4:1]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[5] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[8]_i_1_n_7 ),
        .Q(count_clk5[5]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[6] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[8]_i_1_n_6 ),
        .Q(count_clk5[6]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[7] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[8]_i_1_n_5 ),
        .Q(count_clk5[7]),
        .R(\count_clk5[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[8] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[8]_i_1_n_4 ),
        .Q(count_clk5[8]),
        .R(\count_clk5[31]_i_1_n_0 ));
  CARRY4 \count_clk5_reg[8]_i_1 
       (.CI(\count_clk5_reg[4]_i_1_n_0 ),
        .CO({\count_clk5_reg[8]_i_1_n_0 ,\count_clk5_reg[8]_i_1_n_1 ,\count_clk5_reg[8]_i_1_n_2 ,\count_clk5_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\count_clk5_reg[8]_i_1_n_4 ,\count_clk5_reg[8]_i_1_n_5 ,\count_clk5_reg[8]_i_1_n_6 ,\count_clk5_reg[8]_i_1_n_7 }),
        .S(count_clk5[8:5]));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk5_reg[9] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(\count_clk5_reg[12]_i_1_n_7 ),
        .Q(count_clk5[9]),
        .R(\count_clk5[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \data[0]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[0]),
        .O(data[0]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[10]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[10]),
        .O(data[10]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[11]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[11]),
        .O(data[11]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[12]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[12]),
        .O(data[12]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[13]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[13]),
        .O(data[13]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[14]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[14]),
        .O(data[14]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[15]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[15]),
        .O(data[15]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[16]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[16]),
        .O(data[16]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[17]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[17]),
        .O(data[17]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[18]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[18]),
        .O(data[18]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[19]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[19]),
        .O(data[19]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[1]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[1]),
        .O(data[1]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[20]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[20]),
        .O(data[20]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[21]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[21]),
        .O(data[21]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[22]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[22]),
        .O(data[22]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[23]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[23]),
        .O(data[23]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[24]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[24]),
        .O(data[24]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[25]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[25]),
        .O(data[25]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[26]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[26]),
        .O(data[26]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[27]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[27]),
        .O(data[27]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[28]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[28]),
        .O(data[28]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[29]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[29]),
        .O(data[29]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[2]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[2]),
        .O(data[2]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[30]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[30]),
        .O(data[30]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[31]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[31]),
        .O(data[31]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[3]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[3]),
        .O(data[3]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[4]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[4]),
        .O(data[4]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[5]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[5]),
        .O(data[5]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[6]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[6]),
        .O(data[6]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[7]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[7]),
        .O(data[7]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[8]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[8]),
        .O(data[8]));
  LUT2 #(
    .INIT(4'h8)) 
    \data[9]_INST_0 
       (.I0(UNCONN_OUT),
        .I1(data_adc_deserial[9]),
        .O(data[9]));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[0] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[0]),
        .Q(data_adc_deserial[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[10] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[10]),
        .Q(data_adc_deserial[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[11] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[11]),
        .Q(data_adc_deserial[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[12] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[12]),
        .Q(data_adc_deserial[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[13] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[13]),
        .Q(data_adc_deserial[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[14] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[14]),
        .Q(data_adc_deserial[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[15] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[15]),
        .Q(data_adc_deserial[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[16] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[16]),
        .Q(data_adc_deserial[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[17] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[17]),
        .Q(data_adc_deserial[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[18] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[18]),
        .Q(data_adc_deserial[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[19] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[19]),
        .Q(data_adc_deserial[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[1] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[1]),
        .Q(data_adc_deserial[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[20] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[20]),
        .Q(data_adc_deserial[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[21] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[21]),
        .Q(data_adc_deserial[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[22] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[22]),
        .Q(data_adc_deserial[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[23] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[23]),
        .Q(data_adc_deserial[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[24] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[24]),
        .Q(data_adc_deserial[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[25] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[25]),
        .Q(data_adc_deserial[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[26] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[26]),
        .Q(data_adc_deserial[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[27] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[27]),
        .Q(data_adc_deserial[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[28] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[28]),
        .Q(data_adc_deserial[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[29] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[29]),
        .Q(data_adc_deserial[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[2] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[2]),
        .Q(data_adc_deserial[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[30] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[30]),
        .Q(data_adc_deserial[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[31] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[31]),
        .Q(data_adc_deserial[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[3] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[3]),
        .Q(data_adc_deserial[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[4] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[4]),
        .Q(data_adc_deserial[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[5] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[5]),
        .Q(data_adc_deserial[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[6] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[6]),
        .Q(data_adc_deserial[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[7] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[7]),
        .Q(data_adc_deserial[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[8] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[8]),
        .Q(data_adc_deserial[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[9] 
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(count_clk5[9]),
        .Q(data_adc_deserial[9]),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    write_reg_i_1
       (.I0(write_reg_i_3_n_0),
        .I1(write_reg_i_4_n_0),
        .I2(write_reg_i_5_n_0),
        .I3(write_reg_i_6_n_0),
        .I4(UNCONN_OUT),
        .O(write_reg_i_1_n_0));
  LUT4 #(
    .INIT(16'hFFFD)) 
    write_reg_i_10
       (.I0(count_clk5[12]),
        .I1(count_clk5[13]),
        .I2(count_clk5[15]),
        .I3(count_clk5[14]),
        .O(write_reg_i_10_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    write_reg_i_2
       (.I0(clk_5MHz),
        .I1(s00_axi_aresetn),
        .O(data_adc_deserial0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    write_reg_i_3
       (.I0(count_clk5[18]),
        .I1(count_clk5[19]),
        .I2(count_clk5[16]),
        .I3(count_clk5[17]),
        .I4(write_reg_i_7_n_0),
        .O(write_reg_i_3_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    write_reg_i_4
       (.I0(count_clk5[26]),
        .I1(count_clk5[27]),
        .I2(count_clk5[24]),
        .I3(count_clk5[25]),
        .I4(write_reg_i_8_n_0),
        .O(write_reg_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    write_reg_i_5
       (.I0(count_clk5[2]),
        .I1(count_clk5[3]),
        .I2(count_clk5[0]),
        .I3(count_clk5[1]),
        .I4(write_reg_i_9_n_0),
        .O(write_reg_i_5_n_0));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    write_reg_i_6
       (.I0(count_clk5[10]),
        .I1(count_clk5[11]),
        .I2(count_clk5[8]),
        .I3(count_clk5[9]),
        .I4(write_reg_i_10_n_0),
        .O(write_reg_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    write_reg_i_7
       (.I0(count_clk5[21]),
        .I1(count_clk5[20]),
        .I2(count_clk5[23]),
        .I3(count_clk5[22]),
        .O(write_reg_i_7_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    write_reg_i_8
       (.I0(count_clk5[29]),
        .I1(count_clk5[28]),
        .I2(count_clk5[31]),
        .I3(count_clk5[30]),
        .O(write_reg_i_8_n_0));
  LUT4 #(
    .INIT(16'h7FFF)) 
    write_reg_i_9
       (.I0(count_clk5[5]),
        .I1(count_clk5[4]),
        .I2(count_clk5[7]),
        .I3(count_clk5[6]),
        .O(write_reg_i_9_n_0));
  FDRE #(
    .INIT(1'b1)) 
    write_reg_reg
       (.C(data_adc_deserial0),
        .CE(1'b1),
        .D(write_reg_i_1_n_0),
        .Q(UNCONN_OUT),
        .R(1'b0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    write_reg_reg__0
       (.C(last_word_transmit),
        .CE(1'b1),
        .D(1'b1),
        .Q(UNCONN_OUT),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC_imitation_0_0,ADC_imitation,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ADC_imitation,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_5MHz,
    clk_100MHz,
    s00_axi_aresetn,
    last_word_transmit,
    adr_read_ram,
    clk_5MHzWR_100MhzRD,
    clk_100Mhz_AXI_send,
    adr_bram,
    data,
    write);
  input clk_5MHz;
  input clk_100MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  input last_word_transmit;
  input [31:0]adr_read_ram;
  output clk_5MHzWR_100MhzRD;
  output clk_100Mhz_AXI_send;
  output [31:0]adr_bram;
  output [31:0]data;
  output write;

  wire [31:0]adr_bram;
  wire clk_100MHz;
  wire clk_100Mhz_AXI_send;
  wire clk_5MHz;
  wire clk_5MHzWR_100MhzRD;
  wire [31:0]data;
  wire last_word_transmit;
  wire s00_axi_aresetn;
  wire write;

  LUT2 #(
    .INIT(4'hB)) 
    clk_100Mhz_AXI_send_INST_0
       (.I0(write),
        .I1(clk_100MHz),
        .O(clk_100Mhz_AXI_send));
  LUT3 #(
    .INIT(8'hB8)) 
    clk_5MHzWR_100MhzRD_INST_0
       (.I0(clk_5MHz),
        .I1(write),
        .I2(clk_100MHz),
        .O(clk_5MHzWR_100MhzRD));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation inst
       (.UNCONN_OUT(write),
        .adr_bram(adr_bram),
        .clk_100MHz(clk_100MHz),
        .clk_5MHz(clk_5MHz),
        .data(data),
        .last_word_transmit(last_word_transmit),
        .s00_axi_aresetn(s00_axi_aresetn));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
