-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Thu Jun  4 18:07:12 2020
-- Host        : zl-04 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_imitation_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  port (
    UNCONN_OUT : out STD_LOGIC;
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    last_word_transmit : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  signal \^unconn_out\ : STD_LOGIC;
  signal count_clk100 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal count_clk1000 : STD_LOGIC;
  signal \count_clk100[0]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_10_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_11_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_12_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_5_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_6_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_7_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_8_n_0\ : STD_LOGIC;
  signal \count_clk100[31]_i_9_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[31]_i_3_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[31]_i_3_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk100_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk100_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk100_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk100_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal count_clk5 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \count_clk5[31]_i_1_n_0\ : STD_LOGIC;
  signal count_clk5_0 : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \count_clk5_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \count_clk5_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal data_adc_deserial : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal data_adc_deserial0 : STD_LOGIC;
  signal p_0_in : STD_LOGIC;
  signal write_reg_i_10_n_0 : STD_LOGIC;
  signal write_reg_i_1_n_0 : STD_LOGIC;
  signal write_reg_i_3_n_0 : STD_LOGIC;
  signal write_reg_i_4_n_0 : STD_LOGIC;
  signal write_reg_i_5_n_0 : STD_LOGIC;
  signal write_reg_i_6_n_0 : STD_LOGIC;
  signal write_reg_i_7_n_0 : STD_LOGIC;
  signal write_reg_i_8_n_0 : STD_LOGIC;
  signal write_reg_i_9_n_0 : STD_LOGIC;
  signal \NLW_count_clk100_reg[31]_i_3_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_clk100_reg[31]_i_3_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_count_clk5_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_clk5_reg[31]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count_clk100[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count_clk100[31]_i_7\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count_clk5[0]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of write_reg_i_5 : label is "soft_lutpair1";
begin
  UNCONN_OUT <= \^unconn_out\;
\adr_bram[0]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(0),
      I1 => count_clk100(0),
      I2 => \^unconn_out\,
      O => adr_bram(0)
    );
\adr_bram[10]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(10),
      I1 => count_clk100(10),
      I2 => \^unconn_out\,
      O => adr_bram(10)
    );
\adr_bram[11]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(11),
      I1 => count_clk100(11),
      I2 => \^unconn_out\,
      O => adr_bram(11)
    );
\adr_bram[12]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(12),
      I1 => count_clk100(12),
      I2 => \^unconn_out\,
      O => adr_bram(12)
    );
\adr_bram[13]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(13),
      I1 => count_clk100(13),
      I2 => \^unconn_out\,
      O => adr_bram(13)
    );
\adr_bram[14]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(14),
      I1 => count_clk100(14),
      I2 => \^unconn_out\,
      O => adr_bram(14)
    );
\adr_bram[15]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(15),
      I1 => count_clk100(15),
      I2 => \^unconn_out\,
      O => adr_bram(15)
    );
\adr_bram[16]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(16),
      I1 => count_clk100(16),
      I2 => \^unconn_out\,
      O => adr_bram(16)
    );
\adr_bram[17]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(17),
      I1 => count_clk100(17),
      I2 => \^unconn_out\,
      O => adr_bram(17)
    );
\adr_bram[18]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(18),
      I1 => count_clk100(18),
      I2 => \^unconn_out\,
      O => adr_bram(18)
    );
\adr_bram[19]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(19),
      I1 => count_clk100(19),
      I2 => \^unconn_out\,
      O => adr_bram(19)
    );
\adr_bram[1]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(1),
      I1 => count_clk100(1),
      I2 => \^unconn_out\,
      O => adr_bram(1)
    );
\adr_bram[20]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(20),
      I1 => count_clk100(20),
      I2 => \^unconn_out\,
      O => adr_bram(20)
    );
\adr_bram[21]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(21),
      I1 => count_clk100(21),
      I2 => \^unconn_out\,
      O => adr_bram(21)
    );
\adr_bram[22]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(22),
      I1 => count_clk100(22),
      I2 => \^unconn_out\,
      O => adr_bram(22)
    );
\adr_bram[23]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(23),
      I1 => count_clk100(23),
      I2 => \^unconn_out\,
      O => adr_bram(23)
    );
\adr_bram[24]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(24),
      I1 => count_clk100(24),
      I2 => \^unconn_out\,
      O => adr_bram(24)
    );
\adr_bram[25]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(25),
      I1 => count_clk100(25),
      I2 => \^unconn_out\,
      O => adr_bram(25)
    );
\adr_bram[26]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(26),
      I1 => count_clk100(26),
      I2 => \^unconn_out\,
      O => adr_bram(26)
    );
\adr_bram[27]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(27),
      I1 => count_clk100(27),
      I2 => \^unconn_out\,
      O => adr_bram(27)
    );
\adr_bram[28]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(28),
      I1 => count_clk100(28),
      I2 => \^unconn_out\,
      O => adr_bram(28)
    );
\adr_bram[29]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(29),
      I1 => count_clk100(29),
      I2 => \^unconn_out\,
      O => adr_bram(29)
    );
\adr_bram[2]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(2),
      I1 => count_clk100(2),
      I2 => \^unconn_out\,
      O => adr_bram(2)
    );
\adr_bram[30]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(30),
      I1 => count_clk100(30),
      I2 => \^unconn_out\,
      O => adr_bram(30)
    );
\adr_bram[31]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(31),
      I1 => count_clk100(31),
      I2 => \^unconn_out\,
      O => adr_bram(31)
    );
\adr_bram[3]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(3),
      I1 => count_clk100(3),
      I2 => \^unconn_out\,
      O => adr_bram(3)
    );
\adr_bram[4]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(4),
      I1 => count_clk100(4),
      I2 => \^unconn_out\,
      O => adr_bram(4)
    );
\adr_bram[5]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(5),
      I1 => count_clk100(5),
      I2 => \^unconn_out\,
      O => adr_bram(5)
    );
\adr_bram[6]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(6),
      I1 => count_clk100(6),
      I2 => \^unconn_out\,
      O => adr_bram(6)
    );
\adr_bram[7]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(7),
      I1 => count_clk100(7),
      I2 => \^unconn_out\,
      O => adr_bram(7)
    );
\adr_bram[8]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(8),
      I1 => count_clk100(8),
      I2 => \^unconn_out\,
      O => adr_bram(8)
    );
\adr_bram[9]_INST_0\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"AC"
    )
        port map (
      I0 => data_adc_deserial(9),
      I1 => count_clk100(9),
      I2 => \^unconn_out\,
      O => adr_bram(9)
    );
\count_clk100[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_clk100(0),
      O => \count_clk100[0]_i_1_n_0\
    );
\count_clk100[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000001"
    )
        port map (
      I0 => \count_clk100[31]_i_5_n_0\,
      I1 => \count_clk100[31]_i_6_n_0\,
      I2 => \count_clk100[31]_i_7_n_0\,
      I3 => \count_clk100[31]_i_8_n_0\,
      I4 => \^unconn_out\,
      O => \count_clk100[31]_i_1_n_0\
    );
\count_clk100[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk100(28),
      I1 => count_clk100(27),
      I2 => count_clk100(31),
      I3 => count_clk100(29),
      O => \count_clk100[31]_i_10_n_0\
    );
\count_clk100[31]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => count_clk100(4),
      I1 => count_clk100(3),
      I2 => count_clk100(6),
      I3 => count_clk100(5),
      O => \count_clk100[31]_i_11_n_0\
    );
\count_clk100[31]_i_12\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_clk100(11),
      I1 => count_clk100(30),
      I2 => count_clk100(16),
      I3 => count_clk100(17),
      O => \count_clk100[31]_i_12_n_0\
    );
\count_clk100[31]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^unconn_out\,
      O => p_0_in
    );
\count_clk100[31]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_100MHz,
      I1 => s00_axi_aresetn,
      O => count_clk1000
    );
\count_clk100[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => count_clk100(13),
      I1 => count_clk100(18),
      I2 => count_clk100(15),
      I3 => count_clk100(14),
      I4 => \count_clk100[31]_i_9_n_0\,
      O => \count_clk100[31]_i_5_n_0\
    );
\count_clk100[31]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => count_clk100(25),
      I1 => count_clk100(26),
      I2 => count_clk100(23),
      I3 => count_clk100(24),
      I4 => \count_clk100[31]_i_10_n_0\,
      O => \count_clk100[31]_i_6_n_0\
    );
\count_clk100[31]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => count_clk100(1),
      I1 => count_clk100(2),
      I2 => count_clk100(12),
      I3 => count_clk100(0),
      I4 => \count_clk100[31]_i_11_n_0\,
      O => \count_clk100[31]_i_7_n_0\
    );
\count_clk100[31]_i_8\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => count_clk100(9),
      I1 => count_clk100(10),
      I2 => count_clk100(7),
      I3 => count_clk100(8),
      I4 => \count_clk100[31]_i_12_n_0\,
      O => \count_clk100[31]_i_8_n_0\
    );
\count_clk100[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk100(20),
      I1 => count_clk100(19),
      I2 => count_clk100(22),
      I3 => count_clk100(21),
      O => \count_clk100[31]_i_9_n_0\
    );
\count_clk100_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => \count_clk100[0]_i_1_n_0\,
      Q => count_clk100(0),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(10),
      Q => count_clk100(10),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(11),
      Q => count_clk100(11),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(12),
      Q => count_clk100(12),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[8]_i_1_n_0\,
      CO(3) => \count_clk100_reg[12]_i_1_n_0\,
      CO(2) => \count_clk100_reg[12]_i_1_n_1\,
      CO(1) => \count_clk100_reg[12]_i_1_n_2\,
      CO(0) => \count_clk100_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => count_clk100(12 downto 9)
    );
\count_clk100_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(13),
      Q => count_clk100(13),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(14),
      Q => count_clk100(14),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(15),
      Q => count_clk100(15),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(16),
      Q => count_clk100(16),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[12]_i_1_n_0\,
      CO(3) => \count_clk100_reg[16]_i_1_n_0\,
      CO(2) => \count_clk100_reg[16]_i_1_n_1\,
      CO(1) => \count_clk100_reg[16]_i_1_n_2\,
      CO(0) => \count_clk100_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3 downto 0) => count_clk100(16 downto 13)
    );
\count_clk100_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(17),
      Q => count_clk100(17),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(18),
      Q => count_clk100(18),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(19),
      Q => count_clk100(19),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(1),
      Q => count_clk100(1),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(20),
      Q => count_clk100(20),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[16]_i_1_n_0\,
      CO(3) => \count_clk100_reg[20]_i_1_n_0\,
      CO(2) => \count_clk100_reg[20]_i_1_n_1\,
      CO(1) => \count_clk100_reg[20]_i_1_n_2\,
      CO(0) => \count_clk100_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(20 downto 17),
      S(3 downto 0) => count_clk100(20 downto 17)
    );
\count_clk100_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(21),
      Q => count_clk100(21),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(22),
      Q => count_clk100(22),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(23),
      Q => count_clk100(23),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(24),
      Q => count_clk100(24),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[20]_i_1_n_0\,
      CO(3) => \count_clk100_reg[24]_i_1_n_0\,
      CO(2) => \count_clk100_reg[24]_i_1_n_1\,
      CO(1) => \count_clk100_reg[24]_i_1_n_2\,
      CO(0) => \count_clk100_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(24 downto 21),
      S(3 downto 0) => count_clk100(24 downto 21)
    );
\count_clk100_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(25),
      Q => count_clk100(25),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(26),
      Q => count_clk100(26),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(27),
      Q => count_clk100(27),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(28),
      Q => count_clk100(28),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[24]_i_1_n_0\,
      CO(3) => \count_clk100_reg[28]_i_1_n_0\,
      CO(2) => \count_clk100_reg[28]_i_1_n_1\,
      CO(1) => \count_clk100_reg[28]_i_1_n_2\,
      CO(0) => \count_clk100_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(28 downto 25),
      S(3 downto 0) => count_clk100(28 downto 25)
    );
\count_clk100_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(29),
      Q => count_clk100(29),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(2),
      Q => count_clk100(2),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(30),
      Q => count_clk100(30),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(31),
      Q => count_clk100(31),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[31]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[28]_i_1_n_0\,
      CO(3 downto 2) => \NLW_count_clk100_reg[31]_i_3_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count_clk100_reg[31]_i_3_n_2\,
      CO(0) => \count_clk100_reg[31]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_count_clk100_reg[31]_i_3_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => count_clk100(31 downto 29)
    );
\count_clk100_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(3),
      Q => count_clk100(3),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(4),
      Q => count_clk100(4),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_clk100_reg[4]_i_1_n_0\,
      CO(2) => \count_clk100_reg[4]_i_1_n_1\,
      CO(1) => \count_clk100_reg[4]_i_1_n_2\,
      CO(0) => \count_clk100_reg[4]_i_1_n_3\,
      CYINIT => count_clk100(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => count_clk100(4 downto 1)
    );
\count_clk100_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(5),
      Q => count_clk100(5),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(6),
      Q => count_clk100(6),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(7),
      Q => count_clk100(7),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(8),
      Q => count_clk100(8),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk100_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk100_reg[4]_i_1_n_0\,
      CO(3) => \count_clk100_reg[8]_i_1_n_0\,
      CO(2) => \count_clk100_reg[8]_i_1_n_1\,
      CO(1) => \count_clk100_reg[8]_i_1_n_2\,
      CO(0) => \count_clk100_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => count_clk100(8 downto 5)
    );
\count_clk100_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => count_clk1000,
      CE => p_0_in,
      D => data0(9),
      Q => count_clk100(9),
      R => \count_clk100[31]_i_1_n_0\
    );
\count_clk5[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => count_clk5(0),
      O => count_clk5_0(0)
    );
\count_clk5[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => write_reg_i_3_n_0,
      I1 => write_reg_i_4_n_0,
      I2 => write_reg_i_5_n_0,
      I3 => write_reg_i_6_n_0,
      O => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5_0(0),
      Q => count_clk5(0),
      R => '0'
    );
\count_clk5_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[12]_i_1_n_6\,
      Q => count_clk5(10),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[12]_i_1_n_5\,
      Q => count_clk5(11),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[12]_i_1_n_4\,
      Q => count_clk5(12),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[8]_i_1_n_0\,
      CO(3) => \count_clk5_reg[12]_i_1_n_0\,
      CO(2) => \count_clk5_reg[12]_i_1_n_1\,
      CO(1) => \count_clk5_reg[12]_i_1_n_2\,
      CO(0) => \count_clk5_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[12]_i_1_n_4\,
      O(2) => \count_clk5_reg[12]_i_1_n_5\,
      O(1) => \count_clk5_reg[12]_i_1_n_6\,
      O(0) => \count_clk5_reg[12]_i_1_n_7\,
      S(3 downto 0) => count_clk5(12 downto 9)
    );
\count_clk5_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[16]_i_1_n_7\,
      Q => count_clk5(13),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[16]_i_1_n_6\,
      Q => count_clk5(14),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[16]_i_1_n_5\,
      Q => count_clk5(15),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[16]_i_1_n_4\,
      Q => count_clk5(16),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[12]_i_1_n_0\,
      CO(3) => \count_clk5_reg[16]_i_1_n_0\,
      CO(2) => \count_clk5_reg[16]_i_1_n_1\,
      CO(1) => \count_clk5_reg[16]_i_1_n_2\,
      CO(0) => \count_clk5_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[16]_i_1_n_4\,
      O(2) => \count_clk5_reg[16]_i_1_n_5\,
      O(1) => \count_clk5_reg[16]_i_1_n_6\,
      O(0) => \count_clk5_reg[16]_i_1_n_7\,
      S(3 downto 0) => count_clk5(16 downto 13)
    );
\count_clk5_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[20]_i_1_n_7\,
      Q => count_clk5(17),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[20]_i_1_n_6\,
      Q => count_clk5(18),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[20]_i_1_n_5\,
      Q => count_clk5(19),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[4]_i_1_n_7\,
      Q => count_clk5(1),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[20]_i_1_n_4\,
      Q => count_clk5(20),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[16]_i_1_n_0\,
      CO(3) => \count_clk5_reg[20]_i_1_n_0\,
      CO(2) => \count_clk5_reg[20]_i_1_n_1\,
      CO(1) => \count_clk5_reg[20]_i_1_n_2\,
      CO(0) => \count_clk5_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[20]_i_1_n_4\,
      O(2) => \count_clk5_reg[20]_i_1_n_5\,
      O(1) => \count_clk5_reg[20]_i_1_n_6\,
      O(0) => \count_clk5_reg[20]_i_1_n_7\,
      S(3 downto 0) => count_clk5(20 downto 17)
    );
\count_clk5_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[24]_i_1_n_7\,
      Q => count_clk5(21),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[24]_i_1_n_6\,
      Q => count_clk5(22),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[24]_i_1_n_5\,
      Q => count_clk5(23),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[24]_i_1_n_4\,
      Q => count_clk5(24),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[20]_i_1_n_0\,
      CO(3) => \count_clk5_reg[24]_i_1_n_0\,
      CO(2) => \count_clk5_reg[24]_i_1_n_1\,
      CO(1) => \count_clk5_reg[24]_i_1_n_2\,
      CO(0) => \count_clk5_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[24]_i_1_n_4\,
      O(2) => \count_clk5_reg[24]_i_1_n_5\,
      O(1) => \count_clk5_reg[24]_i_1_n_6\,
      O(0) => \count_clk5_reg[24]_i_1_n_7\,
      S(3 downto 0) => count_clk5(24 downto 21)
    );
\count_clk5_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[28]_i_1_n_7\,
      Q => count_clk5(25),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[28]_i_1_n_6\,
      Q => count_clk5(26),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[28]_i_1_n_5\,
      Q => count_clk5(27),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[28]_i_1_n_4\,
      Q => count_clk5(28),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[24]_i_1_n_0\,
      CO(3) => \count_clk5_reg[28]_i_1_n_0\,
      CO(2) => \count_clk5_reg[28]_i_1_n_1\,
      CO(1) => \count_clk5_reg[28]_i_1_n_2\,
      CO(0) => \count_clk5_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[28]_i_1_n_4\,
      O(2) => \count_clk5_reg[28]_i_1_n_5\,
      O(1) => \count_clk5_reg[28]_i_1_n_6\,
      O(0) => \count_clk5_reg[28]_i_1_n_7\,
      S(3 downto 0) => count_clk5(28 downto 25)
    );
\count_clk5_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[31]_i_2_n_7\,
      Q => count_clk5(29),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[4]_i_1_n_6\,
      Q => count_clk5(2),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[31]_i_2_n_6\,
      Q => count_clk5(30),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[31]_i_2_n_5\,
      Q => count_clk5(31),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[28]_i_1_n_0\,
      CO(3 downto 2) => \NLW_count_clk5_reg[31]_i_2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count_clk5_reg[31]_i_2_n_2\,
      CO(0) => \count_clk5_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_count_clk5_reg[31]_i_2_O_UNCONNECTED\(3),
      O(2) => \count_clk5_reg[31]_i_2_n_5\,
      O(1) => \count_clk5_reg[31]_i_2_n_6\,
      O(0) => \count_clk5_reg[31]_i_2_n_7\,
      S(3) => '0',
      S(2 downto 0) => count_clk5(31 downto 29)
    );
\count_clk5_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[4]_i_1_n_5\,
      Q => count_clk5(3),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[4]_i_1_n_4\,
      Q => count_clk5(4),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_clk5_reg[4]_i_1_n_0\,
      CO(2) => \count_clk5_reg[4]_i_1_n_1\,
      CO(1) => \count_clk5_reg[4]_i_1_n_2\,
      CO(0) => \count_clk5_reg[4]_i_1_n_3\,
      CYINIT => count_clk5(0),
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[4]_i_1_n_4\,
      O(2) => \count_clk5_reg[4]_i_1_n_5\,
      O(1) => \count_clk5_reg[4]_i_1_n_6\,
      O(0) => \count_clk5_reg[4]_i_1_n_7\,
      S(3 downto 0) => count_clk5(4 downto 1)
    );
\count_clk5_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[8]_i_1_n_7\,
      Q => count_clk5(5),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[8]_i_1_n_6\,
      Q => count_clk5(6),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[8]_i_1_n_5\,
      Q => count_clk5(7),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[8]_i_1_n_4\,
      Q => count_clk5(8),
      R => \count_clk5[31]_i_1_n_0\
    );
\count_clk5_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk5_reg[4]_i_1_n_0\,
      CO(3) => \count_clk5_reg[8]_i_1_n_0\,
      CO(2) => \count_clk5_reg[8]_i_1_n_1\,
      CO(1) => \count_clk5_reg[8]_i_1_n_2\,
      CO(0) => \count_clk5_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \count_clk5_reg[8]_i_1_n_4\,
      O(2) => \count_clk5_reg[8]_i_1_n_5\,
      O(1) => \count_clk5_reg[8]_i_1_n_6\,
      O(0) => \count_clk5_reg[8]_i_1_n_7\,
      S(3 downto 0) => count_clk5(8 downto 5)
    );
\count_clk5_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => \count_clk5_reg[12]_i_1_n_7\,
      Q => count_clk5(9),
      R => \count_clk5[31]_i_1_n_0\
    );
\data[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(0),
      O => data(0)
    );
\data[10]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(10),
      O => data(10)
    );
\data[11]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(11),
      O => data(11)
    );
\data[12]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(12),
      O => data(12)
    );
\data[13]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(13),
      O => data(13)
    );
\data[14]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(14),
      O => data(14)
    );
\data[15]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(15),
      O => data(15)
    );
\data[16]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(16),
      O => data(16)
    );
\data[17]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(17),
      O => data(17)
    );
\data[18]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(18),
      O => data(18)
    );
\data[19]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(19),
      O => data(19)
    );
\data[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(1),
      O => data(1)
    );
\data[20]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(20),
      O => data(20)
    );
\data[21]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(21),
      O => data(21)
    );
\data[22]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(22),
      O => data(22)
    );
\data[23]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(23),
      O => data(23)
    );
\data[24]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(24),
      O => data(24)
    );
\data[25]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(25),
      O => data(25)
    );
\data[26]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(26),
      O => data(26)
    );
\data[27]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(27),
      O => data(27)
    );
\data[28]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(28),
      O => data(28)
    );
\data[29]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(29),
      O => data(29)
    );
\data[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(2),
      O => data(2)
    );
\data[30]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(30),
      O => data(30)
    );
\data[31]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(31),
      O => data(31)
    );
\data[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(3),
      O => data(3)
    );
\data[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(4),
      O => data(4)
    );
\data[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(5),
      O => data(5)
    );
\data[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(6),
      O => data(6)
    );
\data[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(7),
      O => data(7)
    );
\data[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(8),
      O => data(8)
    );
\data[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^unconn_out\,
      I1 => data_adc_deserial(9),
      O => data(9)
    );
\data_adc_deserial_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(0),
      Q => data_adc_deserial(0),
      R => '0'
    );
\data_adc_deserial_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(10),
      Q => data_adc_deserial(10),
      R => '0'
    );
\data_adc_deserial_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(11),
      Q => data_adc_deserial(11),
      R => '0'
    );
\data_adc_deserial_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(12),
      Q => data_adc_deserial(12),
      R => '0'
    );
\data_adc_deserial_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(13),
      Q => data_adc_deserial(13),
      R => '0'
    );
\data_adc_deserial_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(14),
      Q => data_adc_deserial(14),
      R => '0'
    );
\data_adc_deserial_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(15),
      Q => data_adc_deserial(15),
      R => '0'
    );
\data_adc_deserial_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(16),
      Q => data_adc_deserial(16),
      R => '0'
    );
\data_adc_deserial_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(17),
      Q => data_adc_deserial(17),
      R => '0'
    );
\data_adc_deserial_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(18),
      Q => data_adc_deserial(18),
      R => '0'
    );
\data_adc_deserial_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(19),
      Q => data_adc_deserial(19),
      R => '0'
    );
\data_adc_deserial_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(1),
      Q => data_adc_deserial(1),
      R => '0'
    );
\data_adc_deserial_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(20),
      Q => data_adc_deserial(20),
      R => '0'
    );
\data_adc_deserial_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(21),
      Q => data_adc_deserial(21),
      R => '0'
    );
\data_adc_deserial_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(22),
      Q => data_adc_deserial(22),
      R => '0'
    );
\data_adc_deserial_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(23),
      Q => data_adc_deserial(23),
      R => '0'
    );
\data_adc_deserial_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(24),
      Q => data_adc_deserial(24),
      R => '0'
    );
\data_adc_deserial_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(25),
      Q => data_adc_deserial(25),
      R => '0'
    );
\data_adc_deserial_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(26),
      Q => data_adc_deserial(26),
      R => '0'
    );
\data_adc_deserial_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(27),
      Q => data_adc_deserial(27),
      R => '0'
    );
\data_adc_deserial_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(28),
      Q => data_adc_deserial(28),
      R => '0'
    );
\data_adc_deserial_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(29),
      Q => data_adc_deserial(29),
      R => '0'
    );
\data_adc_deserial_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(2),
      Q => data_adc_deserial(2),
      R => '0'
    );
\data_adc_deserial_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(30),
      Q => data_adc_deserial(30),
      R => '0'
    );
\data_adc_deserial_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(31),
      Q => data_adc_deserial(31),
      R => '0'
    );
\data_adc_deserial_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(3),
      Q => data_adc_deserial(3),
      R => '0'
    );
\data_adc_deserial_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(4),
      Q => data_adc_deserial(4),
      R => '0'
    );
\data_adc_deserial_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(5),
      Q => data_adc_deserial(5),
      R => '0'
    );
\data_adc_deserial_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(6),
      Q => data_adc_deserial(6),
      R => '0'
    );
\data_adc_deserial_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(7),
      Q => data_adc_deserial(7),
      R => '0'
    );
\data_adc_deserial_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(8),
      Q => data_adc_deserial(8),
      R => '0'
    );
\data_adc_deserial_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => count_clk5(9),
      Q => data_adc_deserial(9),
      R => '0'
    );
write_reg_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => write_reg_i_3_n_0,
      I1 => write_reg_i_4_n_0,
      I2 => write_reg_i_5_n_0,
      I3 => write_reg_i_6_n_0,
      I4 => \^unconn_out\,
      O => write_reg_i_1_n_0
    );
write_reg_i_10: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => count_clk5(12),
      I1 => count_clk5(13),
      I2 => count_clk5(15),
      I3 => count_clk5(14),
      O => write_reg_i_10_n_0
    );
write_reg_i_2: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_5MHz,
      I1 => s00_axi_aresetn,
      O => data_adc_deserial0
    );
write_reg_i_3: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => count_clk5(18),
      I1 => count_clk5(19),
      I2 => count_clk5(16),
      I3 => count_clk5(17),
      I4 => write_reg_i_7_n_0,
      O => write_reg_i_3_n_0
    );
write_reg_i_4: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => count_clk5(26),
      I1 => count_clk5(27),
      I2 => count_clk5(24),
      I3 => count_clk5(25),
      I4 => write_reg_i_8_n_0,
      O => write_reg_i_4_n_0
    );
write_reg_i_5: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => count_clk5(2),
      I1 => count_clk5(3),
      I2 => count_clk5(0),
      I3 => count_clk5(1),
      I4 => write_reg_i_9_n_0,
      O => write_reg_i_5_n_0
    );
write_reg_i_6: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => count_clk5(10),
      I1 => count_clk5(11),
      I2 => count_clk5(8),
      I3 => count_clk5(9),
      I4 => write_reg_i_10_n_0,
      O => write_reg_i_6_n_0
    );
write_reg_i_7: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk5(21),
      I1 => count_clk5(20),
      I2 => count_clk5(23),
      I3 => count_clk5(22),
      O => write_reg_i_7_n_0
    );
write_reg_i_8: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => count_clk5(29),
      I1 => count_clk5(28),
      I2 => count_clk5(31),
      I3 => count_clk5(30),
      O => write_reg_i_8_n_0
    );
write_reg_i_9: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => count_clk5(5),
      I1 => count_clk5(4),
      I2 => count_clk5(7),
      I3 => count_clk5(6),
      O => write_reg_i_9_n_0
    );
write_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => data_adc_deserial0,
      CE => '1',
      D => write_reg_i_1_n_0,
      Q => \^unconn_out\,
      R => '0'
    );
\write_reg_reg__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => last_word_transmit,
      CE => '1',
      D => '1',
      Q => \^unconn_out\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_5MHz : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    last_word_transmit : in STD_LOGIC;
    adr_read_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_5MHzWR_100MhzRD : out STD_LOGIC;
    clk_100Mhz_AXI_send : out STD_LOGIC;
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    write : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_imitation_0_0,ADC_imitation,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_imitation,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^write\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  write <= \^write\;
clk_100Mhz_AXI_send_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^write\,
      I1 => clk_100MHz,
      O => clk_100Mhz_AXI_send
    );
clk_5MHzWR_100MhzRD_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => clk_5MHz,
      I1 => \^write\,
      I2 => clk_100MHz,
      O => clk_5MHzWR_100MhzRD
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
     port map (
      UNCONN_OUT => \^write\,
      adr_bram(31 downto 0) => adr_bram(31 downto 0),
      clk_100MHz => clk_100MHz,
      clk_5MHz => clk_5MHz,
      data(31 downto 0) => data(31 downto 0),
      last_word_transmit => last_word_transmit,
      s00_axi_aresetn => s00_axi_aresetn
    );
end STRUCTURE;
