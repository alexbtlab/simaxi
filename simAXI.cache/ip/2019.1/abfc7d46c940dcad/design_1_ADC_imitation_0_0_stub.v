// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Jun  4 18:07:12 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_stub.v
// Design      : design_1_ADC_imitation_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ADC_imitation,Vivado 2019.1" *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix(clk_5MHz, clk_100MHz, s00_axi_aresetn, 
  last_word_transmit, adr_read_ram, clk_5MHzWR_100MhzRD, clk_100Mhz_AXI_send, adr_bram, data, 
  write)
/* synthesis syn_black_box black_box_pad_pin="clk_5MHz,clk_100MHz,s00_axi_aresetn,last_word_transmit,adr_read_ram[31:0],clk_5MHzWR_100MhzRD,clk_100Mhz_AXI_send,adr_bram[31:0],data[31:0],write" */;
  input clk_5MHz;
  input clk_100MHz;
  input s00_axi_aresetn;
  input last_word_transmit;
  input [31:0]adr_read_ram;
  output clk_5MHzWR_100MhzRD;
  output clk_100Mhz_AXI_send;
  output [31:0]adr_bram;
  output [31:0]data;
  output write;
endmodule
