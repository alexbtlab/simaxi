-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jun  7 20:00:38 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_sampleGenerator_0_0_sim_netlist.vhdl
-- Design      : design_1_sampleGenerator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    main_clk_100MHz : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  signal clear : STD_LOGIC;
  signal m00_axis_tlast_INST_0_i_1_n_0 : STD_LOGIC;
  signal m00_axis_tlast_INST_0_i_2_n_0 : STD_LOGIC;
  signal m00_axis_tlast_INST_0_i_3_n_0 : STD_LOGIC;
  signal m00_axis_tlast_INST_0_i_4_n_0 : STD_LOGIC;
  signal m00_axis_tlast_INST_0_i_5_n_0 : STD_LOGIC;
  signal m00_axis_tlast_INST_0_i_6_n_0 : STD_LOGIC;
  signal \wordCounter0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \wordCounter0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \wordCounter0_carry__0_n_0\ : STD_LOGIC;
  signal \wordCounter0_carry__0_n_1\ : STD_LOGIC;
  signal \wordCounter0_carry__0_n_2\ : STD_LOGIC;
  signal \wordCounter0_carry__0_n_3\ : STD_LOGIC;
  signal \wordCounter0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \wordCounter0_carry__1_n_2\ : STD_LOGIC;
  signal \wordCounter0_carry__1_n_3\ : STD_LOGIC;
  signal wordCounter0_carry_i_1_n_0 : STD_LOGIC;
  signal wordCounter0_carry_i_2_n_0 : STD_LOGIC;
  signal wordCounter0_carry_i_3_n_0 : STD_LOGIC;
  signal wordCounter0_carry_i_4_n_0 : STD_LOGIC;
  signal wordCounter0_carry_n_0 : STD_LOGIC;
  signal wordCounter0_carry_n_1 : STD_LOGIC;
  signal wordCounter0_carry_n_2 : STD_LOGIC;
  signal wordCounter0_carry_n_3 : STD_LOGIC;
  signal wordCounter1 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \wordCounter1_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__0_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__0_n_1\ : STD_LOGIC;
  signal \wordCounter1_carry__0_n_2\ : STD_LOGIC;
  signal \wordCounter1_carry__0_n_3\ : STD_LOGIC;
  signal \wordCounter1_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__1_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__1_n_1\ : STD_LOGIC;
  signal \wordCounter1_carry__1_n_2\ : STD_LOGIC;
  signal \wordCounter1_carry__1_n_3\ : STD_LOGIC;
  signal \wordCounter1_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__2_n_0\ : STD_LOGIC;
  signal \wordCounter1_carry__2_n_2\ : STD_LOGIC;
  signal \wordCounter1_carry__2_n_3\ : STD_LOGIC;
  signal wordCounter1_carry_i_1_n_0 : STD_LOGIC;
  signal wordCounter1_carry_i_2_n_0 : STD_LOGIC;
  signal wordCounter1_carry_i_3_n_0 : STD_LOGIC;
  signal wordCounter1_carry_i_4_n_0 : STD_LOGIC;
  signal wordCounter1_carry_n_0 : STD_LOGIC;
  signal wordCounter1_carry_n_1 : STD_LOGIC;
  signal wordCounter1_carry_n_2 : STD_LOGIC;
  signal wordCounter1_carry_n_3 : STD_LOGIC;
  signal \wordCounter[0]_i_2_n_0\ : STD_LOGIC;
  signal wordCounter_reg : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \wordCounter_reg[0]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[16]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[20]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[20]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[20]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[20]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[24]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[24]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[24]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[24]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[28]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[28]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[28]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[28]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal NLW_wordCounter0_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wordCounter0_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wordCounter0_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wordCounter0_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wordCounter1_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_wordCounter1_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_wordCounter_reg[28]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
\M_AXIS_TDATA_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(0),
      Q => m00_axis_tdata(0),
      R => '0'
    );
\M_AXIS_TDATA_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(10),
      Q => m00_axis_tdata(10),
      R => '0'
    );
\M_AXIS_TDATA_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(11),
      Q => m00_axis_tdata(11),
      R => '0'
    );
\M_AXIS_TDATA_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(12),
      Q => m00_axis_tdata(12),
      R => '0'
    );
\M_AXIS_TDATA_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(13),
      Q => m00_axis_tdata(13),
      R => '0'
    );
\M_AXIS_TDATA_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(14),
      Q => m00_axis_tdata(14),
      R => '0'
    );
\M_AXIS_TDATA_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(15),
      Q => m00_axis_tdata(15),
      R => '0'
    );
\M_AXIS_TDATA_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(16),
      Q => m00_axis_tdata(16),
      R => '0'
    );
\M_AXIS_TDATA_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(17),
      Q => m00_axis_tdata(17),
      R => '0'
    );
\M_AXIS_TDATA_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(18),
      Q => m00_axis_tdata(18),
      R => '0'
    );
\M_AXIS_TDATA_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(19),
      Q => m00_axis_tdata(19),
      R => '0'
    );
\M_AXIS_TDATA_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(1),
      Q => m00_axis_tdata(1),
      R => '0'
    );
\M_AXIS_TDATA_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(20),
      Q => m00_axis_tdata(20),
      R => '0'
    );
\M_AXIS_TDATA_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(21),
      Q => m00_axis_tdata(21),
      R => '0'
    );
\M_AXIS_TDATA_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(22),
      Q => m00_axis_tdata(22),
      R => '0'
    );
\M_AXIS_TDATA_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(23),
      Q => m00_axis_tdata(23),
      R => '0'
    );
\M_AXIS_TDATA_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(24),
      Q => m00_axis_tdata(24),
      R => '0'
    );
\M_AXIS_TDATA_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(25),
      Q => m00_axis_tdata(25),
      R => '0'
    );
\M_AXIS_TDATA_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(26),
      Q => m00_axis_tdata(26),
      R => '0'
    );
\M_AXIS_TDATA_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(27),
      Q => m00_axis_tdata(27),
      R => '0'
    );
\M_AXIS_TDATA_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(28),
      Q => m00_axis_tdata(28),
      R => '0'
    );
\M_AXIS_TDATA_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(29),
      Q => m00_axis_tdata(29),
      R => '0'
    );
\M_AXIS_TDATA_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(2),
      Q => m00_axis_tdata(2),
      R => '0'
    );
\M_AXIS_TDATA_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(30),
      Q => m00_axis_tdata(30),
      R => '0'
    );
\M_AXIS_TDATA_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(31),
      Q => m00_axis_tdata(31),
      R => '0'
    );
\M_AXIS_TDATA_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(3),
      Q => m00_axis_tdata(3),
      R => '0'
    );
\M_AXIS_TDATA_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(4),
      Q => m00_axis_tdata(4),
      R => '0'
    );
\M_AXIS_TDATA_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(5),
      Q => m00_axis_tdata(5),
      R => '0'
    );
\M_AXIS_TDATA_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(6),
      Q => m00_axis_tdata(6),
      R => '0'
    );
\M_AXIS_TDATA_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(7),
      Q => m00_axis_tdata(7),
      R => '0'
    );
\M_AXIS_TDATA_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(8),
      Q => m00_axis_tdata(8),
      R => '0'
    );
\M_AXIS_TDATA_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => main_clk_100MHz,
      CE => '1',
      D => wordCounter_reg(9),
      Q => m00_axis_tdata(9),
      R => '0'
    );
m00_axis_tlast_INST_0: unisim.vcomponents.LUT6
    generic map(
      INIT => X"1000000000000000"
    )
        port map (
      I0 => wordCounter_reg(1),
      I1 => wordCounter_reg(0),
      I2 => m00_axis_tlast_INST_0_i_1_n_0,
      I3 => m00_axis_tlast_INST_0_i_2_n_0,
      I4 => m00_axis_tlast_INST_0_i_3_n_0,
      I5 => m00_axis_tlast_INST_0_i_4_n_0,
      O => m00_axis_tlast
    );
m00_axis_tlast_INST_0_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => wordCounter_reg(4),
      I1 => wordCounter_reg(5),
      I2 => wordCounter_reg(2),
      I3 => wordCounter_reg(3),
      I4 => wordCounter_reg(7),
      I5 => wordCounter_reg(6),
      O => m00_axis_tlast_INST_0_i_1_n_0
    );
m00_axis_tlast_INST_0_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0001000000000000"
    )
        port map (
      I0 => wordCounter_reg(18),
      I1 => wordCounter_reg(19),
      I2 => wordCounter_reg(15),
      I3 => wordCounter_reg(14),
      I4 => m00_axis_tlast_INST_0_i_5_n_0,
      I5 => m00_axis_tlast_INST_0_i_6_n_0,
      O => m00_axis_tlast_INST_0_i_2_n_0
    );
m00_axis_tlast_INST_0_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => wordCounter_reg(22),
      I1 => wordCounter_reg(23),
      I2 => wordCounter_reg(20),
      I3 => wordCounter_reg(21),
      I4 => wordCounter_reg(25),
      I5 => wordCounter_reg(24),
      O => m00_axis_tlast_INST_0_i_3_n_0
    );
m00_axis_tlast_INST_0_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => wordCounter_reg(28),
      I1 => wordCounter_reg(29),
      I2 => wordCounter_reg(26),
      I3 => wordCounter_reg(27),
      I4 => wordCounter_reg(31),
      I5 => wordCounter_reg(30),
      O => m00_axis_tlast_INST_0_i_4_n_0
    );
m00_axis_tlast_INST_0_i_5: unisim.vcomponents.LUT2
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wordCounter_reg(16),
      I1 => wordCounter_reg(17),
      O => m00_axis_tlast_INST_0_i_5_n_0
    );
m00_axis_tlast_INST_0_i_6: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => wordCounter_reg(10),
      I1 => wordCounter_reg(11),
      I2 => wordCounter_reg(8),
      I3 => wordCounter_reg(9),
      I4 => wordCounter_reg(13),
      I5 => wordCounter_reg(12),
      O => m00_axis_tlast_INST_0_i_6_n_0
    );
wordCounter0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => wordCounter0_carry_n_0,
      CO(2) => wordCounter0_carry_n_1,
      CO(1) => wordCounter0_carry_n_2,
      CO(0) => wordCounter0_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_wordCounter0_carry_O_UNCONNECTED(3 downto 0),
      S(3) => wordCounter0_carry_i_1_n_0,
      S(2) => wordCounter0_carry_i_2_n_0,
      S(1) => wordCounter0_carry_i_3_n_0,
      S(0) => wordCounter0_carry_i_4_n_0
    );
\wordCounter0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => wordCounter0_carry_n_0,
      CO(3) => \wordCounter0_carry__0_n_0\,
      CO(2) => \wordCounter0_carry__0_n_1\,
      CO(1) => \wordCounter0_carry__0_n_2\,
      CO(0) => \wordCounter0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_wordCounter0_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \wordCounter0_carry__0_i_1_n_0\,
      S(2) => \wordCounter0_carry__0_i_2_n_0\,
      S(1) => \wordCounter0_carry__0_i_3_n_0\,
      S(0) => \wordCounter0_carry__0_i_4_n_0\
    );
\wordCounter0_carry__0_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0810"
    )
        port map (
      I0 => wordCounter_reg(22),
      I1 => wordCounter_reg(23),
      I2 => \wordCounter1_carry__2_n_0\,
      I3 => wordCounter_reg(21),
      O => \wordCounter0_carry__0_i_1_n_0\
    );
\wordCounter0_carry__0_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0810"
    )
        port map (
      I0 => wordCounter_reg(19),
      I1 => wordCounter_reg(20),
      I2 => \wordCounter1_carry__2_n_0\,
      I3 => wordCounter_reg(18),
      O => \wordCounter0_carry__0_i_2_n_0\
    );
\wordCounter0_carry__0_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00099000"
    )
        port map (
      I0 => wordCounter1(15),
      I1 => wordCounter_reg(15),
      I2 => wordCounter_reg(17),
      I3 => wordCounter_reg(16),
      I4 => \wordCounter1_carry__2_n_0\,
      O => \wordCounter0_carry__0_i_3_n_0\
    );
\wordCounter0_carry__0_i_4\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter1(14),
      I1 => wordCounter_reg(14),
      I2 => wordCounter1(13),
      I3 => wordCounter_reg(13),
      I4 => wordCounter_reg(12),
      I5 => wordCounter1(12),
      O => \wordCounter0_carry__0_i_4_n_0\
    );
\wordCounter0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter0_carry__0_n_0\,
      CO(3) => \NLW_wordCounter0_carry__1_CO_UNCONNECTED\(3),
      CO(2) => clear,
      CO(1) => \wordCounter0_carry__1_n_2\,
      CO(0) => \wordCounter0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_wordCounter0_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \wordCounter0_carry__1_i_1_n_0\,
      S(1) => \wordCounter0_carry__1_i_2_n_0\,
      S(0) => \wordCounter0_carry__1_i_3_n_0\
    );
\wordCounter0_carry__1_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"42"
    )
        port map (
      I0 => \wordCounter1_carry__2_n_0\,
      I1 => wordCounter_reg(30),
      I2 => wordCounter_reg(31),
      O => \wordCounter0_carry__1_i_1_n_0\
    );
\wordCounter0_carry__1_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0810"
    )
        port map (
      I0 => wordCounter_reg(28),
      I1 => wordCounter_reg(29),
      I2 => \wordCounter1_carry__2_n_0\,
      I3 => wordCounter_reg(27),
      O => \wordCounter0_carry__1_i_2_n_0\
    );
\wordCounter0_carry__1_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0810"
    )
        port map (
      I0 => wordCounter_reg(25),
      I1 => wordCounter_reg(26),
      I2 => \wordCounter1_carry__2_n_0\,
      I3 => wordCounter_reg(24),
      O => \wordCounter0_carry__1_i_3_n_0\
    );
wordCounter0_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter1(11),
      I1 => wordCounter_reg(11),
      I2 => wordCounter1(10),
      I3 => wordCounter_reg(10),
      I4 => wordCounter_reg(9),
      I5 => wordCounter1(9),
      O => wordCounter0_carry_i_1_n_0
    );
wordCounter0_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter1(8),
      I1 => wordCounter_reg(8),
      I2 => wordCounter1(7),
      I3 => wordCounter_reg(7),
      I4 => wordCounter_reg(6),
      I5 => wordCounter1(6),
      O => wordCounter0_carry_i_2_n_0
    );
wordCounter0_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter1(5),
      I1 => wordCounter_reg(5),
      I2 => wordCounter1(4),
      I3 => wordCounter_reg(4),
      I4 => wordCounter_reg(3),
      I5 => wordCounter1(3),
      O => wordCounter0_carry_i_3_n_0
    );
wordCounter0_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => wordCounter_reg(0),
      I1 => FrameSize(0),
      I2 => wordCounter1(2),
      I3 => wordCounter_reg(2),
      I4 => wordCounter_reg(1),
      I5 => wordCounter1(1),
      O => wordCounter0_carry_i_4_n_0
    );
wordCounter1_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => wordCounter1_carry_n_0,
      CO(2) => wordCounter1_carry_n_1,
      CO(1) => wordCounter1_carry_n_2,
      CO(0) => wordCounter1_carry_n_3,
      CYINIT => FrameSize(0),
      DI(3 downto 0) => FrameSize(4 downto 1),
      O(3 downto 0) => wordCounter1(4 downto 1),
      S(3) => wordCounter1_carry_i_1_n_0,
      S(2) => wordCounter1_carry_i_2_n_0,
      S(1) => wordCounter1_carry_i_3_n_0,
      S(0) => wordCounter1_carry_i_4_n_0
    );
\wordCounter1_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => wordCounter1_carry_n_0,
      CO(3) => \wordCounter1_carry__0_n_0\,
      CO(2) => \wordCounter1_carry__0_n_1\,
      CO(1) => \wordCounter1_carry__0_n_2\,
      CO(0) => \wordCounter1_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => FrameSize(8 downto 5),
      O(3 downto 0) => wordCounter1(8 downto 5),
      S(3) => \wordCounter1_carry__0_i_1_n_0\,
      S(2) => \wordCounter1_carry__0_i_2_n_0\,
      S(1) => \wordCounter1_carry__0_i_3_n_0\,
      S(0) => \wordCounter1_carry__0_i_4_n_0\
    );
\wordCounter1_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(8),
      O => \wordCounter1_carry__0_i_1_n_0\
    );
\wordCounter1_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(7),
      O => \wordCounter1_carry__0_i_2_n_0\
    );
\wordCounter1_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(6),
      O => \wordCounter1_carry__0_i_3_n_0\
    );
\wordCounter1_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(5),
      O => \wordCounter1_carry__0_i_4_n_0\
    );
\wordCounter1_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter1_carry__0_n_0\,
      CO(3) => \wordCounter1_carry__1_n_0\,
      CO(2) => \wordCounter1_carry__1_n_1\,
      CO(1) => \wordCounter1_carry__1_n_2\,
      CO(0) => \wordCounter1_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => FrameSize(12 downto 9),
      O(3 downto 0) => wordCounter1(12 downto 9),
      S(3) => \wordCounter1_carry__1_i_1_n_0\,
      S(2) => \wordCounter1_carry__1_i_2_n_0\,
      S(1) => \wordCounter1_carry__1_i_3_n_0\,
      S(0) => \wordCounter1_carry__1_i_4_n_0\
    );
\wordCounter1_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(12),
      O => \wordCounter1_carry__1_i_1_n_0\
    );
\wordCounter1_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(11),
      O => \wordCounter1_carry__1_i_2_n_0\
    );
\wordCounter1_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(10),
      O => \wordCounter1_carry__1_i_3_n_0\
    );
\wordCounter1_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(9),
      O => \wordCounter1_carry__1_i_4_n_0\
    );
\wordCounter1_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter1_carry__1_n_0\,
      CO(3) => \wordCounter1_carry__2_n_0\,
      CO(2) => \NLW_wordCounter1_carry__2_CO_UNCONNECTED\(2),
      CO(1) => \wordCounter1_carry__2_n_2\,
      CO(0) => \wordCounter1_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => FrameSize(15 downto 13),
      O(3) => \NLW_wordCounter1_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => wordCounter1(15 downto 13),
      S(3) => '1',
      S(2) => \wordCounter1_carry__2_i_1_n_0\,
      S(1) => \wordCounter1_carry__2_i_2_n_0\,
      S(0) => \wordCounter1_carry__2_i_3_n_0\
    );
\wordCounter1_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(15),
      O => \wordCounter1_carry__2_i_1_n_0\
    );
\wordCounter1_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(14),
      O => \wordCounter1_carry__2_i_2_n_0\
    );
\wordCounter1_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(13),
      O => \wordCounter1_carry__2_i_3_n_0\
    );
wordCounter1_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(4),
      O => wordCounter1_carry_i_1_n_0
    );
wordCounter1_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(3),
      O => wordCounter1_carry_i_2_n_0
    );
wordCounter1_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(2),
      O => wordCounter1_carry_i_3_n_0
    );
wordCounter1_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(1),
      O => wordCounter1_carry_i_4_n_0
    );
\wordCounter[0]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wordCounter_reg(0),
      O => \wordCounter[0]_i_2_n_0\
    );
\wordCounter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[0]_i_1_n_7\,
      Q => wordCounter_reg(0),
      R => clear
    );
\wordCounter_reg[0]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wordCounter_reg[0]_i_1_n_0\,
      CO(2) => \wordCounter_reg[0]_i_1_n_1\,
      CO(1) => \wordCounter_reg[0]_i_1_n_2\,
      CO(0) => \wordCounter_reg[0]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wordCounter_reg[0]_i_1_n_4\,
      O(2) => \wordCounter_reg[0]_i_1_n_5\,
      O(1) => \wordCounter_reg[0]_i_1_n_6\,
      O(0) => \wordCounter_reg[0]_i_1_n_7\,
      S(3 downto 1) => wordCounter_reg(3 downto 1),
      S(0) => \wordCounter[0]_i_2_n_0\
    );
\wordCounter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[8]_i_1_n_5\,
      Q => wordCounter_reg(10),
      R => clear
    );
\wordCounter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[8]_i_1_n_4\,
      Q => wordCounter_reg(11),
      R => clear
    );
\wordCounter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[12]_i_1_n_7\,
      Q => wordCounter_reg(12),
      R => clear
    );
\wordCounter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[8]_i_1_n_0\,
      CO(3) => \wordCounter_reg[12]_i_1_n_0\,
      CO(2) => \wordCounter_reg[12]_i_1_n_1\,
      CO(1) => \wordCounter_reg[12]_i_1_n_2\,
      CO(0) => \wordCounter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[12]_i_1_n_4\,
      O(2) => \wordCounter_reg[12]_i_1_n_5\,
      O(1) => \wordCounter_reg[12]_i_1_n_6\,
      O(0) => \wordCounter_reg[12]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(15 downto 12)
    );
\wordCounter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[12]_i_1_n_6\,
      Q => wordCounter_reg(13),
      R => clear
    );
\wordCounter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[12]_i_1_n_5\,
      Q => wordCounter_reg(14),
      R => clear
    );
\wordCounter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[12]_i_1_n_4\,
      Q => wordCounter_reg(15),
      R => clear
    );
\wordCounter_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[16]_i_1_n_7\,
      Q => wordCounter_reg(16),
      R => clear
    );
\wordCounter_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[12]_i_1_n_0\,
      CO(3) => \wordCounter_reg[16]_i_1_n_0\,
      CO(2) => \wordCounter_reg[16]_i_1_n_1\,
      CO(1) => \wordCounter_reg[16]_i_1_n_2\,
      CO(0) => \wordCounter_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[16]_i_1_n_4\,
      O(2) => \wordCounter_reg[16]_i_1_n_5\,
      O(1) => \wordCounter_reg[16]_i_1_n_6\,
      O(0) => \wordCounter_reg[16]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(19 downto 16)
    );
\wordCounter_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[16]_i_1_n_6\,
      Q => wordCounter_reg(17),
      R => clear
    );
\wordCounter_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[16]_i_1_n_5\,
      Q => wordCounter_reg(18),
      R => clear
    );
\wordCounter_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[16]_i_1_n_4\,
      Q => wordCounter_reg(19),
      R => clear
    );
\wordCounter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[0]_i_1_n_6\,
      Q => wordCounter_reg(1),
      R => clear
    );
\wordCounter_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[20]_i_1_n_7\,
      Q => wordCounter_reg(20),
      R => clear
    );
\wordCounter_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[16]_i_1_n_0\,
      CO(3) => \wordCounter_reg[20]_i_1_n_0\,
      CO(2) => \wordCounter_reg[20]_i_1_n_1\,
      CO(1) => \wordCounter_reg[20]_i_1_n_2\,
      CO(0) => \wordCounter_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[20]_i_1_n_4\,
      O(2) => \wordCounter_reg[20]_i_1_n_5\,
      O(1) => \wordCounter_reg[20]_i_1_n_6\,
      O(0) => \wordCounter_reg[20]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(23 downto 20)
    );
\wordCounter_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[20]_i_1_n_6\,
      Q => wordCounter_reg(21),
      R => clear
    );
\wordCounter_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[20]_i_1_n_5\,
      Q => wordCounter_reg(22),
      R => clear
    );
\wordCounter_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[20]_i_1_n_4\,
      Q => wordCounter_reg(23),
      R => clear
    );
\wordCounter_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[24]_i_1_n_7\,
      Q => wordCounter_reg(24),
      R => clear
    );
\wordCounter_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[20]_i_1_n_0\,
      CO(3) => \wordCounter_reg[24]_i_1_n_0\,
      CO(2) => \wordCounter_reg[24]_i_1_n_1\,
      CO(1) => \wordCounter_reg[24]_i_1_n_2\,
      CO(0) => \wordCounter_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[24]_i_1_n_4\,
      O(2) => \wordCounter_reg[24]_i_1_n_5\,
      O(1) => \wordCounter_reg[24]_i_1_n_6\,
      O(0) => \wordCounter_reg[24]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(27 downto 24)
    );
\wordCounter_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[24]_i_1_n_6\,
      Q => wordCounter_reg(25),
      R => clear
    );
\wordCounter_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[24]_i_1_n_5\,
      Q => wordCounter_reg(26),
      R => clear
    );
\wordCounter_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[24]_i_1_n_4\,
      Q => wordCounter_reg(27),
      R => clear
    );
\wordCounter_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[28]_i_1_n_7\,
      Q => wordCounter_reg(28),
      R => clear
    );
\wordCounter_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[24]_i_1_n_0\,
      CO(3) => \NLW_wordCounter_reg[28]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \wordCounter_reg[28]_i_1_n_1\,
      CO(1) => \wordCounter_reg[28]_i_1_n_2\,
      CO(0) => \wordCounter_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[28]_i_1_n_4\,
      O(2) => \wordCounter_reg[28]_i_1_n_5\,
      O(1) => \wordCounter_reg[28]_i_1_n_6\,
      O(0) => \wordCounter_reg[28]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(31 downto 28)
    );
\wordCounter_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[28]_i_1_n_6\,
      Q => wordCounter_reg(29),
      R => clear
    );
\wordCounter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[0]_i_1_n_5\,
      Q => wordCounter_reg(2),
      R => clear
    );
\wordCounter_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[28]_i_1_n_5\,
      Q => wordCounter_reg(30),
      R => clear
    );
\wordCounter_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[28]_i_1_n_4\,
      Q => wordCounter_reg(31),
      R => clear
    );
\wordCounter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[0]_i_1_n_4\,
      Q => wordCounter_reg(3),
      R => clear
    );
\wordCounter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[4]_i_1_n_7\,
      Q => wordCounter_reg(4),
      R => clear
    );
\wordCounter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[0]_i_1_n_0\,
      CO(3) => \wordCounter_reg[4]_i_1_n_0\,
      CO(2) => \wordCounter_reg[4]_i_1_n_1\,
      CO(1) => \wordCounter_reg[4]_i_1_n_2\,
      CO(0) => \wordCounter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[4]_i_1_n_4\,
      O(2) => \wordCounter_reg[4]_i_1_n_5\,
      O(1) => \wordCounter_reg[4]_i_1_n_6\,
      O(0) => \wordCounter_reg[4]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(7 downto 4)
    );
\wordCounter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[4]_i_1_n_6\,
      Q => wordCounter_reg(5),
      R => clear
    );
\wordCounter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[4]_i_1_n_5\,
      Q => wordCounter_reg(6),
      R => clear
    );
\wordCounter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[4]_i_1_n_4\,
      Q => wordCounter_reg(7),
      R => clear
    );
\wordCounter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[8]_i_1_n_7\,
      Q => wordCounter_reg(8),
      R => clear
    );
\wordCounter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[4]_i_1_n_0\,
      CO(3) => \wordCounter_reg[8]_i_1_n_0\,
      CO(2) => \wordCounter_reg[8]_i_1_n_1\,
      CO(1) => \wordCounter_reg[8]_i_1_n_2\,
      CO(0) => \wordCounter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[8]_i_1_n_4\,
      O(2) => \wordCounter_reg[8]_i_1_n_5\,
      O(1) => \wordCounter_reg[8]_i_1_n_6\,
      O(0) => \wordCounter_reg[8]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(11 downto 8)
    );
\wordCounter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => main_clk_100MHz,
      CE => '1',
      D => \wordCounter_reg[8]_i_1_n_6\,
      Q => wordCounter_reg(9),
      R => clear
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    main_clk_100MHz : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
begin
samplGeneraror_v1_0_M00_AXIS_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      main_clk_100MHz => main_clk_100MHz
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    en_AXIclk : in STD_LOGIC;
    main_clk_100MHz : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_sampleGenerator_0_0,sampleGenerator_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sampleGenerator_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
  m00_axis_tvalid <= \<const1>\;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      main_clk_100MHz => main_clk_100MHz
    );
end STRUCTURE;
