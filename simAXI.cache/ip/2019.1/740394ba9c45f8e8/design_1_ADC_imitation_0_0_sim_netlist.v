// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Thu Jun  4 10:27:41 2020
// Host        : zl-04 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.v
// Design      : design_1_ADC_imitation_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
   (data,
    write,
    clk_5MHz,
    s00_axi_aresetn);
  output [31:0]data;
  output write;
  input clk_5MHz;
  input s00_axi_aresetn;

  wire clk_5MHz;
  wire [31:0]count_clk;
  wire count_clk0_carry__0_n_0;
  wire count_clk0_carry__0_n_1;
  wire count_clk0_carry__0_n_2;
  wire count_clk0_carry__0_n_3;
  wire count_clk0_carry__1_n_0;
  wire count_clk0_carry__1_n_1;
  wire count_clk0_carry__1_n_2;
  wire count_clk0_carry__1_n_3;
  wire count_clk0_carry__2_n_0;
  wire count_clk0_carry__2_n_1;
  wire count_clk0_carry__2_n_2;
  wire count_clk0_carry__2_n_3;
  wire count_clk0_carry__3_n_0;
  wire count_clk0_carry__3_n_1;
  wire count_clk0_carry__3_n_2;
  wire count_clk0_carry__3_n_3;
  wire count_clk0_carry__4_n_0;
  wire count_clk0_carry__4_n_1;
  wire count_clk0_carry__4_n_2;
  wire count_clk0_carry__4_n_3;
  wire count_clk0_carry__5_n_0;
  wire count_clk0_carry__5_n_1;
  wire count_clk0_carry__5_n_2;
  wire count_clk0_carry__5_n_3;
  wire count_clk0_carry__6_n_2;
  wire count_clk0_carry__6_n_3;
  wire count_clk0_carry_n_0;
  wire count_clk0_carry_n_1;
  wire count_clk0_carry_n_2;
  wire count_clk0_carry_n_3;
  wire \count_clk[31]_i_1_n_0 ;
  wire [0:0]count_clk_0;
  wire [31:0]data;
  wire [31:1]data0;
  wire \data_adc_deserial[31]_i_1_n_0 ;
  wire s00_axi_aresetn;
  wire write;
  wire write_reg_i_1_n_0;
  wire write_reg_i_2_n_0;
  wire write_reg_i_3_n_0;
  wire write_reg_i_4_n_0;
  wire write_reg_i_5_n_0;
  wire write_reg_i_6_n_0;
  wire write_reg_i_7_n_0;
  wire write_reg_i_8_n_0;
  wire write_reg_i_9_n_0;
  wire [3:2]NLW_count_clk0_carry__6_CO_UNCONNECTED;
  wire [3:3]NLW_count_clk0_carry__6_O_UNCONNECTED;

  CARRY4 count_clk0_carry
       (.CI(1'b0),
        .CO({count_clk0_carry_n_0,count_clk0_carry_n_1,count_clk0_carry_n_2,count_clk0_carry_n_3}),
        .CYINIT(count_clk[0]),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[4:1]),
        .S(count_clk[4:1]));
  CARRY4 count_clk0_carry__0
       (.CI(count_clk0_carry_n_0),
        .CO({count_clk0_carry__0_n_0,count_clk0_carry__0_n_1,count_clk0_carry__0_n_2,count_clk0_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[8:5]),
        .S(count_clk[8:5]));
  CARRY4 count_clk0_carry__1
       (.CI(count_clk0_carry__0_n_0),
        .CO({count_clk0_carry__1_n_0,count_clk0_carry__1_n_1,count_clk0_carry__1_n_2,count_clk0_carry__1_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[12:9]),
        .S(count_clk[12:9]));
  CARRY4 count_clk0_carry__2
       (.CI(count_clk0_carry__1_n_0),
        .CO({count_clk0_carry__2_n_0,count_clk0_carry__2_n_1,count_clk0_carry__2_n_2,count_clk0_carry__2_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[16:13]),
        .S(count_clk[16:13]));
  CARRY4 count_clk0_carry__3
       (.CI(count_clk0_carry__2_n_0),
        .CO({count_clk0_carry__3_n_0,count_clk0_carry__3_n_1,count_clk0_carry__3_n_2,count_clk0_carry__3_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[20:17]),
        .S(count_clk[20:17]));
  CARRY4 count_clk0_carry__4
       (.CI(count_clk0_carry__3_n_0),
        .CO({count_clk0_carry__4_n_0,count_clk0_carry__4_n_1,count_clk0_carry__4_n_2,count_clk0_carry__4_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[24:21]),
        .S(count_clk[24:21]));
  CARRY4 count_clk0_carry__5
       (.CI(count_clk0_carry__4_n_0),
        .CO({count_clk0_carry__5_n_0,count_clk0_carry__5_n_1,count_clk0_carry__5_n_2,count_clk0_carry__5_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[28:25]),
        .S(count_clk[28:25]));
  CARRY4 count_clk0_carry__6
       (.CI(count_clk0_carry__5_n_0),
        .CO({NLW_count_clk0_carry__6_CO_UNCONNECTED[3:2],count_clk0_carry__6_n_2,count_clk0_carry__6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_count_clk0_carry__6_O_UNCONNECTED[3],data0[31:29]}),
        .S({1'b0,count_clk[31:29]}));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h00FE)) 
    \count_clk[0]_i_1 
       (.I0(write_reg_i_4_n_0),
        .I1(write_reg_i_3_n_0),
        .I2(write_reg_i_2_n_0),
        .I3(count_clk[0]),
        .O(count_clk_0));
  LUT4 #(
    .INIT(16'h0001)) 
    \count_clk[31]_i_1 
       (.I0(count_clk[0]),
        .I1(write_reg_i_2_n_0),
        .I2(write_reg_i_3_n_0),
        .I3(write_reg_i_4_n_0),
        .O(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[0] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk_0),
        .Q(count_clk[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[10] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[10]),
        .Q(count_clk[10]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[11] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[11]),
        .Q(count_clk[11]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[12] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[12]),
        .Q(count_clk[12]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[13] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[13]),
        .Q(count_clk[13]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[14] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[14]),
        .Q(count_clk[14]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[15] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[15]),
        .Q(count_clk[15]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[16] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[16]),
        .Q(count_clk[16]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[17] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[17]),
        .Q(count_clk[17]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[18] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[18]),
        .Q(count_clk[18]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[19] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[19]),
        .Q(count_clk[19]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[1] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[1]),
        .Q(count_clk[1]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[20] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[20]),
        .Q(count_clk[20]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[21] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[21]),
        .Q(count_clk[21]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[22] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[22]),
        .Q(count_clk[22]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[23] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[23]),
        .Q(count_clk[23]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[24] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[24]),
        .Q(count_clk[24]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[25] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[25]),
        .Q(count_clk[25]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[26] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[26]),
        .Q(count_clk[26]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[27] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[27]),
        .Q(count_clk[27]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[28] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[28]),
        .Q(count_clk[28]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[29] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[29]),
        .Q(count_clk[29]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[2] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[2]),
        .Q(count_clk[2]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[30] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[30]),
        .Q(count_clk[30]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[31] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[31]),
        .Q(count_clk[31]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[3] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[3]),
        .Q(count_clk[3]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[4] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[4]),
        .Q(count_clk[4]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[5] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[5]),
        .Q(count_clk[5]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[6] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[6]),
        .Q(count_clk[6]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[7] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[7]),
        .Q(count_clk[7]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[8] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[8]),
        .Q(count_clk[8]),
        .R(\count_clk[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \count_clk_reg[9] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(data0[9]),
        .Q(count_clk[9]),
        .R(\count_clk[31]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h8)) 
    \data_adc_deserial[31]_i_1 
       (.I0(clk_5MHz),
        .I1(s00_axi_aresetn),
        .O(\data_adc_deserial[31]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[0] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[0]),
        .Q(data[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[10] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[10]),
        .Q(data[10]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[11] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[11]),
        .Q(data[11]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[12] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[12]),
        .Q(data[12]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[13] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[13]),
        .Q(data[13]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[14] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[14]),
        .Q(data[14]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[15] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[15]),
        .Q(data[15]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[16] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[16]),
        .Q(data[16]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[17] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[17]),
        .Q(data[17]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[18] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[18]),
        .Q(data[18]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[19] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[19]),
        .Q(data[19]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[1] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[1]),
        .Q(data[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[20] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[20]),
        .Q(data[20]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[21] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[21]),
        .Q(data[21]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[22] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[22]),
        .Q(data[22]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[23] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[23]),
        .Q(data[23]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[24] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[24]),
        .Q(data[24]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[25] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[25]),
        .Q(data[25]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[26] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[26]),
        .Q(data[26]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[27] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[27]),
        .Q(data[27]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[28] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[28]),
        .Q(data[28]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[29] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[29]),
        .Q(data[29]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[2] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[2]),
        .Q(data[2]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[30] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[30]),
        .Q(data[30]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[31] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[31]),
        .Q(data[31]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[3] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[3]),
        .Q(data[3]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[4] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[4]),
        .Q(data[4]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[5] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[5]),
        .Q(data[5]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[6] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[6]),
        .Q(data[6]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[7] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[7]),
        .Q(data[7]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[8] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[8]),
        .Q(data[8]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \data_adc_deserial_reg[9] 
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(count_clk[9]),
        .Q(data[9]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFE0000)) 
    write_reg_i_1
       (.I0(count_clk[0]),
        .I1(write_reg_i_2_n_0),
        .I2(write_reg_i_3_n_0),
        .I3(write_reg_i_4_n_0),
        .I4(write),
        .O(write_reg_i_1_n_0));
  LUT5 #(
    .INIT(32'hFFFFBFFF)) 
    write_reg_i_2
       (.I0(count_clk[13]),
        .I1(count_clk[12]),
        .I2(count_clk[10]),
        .I3(count_clk[11]),
        .I4(write_reg_i_5_n_0),
        .O(write_reg_i_2_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    write_reg_i_3
       (.I0(count_clk[4]),
        .I1(count_clk[5]),
        .I2(count_clk[2]),
        .I3(count_clk[3]),
        .I4(write_reg_i_6_n_0),
        .O(write_reg_i_3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    write_reg_i_4
       (.I0(write_reg_i_7_n_0),
        .I1(write_reg_i_8_n_0),
        .I2(count_clk[31]),
        .I3(count_clk[30]),
        .I4(count_clk[1]),
        .I5(write_reg_i_9_n_0),
        .O(write_reg_i_4_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    write_reg_i_5
       (.I0(count_clk[15]),
        .I1(count_clk[14]),
        .I2(count_clk[17]),
        .I3(count_clk[16]),
        .O(write_reg_i_5_n_0));
  LUT4 #(
    .INIT(16'hDFFF)) 
    write_reg_i_6
       (.I0(count_clk[6]),
        .I1(count_clk[7]),
        .I2(count_clk[9]),
        .I3(count_clk[8]),
        .O(write_reg_i_6_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    write_reg_i_7
       (.I0(count_clk[23]),
        .I1(count_clk[22]),
        .I2(count_clk[25]),
        .I3(count_clk[24]),
        .O(write_reg_i_7_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    write_reg_i_8
       (.I0(count_clk[19]),
        .I1(count_clk[18]),
        .I2(count_clk[21]),
        .I3(count_clk[20]),
        .O(write_reg_i_8_n_0));
  LUT4 #(
    .INIT(16'hFFFE)) 
    write_reg_i_9
       (.I0(count_clk[27]),
        .I1(count_clk[26]),
        .I2(count_clk[29]),
        .I3(count_clk[28]),
        .O(write_reg_i_9_n_0));
  FDRE #(
    .INIT(1'b1)) 
    write_reg_reg
       (.C(\data_adc_deserial[31]_i_1_n_0 ),
        .CE(1'b1),
        .D(write_reg_i_1_n_0),
        .Q(write),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC_imitation_0_0,ADC_imitation,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ADC_imitation,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_5MHz,
    s00_axi_aresetn,
    adr_bram,
    data,
    write);
  input clk_5MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  output [31:0]adr_bram;
  output [31:0]data;
  output write;

  wire clk_5MHz;
  wire [31:0]data;
  wire s00_axi_aresetn;
  wire write;

  assign adr_bram[31:0] = data;
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation inst
       (.clk_5MHz(clk_5MHz),
        .data(data),
        .s00_axi_aresetn(s00_axi_aresetn),
        .write(write));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
