-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sun Jun  7 15:38:26 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_sampleGenerator_0_0_sim_netlist.vhdl
-- Design      : design_1_sampleGenerator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  port (
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    en_AXIclk_0 : out STD_LOGIC;
    en_AXIclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  signal \M_AXIS_TDATA[31]_i_1_n_0\ : STD_LOGIC;
  signal M_AXIS_TLAST0 : STD_LOGIC_VECTOR ( 15 downto 1 );
  signal \M_AXIS_TLAST0_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__0_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__0_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__0_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__0_n_1\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__0_n_2\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__0_n_3\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__1_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__1_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__1_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__1_i_4_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__1_n_1\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__1_n_2\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__1_n_3\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__2_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__2_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__2_i_3_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__2_n_2\ : STD_LOGIC;
  signal \M_AXIS_TLAST0_carry__2_n_3\ : STD_LOGIC;
  signal M_AXIS_TLAST0_carry_i_1_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST0_carry_i_2_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST0_carry_i_3_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST0_carry_i_4_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST0_carry_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST0_carry_n_1 : STD_LOGIC;
  signal M_AXIS_TLAST0_carry_n_2 : STD_LOGIC;
  signal M_AXIS_TLAST0_carry_n_3 : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_i_1_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_i_2_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_0\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_1\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_2\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__0_n_3\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__1_n_2\ : STD_LOGIC;
  signal \M_AXIS_TLAST_carry__1_n_3\ : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_1_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_2_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_3_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_i_4_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_0 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_1 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_2 : STD_LOGIC;
  signal M_AXIS_TLAST_carry_n_3 : STD_LOGIC;
  signal \^en_axiclk_0\ : STD_LOGIC;
  signal \^m00_axis_tlast\ : STD_LOGIC;
  signal \wordCounter[0]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter[0]_i_3_n_0\ : STD_LOGIC;
  signal wordCounter_reg : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal \wordCounter_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_2_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_2_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_2_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_2_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_2_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_2_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[0]_i_2_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \wordCounter_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \NLW_M_AXIS_TLAST0_carry__2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 2 to 2 );
  signal \NLW_M_AXIS_TLAST0_carry__2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal NLW_M_AXIS_TLAST_carry_O_UNCONNECTED : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_M_AXIS_TLAST_carry__1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  signal \NLW_M_AXIS_TLAST_carry__1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal \NLW_wordCounter_reg[12]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
begin
  en_AXIclk_0 <= \^en_axiclk_0\;
  m00_axis_tlast <= \^m00_axis_tlast\;
\M_AXIS_TDATA[31]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"2"
    )
        port map (
      I0 => m00_axis_aresetn,
      I1 => en_AXIclk,
      O => \M_AXIS_TDATA[31]_i_1_n_0\
    );
\M_AXIS_TDATA_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(0),
      Q => m00_axis_tdata(0),
      R => '0'
    );
\M_AXIS_TDATA_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(10),
      Q => m00_axis_tdata(10),
      R => '0'
    );
\M_AXIS_TDATA_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(11),
      Q => m00_axis_tdata(11),
      R => '0'
    );
\M_AXIS_TDATA_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(12),
      Q => m00_axis_tdata(12),
      R => '0'
    );
\M_AXIS_TDATA_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(13),
      Q => m00_axis_tdata(13),
      R => '0'
    );
\M_AXIS_TDATA_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(14),
      Q => m00_axis_tdata(14),
      R => '0'
    );
\M_AXIS_TDATA_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(15),
      Q => m00_axis_tdata(15),
      R => '0'
    );
\M_AXIS_TDATA_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(16),
      Q => m00_axis_tdata(16),
      R => '0'
    );
\M_AXIS_TDATA_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(17),
      Q => m00_axis_tdata(17),
      R => '0'
    );
\M_AXIS_TDATA_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(18),
      Q => m00_axis_tdata(18),
      R => '0'
    );
\M_AXIS_TDATA_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(19),
      Q => m00_axis_tdata(19),
      R => '0'
    );
\M_AXIS_TDATA_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(1),
      Q => m00_axis_tdata(1),
      R => '0'
    );
\M_AXIS_TDATA_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(20),
      Q => m00_axis_tdata(20),
      R => '0'
    );
\M_AXIS_TDATA_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(21),
      Q => m00_axis_tdata(21),
      R => '0'
    );
\M_AXIS_TDATA_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(22),
      Q => m00_axis_tdata(22),
      R => '0'
    );
\M_AXIS_TDATA_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(23),
      Q => m00_axis_tdata(23),
      R => '0'
    );
\M_AXIS_TDATA_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(24),
      Q => m00_axis_tdata(24),
      R => '0'
    );
\M_AXIS_TDATA_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(25),
      Q => m00_axis_tdata(25),
      R => '0'
    );
\M_AXIS_TDATA_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(26),
      Q => m00_axis_tdata(26),
      R => '0'
    );
\M_AXIS_TDATA_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(27),
      Q => m00_axis_tdata(27),
      R => '0'
    );
\M_AXIS_TDATA_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(28),
      Q => m00_axis_tdata(28),
      R => '0'
    );
\M_AXIS_TDATA_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(29),
      Q => m00_axis_tdata(29),
      R => '0'
    );
\M_AXIS_TDATA_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(2),
      Q => m00_axis_tdata(2),
      R => '0'
    );
\M_AXIS_TDATA_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(30),
      Q => m00_axis_tdata(30),
      R => '0'
    );
\M_AXIS_TDATA_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(31),
      Q => m00_axis_tdata(31),
      R => '0'
    );
\M_AXIS_TDATA_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(3),
      Q => m00_axis_tdata(3),
      R => '0'
    );
\M_AXIS_TDATA_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(4),
      Q => m00_axis_tdata(4),
      R => '0'
    );
\M_AXIS_TDATA_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(5),
      Q => m00_axis_tdata(5),
      R => '0'
    );
\M_AXIS_TDATA_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(6),
      Q => m00_axis_tdata(6),
      R => '0'
    );
\M_AXIS_TDATA_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(7),
      Q => m00_axis_tdata(7),
      R => '0'
    );
\M_AXIS_TDATA_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(8),
      Q => m00_axis_tdata(8),
      R => '0'
    );
\M_AXIS_TDATA_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \M_AXIS_TDATA[31]_i_1_n_0\,
      D => data_from_ram(9),
      Q => m00_axis_tdata(9),
      R => '0'
    );
M_AXIS_TLAST0_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => M_AXIS_TLAST0_carry_n_0,
      CO(2) => M_AXIS_TLAST0_carry_n_1,
      CO(1) => M_AXIS_TLAST0_carry_n_2,
      CO(0) => M_AXIS_TLAST0_carry_n_3,
      CYINIT => FrameSize(0),
      DI(3 downto 0) => FrameSize(4 downto 1),
      O(3 downto 0) => M_AXIS_TLAST0(4 downto 1),
      S(3) => M_AXIS_TLAST0_carry_i_1_n_0,
      S(2) => M_AXIS_TLAST0_carry_i_2_n_0,
      S(1) => M_AXIS_TLAST0_carry_i_3_n_0,
      S(0) => M_AXIS_TLAST0_carry_i_4_n_0
    );
\M_AXIS_TLAST0_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => M_AXIS_TLAST0_carry_n_0,
      CO(3) => \M_AXIS_TLAST0_carry__0_n_0\,
      CO(2) => \M_AXIS_TLAST0_carry__0_n_1\,
      CO(1) => \M_AXIS_TLAST0_carry__0_n_2\,
      CO(0) => \M_AXIS_TLAST0_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => FrameSize(8 downto 5),
      O(3 downto 0) => M_AXIS_TLAST0(8 downto 5),
      S(3) => \M_AXIS_TLAST0_carry__0_i_1_n_0\,
      S(2) => \M_AXIS_TLAST0_carry__0_i_2_n_0\,
      S(1) => \M_AXIS_TLAST0_carry__0_i_3_n_0\,
      S(0) => \M_AXIS_TLAST0_carry__0_i_4_n_0\
    );
\M_AXIS_TLAST0_carry__0_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(8),
      O => \M_AXIS_TLAST0_carry__0_i_1_n_0\
    );
\M_AXIS_TLAST0_carry__0_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(7),
      O => \M_AXIS_TLAST0_carry__0_i_2_n_0\
    );
\M_AXIS_TLAST0_carry__0_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(6),
      O => \M_AXIS_TLAST0_carry__0_i_3_n_0\
    );
\M_AXIS_TLAST0_carry__0_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(5),
      O => \M_AXIS_TLAST0_carry__0_i_4_n_0\
    );
\M_AXIS_TLAST0_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXIS_TLAST0_carry__0_n_0\,
      CO(3) => \M_AXIS_TLAST0_carry__1_n_0\,
      CO(2) => \M_AXIS_TLAST0_carry__1_n_1\,
      CO(1) => \M_AXIS_TLAST0_carry__1_n_2\,
      CO(0) => \M_AXIS_TLAST0_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => FrameSize(12 downto 9),
      O(3 downto 0) => M_AXIS_TLAST0(12 downto 9),
      S(3) => \M_AXIS_TLAST0_carry__1_i_1_n_0\,
      S(2) => \M_AXIS_TLAST0_carry__1_i_2_n_0\,
      S(1) => \M_AXIS_TLAST0_carry__1_i_3_n_0\,
      S(0) => \M_AXIS_TLAST0_carry__1_i_4_n_0\
    );
\M_AXIS_TLAST0_carry__1_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(12),
      O => \M_AXIS_TLAST0_carry__1_i_1_n_0\
    );
\M_AXIS_TLAST0_carry__1_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(11),
      O => \M_AXIS_TLAST0_carry__1_i_2_n_0\
    );
\M_AXIS_TLAST0_carry__1_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(10),
      O => \M_AXIS_TLAST0_carry__1_i_3_n_0\
    );
\M_AXIS_TLAST0_carry__1_i_4\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(9),
      O => \M_AXIS_TLAST0_carry__1_i_4_n_0\
    );
\M_AXIS_TLAST0_carry__2\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXIS_TLAST0_carry__1_n_0\,
      CO(3) => \M_AXIS_TLAST0_carry__2_n_0\,
      CO(2) => \NLW_M_AXIS_TLAST0_carry__2_CO_UNCONNECTED\(2),
      CO(1) => \M_AXIS_TLAST0_carry__2_n_2\,
      CO(0) => \M_AXIS_TLAST0_carry__2_n_3\,
      CYINIT => '0',
      DI(3) => '0',
      DI(2 downto 0) => FrameSize(15 downto 13),
      O(3) => \NLW_M_AXIS_TLAST0_carry__2_O_UNCONNECTED\(3),
      O(2 downto 0) => M_AXIS_TLAST0(15 downto 13),
      S(3) => '1',
      S(2) => \M_AXIS_TLAST0_carry__2_i_1_n_0\,
      S(1) => \M_AXIS_TLAST0_carry__2_i_2_n_0\,
      S(0) => \M_AXIS_TLAST0_carry__2_i_3_n_0\
    );
\M_AXIS_TLAST0_carry__2_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(15),
      O => \M_AXIS_TLAST0_carry__2_i_1_n_0\
    );
\M_AXIS_TLAST0_carry__2_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(14),
      O => \M_AXIS_TLAST0_carry__2_i_2_n_0\
    );
\M_AXIS_TLAST0_carry__2_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(13),
      O => \M_AXIS_TLAST0_carry__2_i_3_n_0\
    );
M_AXIS_TLAST0_carry_i_1: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(4),
      O => M_AXIS_TLAST0_carry_i_1_n_0
    );
M_AXIS_TLAST0_carry_i_2: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(3),
      O => M_AXIS_TLAST0_carry_i_2_n_0
    );
M_AXIS_TLAST0_carry_i_3: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(2),
      O => M_AXIS_TLAST0_carry_i_3_n_0
    );
M_AXIS_TLAST0_carry_i_4: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => FrameSize(1),
      O => M_AXIS_TLAST0_carry_i_4_n_0
    );
M_AXIS_TLAST_carry: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => M_AXIS_TLAST_carry_n_0,
      CO(2) => M_AXIS_TLAST_carry_n_1,
      CO(1) => M_AXIS_TLAST_carry_n_2,
      CO(0) => M_AXIS_TLAST_carry_n_3,
      CYINIT => '1',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => NLW_M_AXIS_TLAST_carry_O_UNCONNECTED(3 downto 0),
      S(3) => M_AXIS_TLAST_carry_i_1_n_0,
      S(2) => M_AXIS_TLAST_carry_i_2_n_0,
      S(1) => M_AXIS_TLAST_carry_i_3_n_0,
      S(0) => M_AXIS_TLAST_carry_i_4_n_0
    );
\M_AXIS_TLAST_carry__0\: unisim.vcomponents.CARRY4
     port map (
      CI => M_AXIS_TLAST_carry_n_0,
      CO(3) => \M_AXIS_TLAST_carry__0_n_0\,
      CO(2) => \M_AXIS_TLAST_carry__0_n_1\,
      CO(1) => \M_AXIS_TLAST_carry__0_n_2\,
      CO(0) => \M_AXIS_TLAST_carry__0_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED\(3 downto 0),
      S(3) => \M_AXIS_TLAST0_carry__2_n_0\,
      S(2) => \M_AXIS_TLAST0_carry__2_n_0\,
      S(1) => \M_AXIS_TLAST_carry__0_i_1_n_0\,
      S(0) => \M_AXIS_TLAST_carry__0_i_2_n_0\
    );
\M_AXIS_TLAST_carry__0_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => wordCounter_reg(15),
      I1 => M_AXIS_TLAST0(15),
      I2 => \M_AXIS_TLAST0_carry__2_n_0\,
      O => \M_AXIS_TLAST_carry__0_i_1_n_0\
    );
\M_AXIS_TLAST_carry__0_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter_reg(12),
      I1 => M_AXIS_TLAST0(12),
      I2 => M_AXIS_TLAST0(14),
      I3 => wordCounter_reg(14),
      I4 => M_AXIS_TLAST0(13),
      I5 => wordCounter_reg(13),
      O => \M_AXIS_TLAST_carry__0_i_2_n_0\
    );
\M_AXIS_TLAST_carry__1\: unisim.vcomponents.CARRY4
     port map (
      CI => \M_AXIS_TLAST_carry__0_n_0\,
      CO(3) => \NLW_M_AXIS_TLAST_carry__1_CO_UNCONNECTED\(3),
      CO(2) => \^m00_axis_tlast\,
      CO(1) => \M_AXIS_TLAST_carry__1_n_2\,
      CO(0) => \M_AXIS_TLAST_carry__1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => \NLW_M_AXIS_TLAST_carry__1_O_UNCONNECTED\(3 downto 0),
      S(3) => '0',
      S(2) => \M_AXIS_TLAST0_carry__2_n_0\,
      S(1) => \M_AXIS_TLAST0_carry__2_n_0\,
      S(0) => \M_AXIS_TLAST0_carry__2_n_0\
    );
M_AXIS_TLAST_carry_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter_reg(9),
      I1 => M_AXIS_TLAST0(9),
      I2 => M_AXIS_TLAST0(11),
      I3 => wordCounter_reg(11),
      I4 => M_AXIS_TLAST0(10),
      I5 => wordCounter_reg(10),
      O => M_AXIS_TLAST_carry_i_1_n_0
    );
M_AXIS_TLAST_carry_i_2: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter_reg(6),
      I1 => M_AXIS_TLAST0(6),
      I2 => M_AXIS_TLAST0(8),
      I3 => wordCounter_reg(8),
      I4 => M_AXIS_TLAST0(7),
      I5 => wordCounter_reg(7),
      O => M_AXIS_TLAST_carry_i_2_n_0
    );
M_AXIS_TLAST_carry_i_3: unisim.vcomponents.LUT6
    generic map(
      INIT => X"9009000000009009"
    )
        port map (
      I0 => wordCounter_reg(3),
      I1 => M_AXIS_TLAST0(3),
      I2 => M_AXIS_TLAST0(5),
      I3 => wordCounter_reg(5),
      I4 => M_AXIS_TLAST0(4),
      I5 => wordCounter_reg(4),
      O => M_AXIS_TLAST_carry_i_3_n_0
    );
M_AXIS_TLAST_carry_i_4: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6006000000006006"
    )
        port map (
      I0 => wordCounter_reg(0),
      I1 => FrameSize(0),
      I2 => M_AXIS_TLAST0(2),
      I3 => wordCounter_reg(2),
      I4 => M_AXIS_TLAST0(1),
      I5 => wordCounter_reg(1),
      O => M_AXIS_TLAST_carry_i_4_n_0
    );
m00_axis_tvalid_INST_0: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => en_AXIclk,
      O => \^en_axiclk_0\
    );
\wordCounter[0]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"4F"
    )
        port map (
      I0 => en_AXIclk,
      I1 => \^m00_axis_tlast\,
      I2 => m00_axis_aresetn,
      O => \wordCounter[0]_i_1_n_0\
    );
\wordCounter[0]_i_3\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => wordCounter_reg(0),
      O => \wordCounter[0]_i_3_n_0\
    );
\wordCounter_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[0]_i_2_n_7\,
      Q => wordCounter_reg(0),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[0]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \wordCounter_reg[0]_i_2_n_0\,
      CO(2) => \wordCounter_reg[0]_i_2_n_1\,
      CO(1) => \wordCounter_reg[0]_i_2_n_2\,
      CO(0) => \wordCounter_reg[0]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \wordCounter_reg[0]_i_2_n_4\,
      O(2) => \wordCounter_reg[0]_i_2_n_5\,
      O(1) => \wordCounter_reg[0]_i_2_n_6\,
      O(0) => \wordCounter_reg[0]_i_2_n_7\,
      S(3 downto 1) => wordCounter_reg(3 downto 1),
      S(0) => \wordCounter[0]_i_3_n_0\
    );
\wordCounter_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[8]_i_1_n_5\,
      Q => wordCounter_reg(10),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[8]_i_1_n_4\,
      Q => wordCounter_reg(11),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[12]_i_1_n_7\,
      Q => wordCounter_reg(12),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[8]_i_1_n_0\,
      CO(3) => \NLW_wordCounter_reg[12]_i_1_CO_UNCONNECTED\(3),
      CO(2) => \wordCounter_reg[12]_i_1_n_1\,
      CO(1) => \wordCounter_reg[12]_i_1_n_2\,
      CO(0) => \wordCounter_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[12]_i_1_n_4\,
      O(2) => \wordCounter_reg[12]_i_1_n_5\,
      O(1) => \wordCounter_reg[12]_i_1_n_6\,
      O(0) => \wordCounter_reg[12]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(15 downto 12)
    );
\wordCounter_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[12]_i_1_n_6\,
      Q => wordCounter_reg(13),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[12]_i_1_n_5\,
      Q => wordCounter_reg(14),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[12]_i_1_n_4\,
      Q => wordCounter_reg(15),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[0]_i_2_n_6\,
      Q => wordCounter_reg(1),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[0]_i_2_n_5\,
      Q => wordCounter_reg(2),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[0]_i_2_n_4\,
      Q => wordCounter_reg(3),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[4]_i_1_n_7\,
      Q => wordCounter_reg(4),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[0]_i_2_n_0\,
      CO(3) => \wordCounter_reg[4]_i_1_n_0\,
      CO(2) => \wordCounter_reg[4]_i_1_n_1\,
      CO(1) => \wordCounter_reg[4]_i_1_n_2\,
      CO(0) => \wordCounter_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[4]_i_1_n_4\,
      O(2) => \wordCounter_reg[4]_i_1_n_5\,
      O(1) => \wordCounter_reg[4]_i_1_n_6\,
      O(0) => \wordCounter_reg[4]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(7 downto 4)
    );
\wordCounter_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[4]_i_1_n_6\,
      Q => wordCounter_reg(5),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[4]_i_1_n_5\,
      Q => wordCounter_reg(6),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[4]_i_1_n_4\,
      Q => wordCounter_reg(7),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[8]_i_1_n_7\,
      Q => wordCounter_reg(8),
      R => \wordCounter[0]_i_1_n_0\
    );
\wordCounter_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \wordCounter_reg[4]_i_1_n_0\,
      CO(3) => \wordCounter_reg[8]_i_1_n_0\,
      CO(2) => \wordCounter_reg[8]_i_1_n_1\,
      CO(1) => \wordCounter_reg[8]_i_1_n_2\,
      CO(0) => \wordCounter_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \wordCounter_reg[8]_i_1_n_4\,
      O(2) => \wordCounter_reg[8]_i_1_n_5\,
      O(1) => \wordCounter_reg[8]_i_1_n_6\,
      O(0) => \wordCounter_reg[8]_i_1_n_7\,
      S(3 downto 0) => wordCounter_reg(11 downto 8)
    );
\wordCounter_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => m00_axis_aclk,
      CE => \^en_axiclk_0\,
      D => \wordCounter_reg[8]_i_1_n_6\,
      Q => wordCounter_reg(9),
      R => \wordCounter[0]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
  port (
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tvalid : out STD_LOGIC;
    en_AXIclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_aclk : in STD_LOGIC;
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
begin
samplGeneraror_v1_0_M00_AXIS_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      data_from_ram(31 downto 0) => data_from_ram(31 downto 0),
      en_AXIclk => en_AXIclk,
      en_AXIclk_0 => m00_axis_tvalid,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    FrameSize : in STD_LOGIC_VECTOR ( 15 downto 0 );
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    data_from_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    en_AXIclk : in STD_LOGIC;
    main_clk_100MHz : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_sampleGenerator_0_0,sampleGenerator_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sampleGenerator_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0
     port map (
      FrameSize(15 downto 0) => FrameSize(15 downto 0),
      data_from_ram(31 downto 0) => data_from_ram(31 downto 0),
      en_AXIclk => en_AXIclk,
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tlast => m00_axis_tlast,
      m00_axis_tvalid => m00_axis_tvalid
    );
end STRUCTURE;
