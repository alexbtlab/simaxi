// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Jun  3 21:02:54 2020
// Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_sampleGenerator_0_0_sim_netlist.v
// Design      : design_1_sampleGenerator_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_sampleGenerator_0_0,sampleGenerator_v1_0,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "sampleGenerator_v1_0,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (m00_axis_tdata,
    m00_axis_tstrb,
    m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tready,
    m00_axis_aclk,
    m00_axis_aresetn);
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TDATA" *) output [31:0]m00_axis_tdata;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB" *) output [3:0]m00_axis_tstrb;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TLAST" *) output m00_axis_tlast;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TVALID" *) output m00_axis_tvalid;
  (* X_INTERFACE_INFO = "xilinx.com:interface:axis:1.0 M00_AXIS TREADY" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_m00_axis_aclk_0, LAYERED_METADATA undef, INSERT_VIP 0" *) input m00_axis_tready;
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_m00_axis_aclk_0, INSERT_VIP 0" *) input m00_axis_aclk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input m00_axis_aresetn;

  wire \<const1> ;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;

  assign m00_axis_tstrb[3] = \<const1> ;
  assign m00_axis_tstrb[2] = \<const1> ;
  assign m00_axis_tstrb[1] = \<const1> ;
  assign m00_axis_tstrb[0] = \<const1> ;
  VCC VCC
       (.P(\<const1> ));
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 inst
       (.m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .m00_axis_tvalid(m00_axis_tvalid));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS
   (m00_axis_tlast,
    tValidR_reg_0,
    m00_axis_tdata,
    m00_axis_aresetn,
    m00_axis_aclk);
  output m00_axis_tlast;
  output tValidR_reg_0;
  output [31:0]m00_axis_tdata;
  input m00_axis_aresetn;
  input m00_axis_aclk;

  wire M_AXIS_TLAST_carry__0_i_1_n_0;
  wire M_AXIS_TLAST_carry__0_i_2_n_0;
  wire M_AXIS_TLAST_carry__0_n_3;
  wire M_AXIS_TLAST_carry_i_1_n_0;
  wire M_AXIS_TLAST_carry_i_2_n_0;
  wire M_AXIS_TLAST_carry_i_3_n_0;
  wire M_AXIS_TLAST_carry_i_4_n_0;
  wire M_AXIS_TLAST_carry_n_0;
  wire M_AXIS_TLAST_carry_n_1;
  wire M_AXIS_TLAST_carry_n_2;
  wire M_AXIS_TLAST_carry_n_3;
  wire \afterResetCycleCounterR[7]_i_2_n_0 ;
  wire [7:0]afterResetCycleCounterR_reg;
  wire clear;
  wire \counterR[3]_i_2_n_0 ;
  wire \counterR_reg[11]_i_1_n_0 ;
  wire \counterR_reg[11]_i_1_n_1 ;
  wire \counterR_reg[11]_i_1_n_2 ;
  wire \counterR_reg[11]_i_1_n_3 ;
  wire \counterR_reg[11]_i_1_n_4 ;
  wire \counterR_reg[11]_i_1_n_5 ;
  wire \counterR_reg[11]_i_1_n_6 ;
  wire \counterR_reg[11]_i_1_n_7 ;
  wire \counterR_reg[15]_i_1_n_0 ;
  wire \counterR_reg[15]_i_1_n_1 ;
  wire \counterR_reg[15]_i_1_n_2 ;
  wire \counterR_reg[15]_i_1_n_3 ;
  wire \counterR_reg[15]_i_1_n_4 ;
  wire \counterR_reg[15]_i_1_n_5 ;
  wire \counterR_reg[15]_i_1_n_6 ;
  wire \counterR_reg[15]_i_1_n_7 ;
  wire \counterR_reg[19]_i_1_n_0 ;
  wire \counterR_reg[19]_i_1_n_1 ;
  wire \counterR_reg[19]_i_1_n_2 ;
  wire \counterR_reg[19]_i_1_n_3 ;
  wire \counterR_reg[19]_i_1_n_4 ;
  wire \counterR_reg[19]_i_1_n_5 ;
  wire \counterR_reg[19]_i_1_n_6 ;
  wire \counterR_reg[19]_i_1_n_7 ;
  wire \counterR_reg[23]_i_1_n_0 ;
  wire \counterR_reg[23]_i_1_n_1 ;
  wire \counterR_reg[23]_i_1_n_2 ;
  wire \counterR_reg[23]_i_1_n_3 ;
  wire \counterR_reg[23]_i_1_n_4 ;
  wire \counterR_reg[23]_i_1_n_5 ;
  wire \counterR_reg[23]_i_1_n_6 ;
  wire \counterR_reg[23]_i_1_n_7 ;
  wire \counterR_reg[27]_i_1_n_0 ;
  wire \counterR_reg[27]_i_1_n_1 ;
  wire \counterR_reg[27]_i_1_n_2 ;
  wire \counterR_reg[27]_i_1_n_3 ;
  wire \counterR_reg[27]_i_1_n_4 ;
  wire \counterR_reg[27]_i_1_n_5 ;
  wire \counterR_reg[27]_i_1_n_6 ;
  wire \counterR_reg[27]_i_1_n_7 ;
  wire \counterR_reg[31]_i_2_n_1 ;
  wire \counterR_reg[31]_i_2_n_2 ;
  wire \counterR_reg[31]_i_2_n_3 ;
  wire \counterR_reg[31]_i_2_n_4 ;
  wire \counterR_reg[31]_i_2_n_5 ;
  wire \counterR_reg[31]_i_2_n_6 ;
  wire \counterR_reg[31]_i_2_n_7 ;
  wire \counterR_reg[3]_i_1_n_0 ;
  wire \counterR_reg[3]_i_1_n_1 ;
  wire \counterR_reg[3]_i_1_n_2 ;
  wire \counterR_reg[3]_i_1_n_3 ;
  wire \counterR_reg[3]_i_1_n_4 ;
  wire \counterR_reg[3]_i_1_n_5 ;
  wire \counterR_reg[3]_i_1_n_6 ;
  wire \counterR_reg[3]_i_1_n_7 ;
  wire \counterR_reg[7]_i_1_n_0 ;
  wire \counterR_reg[7]_i_1_n_1 ;
  wire \counterR_reg[7]_i_1_n_2 ;
  wire \counterR_reg[7]_i_1_n_3 ;
  wire \counterR_reg[7]_i_1_n_4 ;
  wire \counterR_reg[7]_i_1_n_5 ;
  wire \counterR_reg[7]_i_1_n_6 ;
  wire \counterR_reg[7]_i_1_n_7 ;
  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire [7:0]p_0_in;
  wire \packetCounter[0]_i_1_n_0 ;
  wire \packetCounter[0]_i_3_n_0 ;
  wire [15:0]packetCounter_reg;
  wire \packetCounter_reg[0]_i_2_n_0 ;
  wire \packetCounter_reg[0]_i_2_n_1 ;
  wire \packetCounter_reg[0]_i_2_n_2 ;
  wire \packetCounter_reg[0]_i_2_n_3 ;
  wire \packetCounter_reg[0]_i_2_n_4 ;
  wire \packetCounter_reg[0]_i_2_n_5 ;
  wire \packetCounter_reg[0]_i_2_n_6 ;
  wire \packetCounter_reg[0]_i_2_n_7 ;
  wire \packetCounter_reg[12]_i_1_n_1 ;
  wire \packetCounter_reg[12]_i_1_n_2 ;
  wire \packetCounter_reg[12]_i_1_n_3 ;
  wire \packetCounter_reg[12]_i_1_n_4 ;
  wire \packetCounter_reg[12]_i_1_n_5 ;
  wire \packetCounter_reg[12]_i_1_n_6 ;
  wire \packetCounter_reg[12]_i_1_n_7 ;
  wire \packetCounter_reg[4]_i_1_n_0 ;
  wire \packetCounter_reg[4]_i_1_n_1 ;
  wire \packetCounter_reg[4]_i_1_n_2 ;
  wire \packetCounter_reg[4]_i_1_n_3 ;
  wire \packetCounter_reg[4]_i_1_n_4 ;
  wire \packetCounter_reg[4]_i_1_n_5 ;
  wire \packetCounter_reg[4]_i_1_n_6 ;
  wire \packetCounter_reg[4]_i_1_n_7 ;
  wire \packetCounter_reg[8]_i_1_n_0 ;
  wire \packetCounter_reg[8]_i_1_n_1 ;
  wire \packetCounter_reg[8]_i_1_n_2 ;
  wire \packetCounter_reg[8]_i_1_n_3 ;
  wire \packetCounter_reg[8]_i_1_n_4 ;
  wire \packetCounter_reg[8]_i_1_n_5 ;
  wire \packetCounter_reg[8]_i_1_n_6 ;
  wire \packetCounter_reg[8]_i_1_n_7 ;
  wire sampleGeneratorEnR;
  wire sampleGeneratorEnR_i_1_n_0;
  wire sampleGeneratorEnR_i_2_n_0;
  wire tValidR_i_1_n_0;
  wire tValidR_reg_0;
  wire [3:0]NLW_M_AXIS_TLAST_carry_O_UNCONNECTED;
  wire [3:2]NLW_M_AXIS_TLAST_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED;
  wire [3:3]\NLW_counterR_reg[31]_i_2_CO_UNCONNECTED ;
  wire [3:3]\NLW_packetCounter_reg[12]_i_1_CO_UNCONNECTED ;

  CARRY4 M_AXIS_TLAST_carry
       (.CI(1'b0),
        .CO({M_AXIS_TLAST_carry_n_0,M_AXIS_TLAST_carry_n_1,M_AXIS_TLAST_carry_n_2,M_AXIS_TLAST_carry_n_3}),
        .CYINIT(1'b1),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_M_AXIS_TLAST_carry_O_UNCONNECTED[3:0]),
        .S({M_AXIS_TLAST_carry_i_1_n_0,M_AXIS_TLAST_carry_i_2_n_0,M_AXIS_TLAST_carry_i_3_n_0,M_AXIS_TLAST_carry_i_4_n_0}));
  CARRY4 M_AXIS_TLAST_carry__0
       (.CI(M_AXIS_TLAST_carry_n_0),
        .CO({NLW_M_AXIS_TLAST_carry__0_CO_UNCONNECTED[3:2],m00_axis_tlast,M_AXIS_TLAST_carry__0_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_M_AXIS_TLAST_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,M_AXIS_TLAST_carry__0_i_1_n_0,M_AXIS_TLAST_carry__0_i_2_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    M_AXIS_TLAST_carry__0_i_1
       (.I0(packetCounter_reg[15]),
        .O(M_AXIS_TLAST_carry__0_i_1_n_0));
  LUT3 #(
    .INIT(8'h02)) 
    M_AXIS_TLAST_carry__0_i_2
       (.I0(packetCounter_reg[12]),
        .I1(packetCounter_reg[14]),
        .I2(packetCounter_reg[13]),
        .O(M_AXIS_TLAST_carry__0_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    M_AXIS_TLAST_carry_i_1
       (.I0(packetCounter_reg[9]),
        .I1(packetCounter_reg[11]),
        .I2(packetCounter_reg[10]),
        .O(M_AXIS_TLAST_carry_i_1_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    M_AXIS_TLAST_carry_i_2
       (.I0(packetCounter_reg[6]),
        .I1(packetCounter_reg[8]),
        .I2(packetCounter_reg[7]),
        .O(M_AXIS_TLAST_carry_i_2_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    M_AXIS_TLAST_carry_i_3
       (.I0(packetCounter_reg[3]),
        .I1(packetCounter_reg[5]),
        .I2(packetCounter_reg[4]),
        .O(M_AXIS_TLAST_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h80)) 
    M_AXIS_TLAST_carry_i_4
       (.I0(packetCounter_reg[0]),
        .I1(packetCounter_reg[2]),
        .I2(packetCounter_reg[1]),
        .O(M_AXIS_TLAST_carry_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT1 #(
    .INIT(2'h1)) 
    \afterResetCycleCounterR[0]_i_1 
       (.I0(afterResetCycleCounterR_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \afterResetCycleCounterR[1]_i_1 
       (.I0(afterResetCycleCounterR_reg[0]),
        .I1(afterResetCycleCounterR_reg[1]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \afterResetCycleCounterR[2]_i_1 
       (.I0(afterResetCycleCounterR_reg[0]),
        .I1(afterResetCycleCounterR_reg[1]),
        .I2(afterResetCycleCounterR_reg[2]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \afterResetCycleCounterR[3]_i_1 
       (.I0(afterResetCycleCounterR_reg[1]),
        .I1(afterResetCycleCounterR_reg[0]),
        .I2(afterResetCycleCounterR_reg[2]),
        .I3(afterResetCycleCounterR_reg[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h7FFF8000)) 
    \afterResetCycleCounterR[4]_i_1 
       (.I0(afterResetCycleCounterR_reg[2]),
        .I1(afterResetCycleCounterR_reg[0]),
        .I2(afterResetCycleCounterR_reg[1]),
        .I3(afterResetCycleCounterR_reg[3]),
        .I4(afterResetCycleCounterR_reg[4]),
        .O(p_0_in[4]));
  LUT6 #(
    .INIT(64'h7FFFFFFF80000000)) 
    \afterResetCycleCounterR[5]_i_1 
       (.I0(afterResetCycleCounterR_reg[3]),
        .I1(afterResetCycleCounterR_reg[1]),
        .I2(afterResetCycleCounterR_reg[0]),
        .I3(afterResetCycleCounterR_reg[2]),
        .I4(afterResetCycleCounterR_reg[4]),
        .I5(afterResetCycleCounterR_reg[5]),
        .O(p_0_in[5]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \afterResetCycleCounterR[6]_i_1 
       (.I0(\afterResetCycleCounterR[7]_i_2_n_0 ),
        .I1(afterResetCycleCounterR_reg[6]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \afterResetCycleCounterR[7]_i_1 
       (.I0(\afterResetCycleCounterR[7]_i_2_n_0 ),
        .I1(afterResetCycleCounterR_reg[6]),
        .I2(afterResetCycleCounterR_reg[7]),
        .O(p_0_in[7]));
  LUT6 #(
    .INIT(64'h8000000000000000)) 
    \afterResetCycleCounterR[7]_i_2 
       (.I0(afterResetCycleCounterR_reg[5]),
        .I1(afterResetCycleCounterR_reg[3]),
        .I2(afterResetCycleCounterR_reg[1]),
        .I3(afterResetCycleCounterR_reg[0]),
        .I4(afterResetCycleCounterR_reg[2]),
        .I5(afterResetCycleCounterR_reg[4]),
        .O(\afterResetCycleCounterR[7]_i_2_n_0 ));
  FDRE \afterResetCycleCounterR_reg[0] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(afterResetCycleCounterR_reg[0]),
        .R(clear));
  FDRE \afterResetCycleCounterR_reg[1] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(afterResetCycleCounterR_reg[1]),
        .R(clear));
  FDRE \afterResetCycleCounterR_reg[2] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(afterResetCycleCounterR_reg[2]),
        .R(clear));
  FDRE \afterResetCycleCounterR_reg[3] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(afterResetCycleCounterR_reg[3]),
        .R(clear));
  FDRE \afterResetCycleCounterR_reg[4] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(afterResetCycleCounterR_reg[4]),
        .R(clear));
  FDRE \afterResetCycleCounterR_reg[5] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(afterResetCycleCounterR_reg[5]),
        .R(clear));
  FDRE \afterResetCycleCounterR_reg[6] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in[6]),
        .Q(afterResetCycleCounterR_reg[6]),
        .R(clear));
  FDRE \afterResetCycleCounterR_reg[7] 
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(p_0_in[7]),
        .Q(afterResetCycleCounterR_reg[7]),
        .R(clear));
  LUT1 #(
    .INIT(2'h1)) 
    \counterR[31]_i_1 
       (.I0(m00_axis_aresetn),
        .O(clear));
  LUT1 #(
    .INIT(2'h1)) 
    \counterR[3]_i_2 
       (.I0(m00_axis_tdata[0]),
        .O(\counterR[3]_i_2_n_0 ));
  FDRE \counterR_reg[0] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[3]_i_1_n_7 ),
        .Q(m00_axis_tdata[0]),
        .R(clear));
  FDRE \counterR_reg[10] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[11]_i_1_n_5 ),
        .Q(m00_axis_tdata[10]),
        .R(clear));
  FDRE \counterR_reg[11] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[11]_i_1_n_4 ),
        .Q(m00_axis_tdata[11]),
        .R(clear));
  CARRY4 \counterR_reg[11]_i_1 
       (.CI(\counterR_reg[7]_i_1_n_0 ),
        .CO({\counterR_reg[11]_i_1_n_0 ,\counterR_reg[11]_i_1_n_1 ,\counterR_reg[11]_i_1_n_2 ,\counterR_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counterR_reg[11]_i_1_n_4 ,\counterR_reg[11]_i_1_n_5 ,\counterR_reg[11]_i_1_n_6 ,\counterR_reg[11]_i_1_n_7 }),
        .S(m00_axis_tdata[11:8]));
  FDRE \counterR_reg[12] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[15]_i_1_n_7 ),
        .Q(m00_axis_tdata[12]),
        .R(clear));
  FDRE \counterR_reg[13] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[15]_i_1_n_6 ),
        .Q(m00_axis_tdata[13]),
        .R(clear));
  FDRE \counterR_reg[14] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[15]_i_1_n_5 ),
        .Q(m00_axis_tdata[14]),
        .R(clear));
  FDRE \counterR_reg[15] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[15]_i_1_n_4 ),
        .Q(m00_axis_tdata[15]),
        .R(clear));
  CARRY4 \counterR_reg[15]_i_1 
       (.CI(\counterR_reg[11]_i_1_n_0 ),
        .CO({\counterR_reg[15]_i_1_n_0 ,\counterR_reg[15]_i_1_n_1 ,\counterR_reg[15]_i_1_n_2 ,\counterR_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counterR_reg[15]_i_1_n_4 ,\counterR_reg[15]_i_1_n_5 ,\counterR_reg[15]_i_1_n_6 ,\counterR_reg[15]_i_1_n_7 }),
        .S(m00_axis_tdata[15:12]));
  FDRE \counterR_reg[16] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[19]_i_1_n_7 ),
        .Q(m00_axis_tdata[16]),
        .R(clear));
  FDRE \counterR_reg[17] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[19]_i_1_n_6 ),
        .Q(m00_axis_tdata[17]),
        .R(clear));
  FDRE \counterR_reg[18] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[19]_i_1_n_5 ),
        .Q(m00_axis_tdata[18]),
        .R(clear));
  FDRE \counterR_reg[19] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[19]_i_1_n_4 ),
        .Q(m00_axis_tdata[19]),
        .R(clear));
  CARRY4 \counterR_reg[19]_i_1 
       (.CI(\counterR_reg[15]_i_1_n_0 ),
        .CO({\counterR_reg[19]_i_1_n_0 ,\counterR_reg[19]_i_1_n_1 ,\counterR_reg[19]_i_1_n_2 ,\counterR_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counterR_reg[19]_i_1_n_4 ,\counterR_reg[19]_i_1_n_5 ,\counterR_reg[19]_i_1_n_6 ,\counterR_reg[19]_i_1_n_7 }),
        .S(m00_axis_tdata[19:16]));
  FDRE \counterR_reg[1] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[3]_i_1_n_6 ),
        .Q(m00_axis_tdata[1]),
        .R(clear));
  FDRE \counterR_reg[20] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[23]_i_1_n_7 ),
        .Q(m00_axis_tdata[20]),
        .R(clear));
  FDRE \counterR_reg[21] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[23]_i_1_n_6 ),
        .Q(m00_axis_tdata[21]),
        .R(clear));
  FDRE \counterR_reg[22] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[23]_i_1_n_5 ),
        .Q(m00_axis_tdata[22]),
        .R(clear));
  FDRE \counterR_reg[23] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[23]_i_1_n_4 ),
        .Q(m00_axis_tdata[23]),
        .R(clear));
  CARRY4 \counterR_reg[23]_i_1 
       (.CI(\counterR_reg[19]_i_1_n_0 ),
        .CO({\counterR_reg[23]_i_1_n_0 ,\counterR_reg[23]_i_1_n_1 ,\counterR_reg[23]_i_1_n_2 ,\counterR_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counterR_reg[23]_i_1_n_4 ,\counterR_reg[23]_i_1_n_5 ,\counterR_reg[23]_i_1_n_6 ,\counterR_reg[23]_i_1_n_7 }),
        .S(m00_axis_tdata[23:20]));
  FDRE \counterR_reg[24] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[27]_i_1_n_7 ),
        .Q(m00_axis_tdata[24]),
        .R(clear));
  FDRE \counterR_reg[25] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[27]_i_1_n_6 ),
        .Q(m00_axis_tdata[25]),
        .R(clear));
  FDRE \counterR_reg[26] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[27]_i_1_n_5 ),
        .Q(m00_axis_tdata[26]),
        .R(clear));
  FDRE \counterR_reg[27] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[27]_i_1_n_4 ),
        .Q(m00_axis_tdata[27]),
        .R(clear));
  CARRY4 \counterR_reg[27]_i_1 
       (.CI(\counterR_reg[23]_i_1_n_0 ),
        .CO({\counterR_reg[27]_i_1_n_0 ,\counterR_reg[27]_i_1_n_1 ,\counterR_reg[27]_i_1_n_2 ,\counterR_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counterR_reg[27]_i_1_n_4 ,\counterR_reg[27]_i_1_n_5 ,\counterR_reg[27]_i_1_n_6 ,\counterR_reg[27]_i_1_n_7 }),
        .S(m00_axis_tdata[27:24]));
  FDRE \counterR_reg[28] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[31]_i_2_n_7 ),
        .Q(m00_axis_tdata[28]),
        .R(clear));
  FDRE \counterR_reg[29] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[31]_i_2_n_6 ),
        .Q(m00_axis_tdata[29]),
        .R(clear));
  FDRE \counterR_reg[2] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[3]_i_1_n_5 ),
        .Q(m00_axis_tdata[2]),
        .R(clear));
  FDRE \counterR_reg[30] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[31]_i_2_n_5 ),
        .Q(m00_axis_tdata[30]),
        .R(clear));
  FDRE \counterR_reg[31] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[31]_i_2_n_4 ),
        .Q(m00_axis_tdata[31]),
        .R(clear));
  CARRY4 \counterR_reg[31]_i_2 
       (.CI(\counterR_reg[27]_i_1_n_0 ),
        .CO({\NLW_counterR_reg[31]_i_2_CO_UNCONNECTED [3],\counterR_reg[31]_i_2_n_1 ,\counterR_reg[31]_i_2_n_2 ,\counterR_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counterR_reg[31]_i_2_n_4 ,\counterR_reg[31]_i_2_n_5 ,\counterR_reg[31]_i_2_n_6 ,\counterR_reg[31]_i_2_n_7 }),
        .S(m00_axis_tdata[31:28]));
  FDRE \counterR_reg[3] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[3]_i_1_n_4 ),
        .Q(m00_axis_tdata[3]),
        .R(clear));
  CARRY4 \counterR_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\counterR_reg[3]_i_1_n_0 ,\counterR_reg[3]_i_1_n_1 ,\counterR_reg[3]_i_1_n_2 ,\counterR_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\counterR_reg[3]_i_1_n_4 ,\counterR_reg[3]_i_1_n_5 ,\counterR_reg[3]_i_1_n_6 ,\counterR_reg[3]_i_1_n_7 }),
        .S({m00_axis_tdata[3:1],\counterR[3]_i_2_n_0 }));
  FDRE \counterR_reg[4] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[7]_i_1_n_7 ),
        .Q(m00_axis_tdata[4]),
        .R(clear));
  FDRE \counterR_reg[5] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[7]_i_1_n_6 ),
        .Q(m00_axis_tdata[5]),
        .R(clear));
  FDRE \counterR_reg[6] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[7]_i_1_n_5 ),
        .Q(m00_axis_tdata[6]),
        .R(clear));
  FDRE \counterR_reg[7] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[7]_i_1_n_4 ),
        .Q(m00_axis_tdata[7]),
        .R(clear));
  CARRY4 \counterR_reg[7]_i_1 
       (.CI(\counterR_reg[3]_i_1_n_0 ),
        .CO({\counterR_reg[7]_i_1_n_0 ,\counterR_reg[7]_i_1_n_1 ,\counterR_reg[7]_i_1_n_2 ,\counterR_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\counterR_reg[7]_i_1_n_4 ,\counterR_reg[7]_i_1_n_5 ,\counterR_reg[7]_i_1_n_6 ,\counterR_reg[7]_i_1_n_7 }),
        .S(m00_axis_tdata[7:4]));
  FDRE \counterR_reg[8] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[11]_i_1_n_7 ),
        .Q(m00_axis_tdata[8]),
        .R(clear));
  FDRE \counterR_reg[9] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\counterR_reg[11]_i_1_n_6 ),
        .Q(m00_axis_tdata[9]),
        .R(clear));
  LUT3 #(
    .INIT(8'h8F)) 
    \packetCounter[0]_i_1 
       (.I0(m00_axis_tlast),
        .I1(tValidR_reg_0),
        .I2(m00_axis_aresetn),
        .O(\packetCounter[0]_i_1_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \packetCounter[0]_i_3 
       (.I0(packetCounter_reg[0]),
        .O(\packetCounter[0]_i_3_n_0 ));
  FDSE \packetCounter_reg[0] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[0]_i_2_n_7 ),
        .Q(packetCounter_reg[0]),
        .S(\packetCounter[0]_i_1_n_0 ));
  CARRY4 \packetCounter_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\packetCounter_reg[0]_i_2_n_0 ,\packetCounter_reg[0]_i_2_n_1 ,\packetCounter_reg[0]_i_2_n_2 ,\packetCounter_reg[0]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\packetCounter_reg[0]_i_2_n_4 ,\packetCounter_reg[0]_i_2_n_5 ,\packetCounter_reg[0]_i_2_n_6 ,\packetCounter_reg[0]_i_2_n_7 }),
        .S({packetCounter_reg[3:1],\packetCounter[0]_i_3_n_0 }));
  FDRE \packetCounter_reg[10] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[8]_i_1_n_5 ),
        .Q(packetCounter_reg[10]),
        .R(\packetCounter[0]_i_1_n_0 ));
  FDRE \packetCounter_reg[11] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[8]_i_1_n_4 ),
        .Q(packetCounter_reg[11]),
        .R(\packetCounter[0]_i_1_n_0 ));
  FDRE \packetCounter_reg[12] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[12]_i_1_n_7 ),
        .Q(packetCounter_reg[12]),
        .R(\packetCounter[0]_i_1_n_0 ));
  CARRY4 \packetCounter_reg[12]_i_1 
       (.CI(\packetCounter_reg[8]_i_1_n_0 ),
        .CO({\NLW_packetCounter_reg[12]_i_1_CO_UNCONNECTED [3],\packetCounter_reg[12]_i_1_n_1 ,\packetCounter_reg[12]_i_1_n_2 ,\packetCounter_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\packetCounter_reg[12]_i_1_n_4 ,\packetCounter_reg[12]_i_1_n_5 ,\packetCounter_reg[12]_i_1_n_6 ,\packetCounter_reg[12]_i_1_n_7 }),
        .S(packetCounter_reg[15:12]));
  FDRE \packetCounter_reg[13] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[12]_i_1_n_6 ),
        .Q(packetCounter_reg[13]),
        .R(\packetCounter[0]_i_1_n_0 ));
  FDRE \packetCounter_reg[14] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[12]_i_1_n_5 ),
        .Q(packetCounter_reg[14]),
        .R(\packetCounter[0]_i_1_n_0 ));
  FDRE \packetCounter_reg[15] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[12]_i_1_n_4 ),
        .Q(packetCounter_reg[15]),
        .R(\packetCounter[0]_i_1_n_0 ));
  FDSE \packetCounter_reg[1] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[0]_i_2_n_6 ),
        .Q(packetCounter_reg[1]),
        .S(\packetCounter[0]_i_1_n_0 ));
  FDSE \packetCounter_reg[2] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[0]_i_2_n_5 ),
        .Q(packetCounter_reg[2]),
        .S(\packetCounter[0]_i_1_n_0 ));
  FDSE \packetCounter_reg[3] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[0]_i_2_n_4 ),
        .Q(packetCounter_reg[3]),
        .S(\packetCounter[0]_i_1_n_0 ));
  FDSE \packetCounter_reg[4] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[4]_i_1_n_7 ),
        .Q(packetCounter_reg[4]),
        .S(\packetCounter[0]_i_1_n_0 ));
  CARRY4 \packetCounter_reg[4]_i_1 
       (.CI(\packetCounter_reg[0]_i_2_n_0 ),
        .CO({\packetCounter_reg[4]_i_1_n_0 ,\packetCounter_reg[4]_i_1_n_1 ,\packetCounter_reg[4]_i_1_n_2 ,\packetCounter_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\packetCounter_reg[4]_i_1_n_4 ,\packetCounter_reg[4]_i_1_n_5 ,\packetCounter_reg[4]_i_1_n_6 ,\packetCounter_reg[4]_i_1_n_7 }),
        .S(packetCounter_reg[7:4]));
  FDSE \packetCounter_reg[5] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[4]_i_1_n_6 ),
        .Q(packetCounter_reg[5]),
        .S(\packetCounter[0]_i_1_n_0 ));
  FDSE \packetCounter_reg[6] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[4]_i_1_n_5 ),
        .Q(packetCounter_reg[6]),
        .S(\packetCounter[0]_i_1_n_0 ));
  FDSE \packetCounter_reg[7] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[4]_i_1_n_4 ),
        .Q(packetCounter_reg[7]),
        .S(\packetCounter[0]_i_1_n_0 ));
  FDRE \packetCounter_reg[8] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[8]_i_1_n_7 ),
        .Q(packetCounter_reg[8]),
        .R(\packetCounter[0]_i_1_n_0 ));
  CARRY4 \packetCounter_reg[8]_i_1 
       (.CI(\packetCounter_reg[4]_i_1_n_0 ),
        .CO({\packetCounter_reg[8]_i_1_n_0 ,\packetCounter_reg[8]_i_1_n_1 ,\packetCounter_reg[8]_i_1_n_2 ,\packetCounter_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\packetCounter_reg[8]_i_1_n_4 ,\packetCounter_reg[8]_i_1_n_5 ,\packetCounter_reg[8]_i_1_n_6 ,\packetCounter_reg[8]_i_1_n_7 }),
        .S(packetCounter_reg[11:8]));
  FDRE \packetCounter_reg[9] 
       (.C(m00_axis_aclk),
        .CE(tValidR_reg_0),
        .D(\packetCounter_reg[8]_i_1_n_6 ),
        .Q(packetCounter_reg[9]),
        .R(\packetCounter[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFF00000001)) 
    sampleGeneratorEnR_i_1
       (.I0(sampleGeneratorEnR_i_2_n_0),
        .I1(afterResetCycleCounterR_reg[7]),
        .I2(afterResetCycleCounterR_reg[6]),
        .I3(afterResetCycleCounterR_reg[4]),
        .I4(afterResetCycleCounterR_reg[0]),
        .I5(sampleGeneratorEnR),
        .O(sampleGeneratorEnR_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    sampleGeneratorEnR_i_2
       (.I0(afterResetCycleCounterR_reg[2]),
        .I1(afterResetCycleCounterR_reg[3]),
        .I2(afterResetCycleCounterR_reg[5]),
        .I3(afterResetCycleCounterR_reg[1]),
        .O(sampleGeneratorEnR_i_2_n_0));
  FDRE sampleGeneratorEnR_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(sampleGeneratorEnR_i_1_n_0),
        .Q(sampleGeneratorEnR),
        .R(clear));
  LUT2 #(
    .INIT(4'hE)) 
    tValidR_i_1
       (.I0(sampleGeneratorEnR),
        .I1(tValidR_reg_0),
        .O(tValidR_i_1_n_0));
  FDRE tValidR_reg
       (.C(m00_axis_aclk),
        .CE(1'b1),
        .D(tValidR_i_1_n_0),
        .Q(tValidR_reg_0),
        .R(clear));
endmodule

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0
   (m00_axis_tlast,
    m00_axis_tvalid,
    m00_axis_tdata,
    m00_axis_aresetn,
    m00_axis_aclk);
  output m00_axis_tlast;
  output m00_axis_tvalid;
  output [31:0]m00_axis_tdata;
  input m00_axis_aresetn;
  input m00_axis_aclk;

  wire m00_axis_aclk;
  wire m00_axis_aresetn;
  wire [31:0]m00_axis_tdata;
  wire m00_axis_tlast;
  wire m00_axis_tvalid;

  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS samplGeneraror_v1_0_M00_AXIS_inst
       (.m00_axis_aclk(m00_axis_aclk),
        .m00_axis_aresetn(m00_axis_aresetn),
        .m00_axis_tdata(m00_axis_tdata),
        .m00_axis_tlast(m00_axis_tlast),
        .tValidR_reg_0(m00_axis_tvalid));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
