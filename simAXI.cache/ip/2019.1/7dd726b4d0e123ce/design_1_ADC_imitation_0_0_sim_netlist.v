// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Sat Jun  6 22:15:45 2020
// Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.v
// Design      : design_1_ADC_imitation_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tfgg484-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
   (UNCONN_OUT,
    clk_100Mhz_AXI_send,
    write_reg_reg_0,
    UNCONN_OUT_0,
    UNCONN_OUT_1,
    UNCONN_OUT_2,
    UNCONN_OUT_3,
    UNCONN_OUT_4,
    UNCONN_OUT_5,
    UNCONN_OUT_6,
    UNCONN_OUT_7,
    UNCONN_OUT_8,
    UNCONN_OUT_9,
    UNCONN_OUT_10,
    UNCONN_OUT_11,
    UNCONN_OUT_12,
    UNCONN_OUT_13,
    UNCONN_OUT_14,
    UNCONN_OUT_15,
    UNCONN_OUT_16,
    UNCONN_OUT_17,
    UNCONN_OUT_18,
    UNCONN_OUT_19,
    UNCONN_OUT_20,
    UNCONN_OUT_21,
    UNCONN_OUT_22,
    UNCONN_OUT_23,
    UNCONN_OUT_24,
    UNCONN_OUT_25,
    UNCONN_OUT_26,
    UNCONN_OUT_27,
    UNCONN_OUT_28,
    UNCONN_OUT_29,
    UNCONN_OUT_30,
    clk_5MHzWR_100MhzRD,
    clk_100MHz,
    clk_5MHz,
    s00_axi_aresetn);
  output UNCONN_OUT;
  output clk_100Mhz_AXI_send;
  output write_reg_reg_0;
  output UNCONN_OUT_0;
  output UNCONN_OUT_1;
  output UNCONN_OUT_2;
  output UNCONN_OUT_3;
  output UNCONN_OUT_4;
  output UNCONN_OUT_5;
  output UNCONN_OUT_6;
  output UNCONN_OUT_7;
  output UNCONN_OUT_8;
  output UNCONN_OUT_9;
  output UNCONN_OUT_10;
  output UNCONN_OUT_11;
  output UNCONN_OUT_12;
  output UNCONN_OUT_13;
  output UNCONN_OUT_14;
  output UNCONN_OUT_15;
  output UNCONN_OUT_16;
  output UNCONN_OUT_17;
  output UNCONN_OUT_18;
  output UNCONN_OUT_19;
  output UNCONN_OUT_20;
  output UNCONN_OUT_21;
  output UNCONN_OUT_22;
  output UNCONN_OUT_23;
  output UNCONN_OUT_24;
  output UNCONN_OUT_25;
  output UNCONN_OUT_26;
  output UNCONN_OUT_27;
  output UNCONN_OUT_28;
  output UNCONN_OUT_29;
  output UNCONN_OUT_30;
  output clk_5MHzWR_100MhzRD;
  input clk_100MHz;
  input clk_5MHz;
  input s00_axi_aresetn;

  wire FLAG_state;
  wire FLAG_state_i_1_n_0;
  wire FLAG_state_reg_n_0;
  wire UNCONN_OUT;
  wire UNCONN_OUT_0;
  wire UNCONN_OUT_1;
  wire UNCONN_OUT_10;
  wire UNCONN_OUT_11;
  wire UNCONN_OUT_12;
  wire UNCONN_OUT_13;
  wire UNCONN_OUT_14;
  wire UNCONN_OUT_15;
  wire UNCONN_OUT_16;
  wire UNCONN_OUT_17;
  wire UNCONN_OUT_18;
  wire UNCONN_OUT_19;
  wire UNCONN_OUT_2;
  wire UNCONN_OUT_20;
  wire UNCONN_OUT_21;
  wire UNCONN_OUT_22;
  wire UNCONN_OUT_23;
  wire UNCONN_OUT_24;
  wire UNCONN_OUT_25;
  wire UNCONN_OUT_26;
  wire UNCONN_OUT_27;
  wire UNCONN_OUT_28;
  wire UNCONN_OUT_29;
  wire UNCONN_OUT_3;
  wire UNCONN_OUT_30;
  wire UNCONN_OUT_4;
  wire UNCONN_OUT_5;
  wire UNCONN_OUT_6;
  wire UNCONN_OUT_7;
  wire UNCONN_OUT_8;
  wire UNCONN_OUT_9;
  wire clk_100MHz;
  wire clk_100Mhz_AXI_send;
  wire clk_5MHz;
  wire clk_5MHzWR_100MhzRD;
  wire \count_clk[31]_i_10_n_0 ;
  wire \count_clk[31]_i_11_n_0 ;
  wire \count_clk[31]_i_3_n_0 ;
  wire \count_clk[31]_i_4_n_0 ;
  wire \count_clk[31]_i_5_n_0 ;
  wire \count_clk[31]_i_6_n_0 ;
  wire \count_clk[31]_i_7_n_0 ;
  wire \count_clk[31]_i_8_n_0 ;
  wire \count_clk[31]_i_9_n_0 ;
  wire \count_clk[3]_i_2_n_0 ;
  wire \count_clk_reg[11]_i_1_n_0 ;
  wire \count_clk_reg[11]_i_1_n_1 ;
  wire \count_clk_reg[11]_i_1_n_2 ;
  wire \count_clk_reg[11]_i_1_n_3 ;
  wire \count_clk_reg[15]_i_1_n_0 ;
  wire \count_clk_reg[15]_i_1_n_1 ;
  wire \count_clk_reg[15]_i_1_n_2 ;
  wire \count_clk_reg[15]_i_1_n_3 ;
  wire \count_clk_reg[19]_i_1_n_0 ;
  wire \count_clk_reg[19]_i_1_n_1 ;
  wire \count_clk_reg[19]_i_1_n_2 ;
  wire \count_clk_reg[19]_i_1_n_3 ;
  wire \count_clk_reg[23]_i_1_n_0 ;
  wire \count_clk_reg[23]_i_1_n_1 ;
  wire \count_clk_reg[23]_i_1_n_2 ;
  wire \count_clk_reg[23]_i_1_n_3 ;
  wire \count_clk_reg[27]_i_1_n_0 ;
  wire \count_clk_reg[27]_i_1_n_1 ;
  wire \count_clk_reg[27]_i_1_n_2 ;
  wire \count_clk_reg[27]_i_1_n_3 ;
  wire \count_clk_reg[31]_i_2_n_1 ;
  wire \count_clk_reg[31]_i_2_n_2 ;
  wire \count_clk_reg[31]_i_2_n_3 ;
  wire \count_clk_reg[3]_i_1_n_0 ;
  wire \count_clk_reg[3]_i_1_n_1 ;
  wire \count_clk_reg[3]_i_1_n_2 ;
  wire \count_clk_reg[3]_i_1_n_3 ;
  wire \count_clk_reg[7]_i_1_n_0 ;
  wire \count_clk_reg[7]_i_1_n_1 ;
  wire \count_clk_reg[7]_i_1_n_2 ;
  wire \count_clk_reg[7]_i_1_n_3 ;
  wire [31:0]data0;
  wire s00_axi_aresetn;
  wire write_reg_i_1_n_0;
  wire write_reg_reg_0;
  wire [3:3]\NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED ;

  LUT5 #(
    .INIT(32'hFFFE0001)) 
    FLAG_state_i_1
       (.I0(\count_clk[31]_i_4_n_0 ),
        .I1(\count_clk[31]_i_5_n_0 ),
        .I2(\count_clk[31]_i_6_n_0 ),
        .I3(\count_clk[31]_i_7_n_0 ),
        .I4(FLAG_state_reg_n_0),
        .O(FLAG_state_i_1_n_0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    FLAG_state_reg
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(FLAG_state_i_1_n_0),
        .Q(FLAG_state_reg_n_0),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT2 #(
    .INIT(4'hB)) 
    clk_100Mhz_AXI_send_INST_0
       (.I0(write_reg_reg_0),
        .I1(clk_100MHz),
        .O(clk_100Mhz_AXI_send));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    clk_5MHzWR_100MhzRD_INST_0
       (.I0(clk_5MHz),
        .I1(write_reg_reg_0),
        .I2(clk_100MHz),
        .O(clk_5MHzWR_100MhzRD));
  LUT4 #(
    .INIT(16'h0001)) 
    \count_clk[31]_i_1 
       (.I0(\count_clk[31]_i_4_n_0 ),
        .I1(\count_clk[31]_i_5_n_0 ),
        .I2(\count_clk[31]_i_6_n_0 ),
        .I3(\count_clk[31]_i_7_n_0 ),
        .O(FLAG_state));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \count_clk[31]_i_10 
       (.I0(UNCONN_OUT_26),
        .I1(UNCONN_OUT_27),
        .I2(UNCONN_OUT_24),
        .I3(UNCONN_OUT_25),
        .O(\count_clk[31]_i_10_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count_clk[31]_i_11 
       (.I0(UNCONN_OUT_18),
        .I1(UNCONN_OUT_19),
        .I2(UNCONN_OUT_16),
        .I3(UNCONN_OUT_17),
        .O(\count_clk[31]_i_11_n_0 ));
  LUT4 #(
    .INIT(16'hE200)) 
    \count_clk[31]_i_3 
       (.I0(clk_100MHz),
        .I1(write_reg_reg_0),
        .I2(clk_5MHz),
        .I3(s00_axi_aresetn),
        .O(\count_clk[31]_i_3_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \count_clk[31]_i_4 
       (.I0(UNCONN_OUT_13),
        .I1(UNCONN_OUT_12),
        .I2(UNCONN_OUT_15),
        .I3(UNCONN_OUT_14),
        .I4(\count_clk[31]_i_8_n_0 ),
        .O(\count_clk[31]_i_4_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \count_clk[31]_i_5 
       (.I0(UNCONN_OUT_5),
        .I1(UNCONN_OUT_4),
        .I2(UNCONN_OUT_7),
        .I3(UNCONN_OUT_6),
        .I4(\count_clk[31]_i_9_n_0 ),
        .O(\count_clk[31]_i_5_n_0 ));
  LUT5 #(
    .INIT(32'hFFFF7FFF)) 
    \count_clk[31]_i_6 
       (.I0(UNCONN_OUT_29),
        .I1(UNCONN_OUT_28),
        .I2(UNCONN_OUT),
        .I3(UNCONN_OUT_30),
        .I4(\count_clk[31]_i_10_n_0 ),
        .O(\count_clk[31]_i_6_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \count_clk[31]_i_7 
       (.I0(UNCONN_OUT_21),
        .I1(UNCONN_OUT_20),
        .I2(UNCONN_OUT_23),
        .I3(UNCONN_OUT_22),
        .I4(\count_clk[31]_i_11_n_0 ),
        .O(\count_clk[31]_i_7_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count_clk[31]_i_8 
       (.I0(UNCONN_OUT_10),
        .I1(UNCONN_OUT_11),
        .I2(UNCONN_OUT_8),
        .I3(UNCONN_OUT_9),
        .O(\count_clk[31]_i_8_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \count_clk[31]_i_9 
       (.I0(UNCONN_OUT_2),
        .I1(UNCONN_OUT_3),
        .I2(UNCONN_OUT_0),
        .I3(UNCONN_OUT_1),
        .O(\count_clk[31]_i_9_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \count_clk[3]_i_2 
       (.I0(UNCONN_OUT),
        .O(\count_clk[3]_i_2_n_0 ));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[0] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[0]),
        .Q(UNCONN_OUT),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[10] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[10]),
        .Q(UNCONN_OUT_21),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[11] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[11]),
        .Q(UNCONN_OUT_20),
        .R(FLAG_state));
  CARRY4 \count_clk_reg[11]_i_1 
       (.CI(\count_clk_reg[7]_i_1_n_0 ),
        .CO({\count_clk_reg[11]_i_1_n_0 ,\count_clk_reg[11]_i_1_n_1 ,\count_clk_reg[11]_i_1_n_2 ,\count_clk_reg[11]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[11:8]),
        .S({UNCONN_OUT_20,UNCONN_OUT_21,UNCONN_OUT_22,UNCONN_OUT_23}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[12] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[12]),
        .Q(UNCONN_OUT_19),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[13] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[13]),
        .Q(UNCONN_OUT_18),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[14] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[14]),
        .Q(UNCONN_OUT_17),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[15] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[15]),
        .Q(UNCONN_OUT_16),
        .R(FLAG_state));
  CARRY4 \count_clk_reg[15]_i_1 
       (.CI(\count_clk_reg[11]_i_1_n_0 ),
        .CO({\count_clk_reg[15]_i_1_n_0 ,\count_clk_reg[15]_i_1_n_1 ,\count_clk_reg[15]_i_1_n_2 ,\count_clk_reg[15]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[15:12]),
        .S({UNCONN_OUT_16,UNCONN_OUT_17,UNCONN_OUT_18,UNCONN_OUT_19}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[16] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[16]),
        .Q(UNCONN_OUT_15),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[17] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[17]),
        .Q(UNCONN_OUT_14),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[18] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[18]),
        .Q(UNCONN_OUT_13),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[19] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[19]),
        .Q(UNCONN_OUT_12),
        .R(FLAG_state));
  CARRY4 \count_clk_reg[19]_i_1 
       (.CI(\count_clk_reg[15]_i_1_n_0 ),
        .CO({\count_clk_reg[19]_i_1_n_0 ,\count_clk_reg[19]_i_1_n_1 ,\count_clk_reg[19]_i_1_n_2 ,\count_clk_reg[19]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[19:16]),
        .S({UNCONN_OUT_12,UNCONN_OUT_13,UNCONN_OUT_14,UNCONN_OUT_15}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[1] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[1]),
        .Q(UNCONN_OUT_30),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[20] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[20]),
        .Q(UNCONN_OUT_11),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[21] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[21]),
        .Q(UNCONN_OUT_10),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[22] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[22]),
        .Q(UNCONN_OUT_9),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[23] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[23]),
        .Q(UNCONN_OUT_8),
        .R(FLAG_state));
  CARRY4 \count_clk_reg[23]_i_1 
       (.CI(\count_clk_reg[19]_i_1_n_0 ),
        .CO({\count_clk_reg[23]_i_1_n_0 ,\count_clk_reg[23]_i_1_n_1 ,\count_clk_reg[23]_i_1_n_2 ,\count_clk_reg[23]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[23:20]),
        .S({UNCONN_OUT_8,UNCONN_OUT_9,UNCONN_OUT_10,UNCONN_OUT_11}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[24] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[24]),
        .Q(UNCONN_OUT_7),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[25] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[25]),
        .Q(UNCONN_OUT_6),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[26] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[26]),
        .Q(UNCONN_OUT_5),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[27] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[27]),
        .Q(UNCONN_OUT_4),
        .R(FLAG_state));
  CARRY4 \count_clk_reg[27]_i_1 
       (.CI(\count_clk_reg[23]_i_1_n_0 ),
        .CO({\count_clk_reg[27]_i_1_n_0 ,\count_clk_reg[27]_i_1_n_1 ,\count_clk_reg[27]_i_1_n_2 ,\count_clk_reg[27]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[27:24]),
        .S({UNCONN_OUT_4,UNCONN_OUT_5,UNCONN_OUT_6,UNCONN_OUT_7}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[28] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[28]),
        .Q(UNCONN_OUT_3),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[29] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[29]),
        .Q(UNCONN_OUT_2),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[2] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[2]),
        .Q(UNCONN_OUT_29),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[30] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[30]),
        .Q(UNCONN_OUT_1),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[31] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[31]),
        .Q(UNCONN_OUT_0),
        .R(FLAG_state));
  CARRY4 \count_clk_reg[31]_i_2 
       (.CI(\count_clk_reg[27]_i_1_n_0 ),
        .CO({\NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED [3],\count_clk_reg[31]_i_2_n_1 ,\count_clk_reg[31]_i_2_n_2 ,\count_clk_reg[31]_i_2_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[31:28]),
        .S({UNCONN_OUT_0,UNCONN_OUT_1,UNCONN_OUT_2,UNCONN_OUT_3}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[3] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[3]),
        .Q(UNCONN_OUT_28),
        .R(FLAG_state));
  CARRY4 \count_clk_reg[3]_i_1 
       (.CI(1'b0),
        .CO({\count_clk_reg[3]_i_1_n_0 ,\count_clk_reg[3]_i_1_n_1 ,\count_clk_reg[3]_i_1_n_2 ,\count_clk_reg[3]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,UNCONN_OUT}),
        .O(data0[3:0]),
        .S({UNCONN_OUT_28,UNCONN_OUT_29,UNCONN_OUT_30,\count_clk[3]_i_2_n_0 }));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[4] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[4]),
        .Q(UNCONN_OUT_27),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[5] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[5]),
        .Q(UNCONN_OUT_26),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[6] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[6]),
        .Q(UNCONN_OUT_25),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[7] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[7]),
        .Q(UNCONN_OUT_24),
        .R(FLAG_state));
  CARRY4 \count_clk_reg[7]_i_1 
       (.CI(\count_clk_reg[3]_i_1_n_0 ),
        .CO({\count_clk_reg[7]_i_1_n_0 ,\count_clk_reg[7]_i_1_n_1 ,\count_clk_reg[7]_i_1_n_2 ,\count_clk_reg[7]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(data0[7:4]),
        .S({UNCONN_OUT_24,UNCONN_OUT_25,UNCONN_OUT_26,UNCONN_OUT_27}));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[8] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[8]),
        .Q(UNCONN_OUT_23),
        .R(FLAG_state));
  FDRE #(
    .INIT(1'b0),
    .IS_C_INVERTED(1'b1)) 
    \count_clk_reg[9] 
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(data0[9]),
        .Q(UNCONN_OUT_22),
        .R(FLAG_state));
  LUT2 #(
    .INIT(4'hD)) 
    \data[0]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT),
        .O(UNCONN_OUT));
  LUT2 #(
    .INIT(4'h8)) 
    \data[10]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_21),
        .O(UNCONN_OUT_21));
  LUT2 #(
    .INIT(4'h8)) 
    \data[11]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_20),
        .O(UNCONN_OUT_20));
  LUT2 #(
    .INIT(4'h8)) 
    \data[12]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_19),
        .O(UNCONN_OUT_19));
  LUT2 #(
    .INIT(4'h8)) 
    \data[13]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_18),
        .O(UNCONN_OUT_18));
  LUT2 #(
    .INIT(4'h8)) 
    \data[14]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_17),
        .O(UNCONN_OUT_17));
  LUT2 #(
    .INIT(4'h8)) 
    \data[15]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_16),
        .O(UNCONN_OUT_16));
  LUT2 #(
    .INIT(4'h8)) 
    \data[16]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_15),
        .O(UNCONN_OUT_15));
  LUT2 #(
    .INIT(4'h8)) 
    \data[17]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_14),
        .O(UNCONN_OUT_14));
  LUT2 #(
    .INIT(4'h8)) 
    \data[18]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_13),
        .O(UNCONN_OUT_13));
  LUT2 #(
    .INIT(4'h8)) 
    \data[19]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_12),
        .O(UNCONN_OUT_12));
  LUT2 #(
    .INIT(4'h8)) 
    \data[1]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_30),
        .O(UNCONN_OUT_30));
  LUT2 #(
    .INIT(4'h8)) 
    \data[20]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_11),
        .O(UNCONN_OUT_11));
  LUT2 #(
    .INIT(4'h8)) 
    \data[21]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_10),
        .O(UNCONN_OUT_10));
  LUT2 #(
    .INIT(4'h8)) 
    \data[22]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_9),
        .O(UNCONN_OUT_9));
  LUT2 #(
    .INIT(4'h8)) 
    \data[23]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_8),
        .O(UNCONN_OUT_8));
  LUT2 #(
    .INIT(4'h8)) 
    \data[24]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_7),
        .O(UNCONN_OUT_7));
  LUT2 #(
    .INIT(4'h8)) 
    \data[25]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_6),
        .O(UNCONN_OUT_6));
  LUT2 #(
    .INIT(4'h8)) 
    \data[26]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_5),
        .O(UNCONN_OUT_5));
  LUT2 #(
    .INIT(4'h8)) 
    \data[27]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_4),
        .O(UNCONN_OUT_4));
  LUT2 #(
    .INIT(4'h8)) 
    \data[28]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_3),
        .O(UNCONN_OUT_3));
  LUT2 #(
    .INIT(4'h8)) 
    \data[29]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_2),
        .O(UNCONN_OUT_2));
  LUT2 #(
    .INIT(4'h8)) 
    \data[2]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_29),
        .O(UNCONN_OUT_29));
  LUT2 #(
    .INIT(4'h8)) 
    \data[30]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_1),
        .O(UNCONN_OUT_1));
  LUT2 #(
    .INIT(4'h8)) 
    \data[31]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_0),
        .O(UNCONN_OUT_0));
  LUT2 #(
    .INIT(4'h8)) 
    \data[3]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_28),
        .O(UNCONN_OUT_28));
  LUT2 #(
    .INIT(4'h8)) 
    \data[4]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_27),
        .O(UNCONN_OUT_27));
  LUT2 #(
    .INIT(4'h8)) 
    \data[5]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_26),
        .O(UNCONN_OUT_26));
  LUT2 #(
    .INIT(4'h8)) 
    \data[6]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_25),
        .O(UNCONN_OUT_25));
  LUT2 #(
    .INIT(4'h8)) 
    \data[7]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_24),
        .O(UNCONN_OUT_24));
  LUT2 #(
    .INIT(4'h8)) 
    \data[8]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_23),
        .O(UNCONN_OUT_23));
  LUT2 #(
    .INIT(4'h8)) 
    \data[9]_INST_0 
       (.I0(FLAG_state_reg_n_0),
        .I1(UNCONN_OUT_22),
        .O(UNCONN_OUT_22));
  LUT6 #(
    .INIT(64'hFFFFFFFD00000001)) 
    write_reg_i_1
       (.I0(FLAG_state_reg_n_0),
        .I1(\count_clk[31]_i_7_n_0 ),
        .I2(\count_clk[31]_i_6_n_0 ),
        .I3(\count_clk[31]_i_5_n_0 ),
        .I4(\count_clk[31]_i_4_n_0 ),
        .I5(write_reg_reg_0),
        .O(write_reg_i_1_n_0));
  FDRE #(
    .INIT(1'b1),
    .IS_C_INVERTED(1'b1)) 
    write_reg_reg
       (.C(\count_clk[31]_i_3_n_0 ),
        .CE(1'b1),
        .D(write_reg_i_1_n_0),
        .Q(write_reg_reg_0),
        .R(1'b0));
endmodule

(* CHECK_LICENSE_TYPE = "design_1_ADC_imitation_0_0,ADC_imitation,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* X_CORE_INFO = "ADC_imitation,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (clk_5MHz,
    clk_100MHz,
    s00_axi_aresetn,
    last_word_transmit,
    adr_read_ram,
    clk_5MHzWR_100MhzRD,
    clk_100Mhz_AXI_send,
    adr_bram,
    data,
    write);
  input clk_5MHz;
  input clk_100MHz;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *) input s00_axi_aresetn;
  input last_word_transmit;
  input [31:0]adr_read_ram;
  output clk_5MHzWR_100MhzRD;
  output clk_100Mhz_AXI_send;
  output [31:0]adr_bram;
  output [31:0]data;
  output write;

  wire [31:1]\^adr_bram ;
  wire clk_100MHz;
  wire clk_100Mhz_AXI_send;
  wire clk_5MHz;
  wire clk_5MHzWR_100MhzRD;
  wire [0:0]\^data ;
  wire s00_axi_aresetn;
  wire write;

  assign adr_bram[31:1] = \^adr_bram [31:1];
  assign adr_bram[0] = \^data [0];
  assign data[31:1] = \^adr_bram [31:1];
  assign data[0] = \^data [0];
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation inst
       (.UNCONN_OUT(\^data ),
        .UNCONN_OUT_0(\^adr_bram [31]),
        .UNCONN_OUT_1(\^adr_bram [30]),
        .UNCONN_OUT_10(\^adr_bram [21]),
        .UNCONN_OUT_11(\^adr_bram [20]),
        .UNCONN_OUT_12(\^adr_bram [19]),
        .UNCONN_OUT_13(\^adr_bram [18]),
        .UNCONN_OUT_14(\^adr_bram [17]),
        .UNCONN_OUT_15(\^adr_bram [16]),
        .UNCONN_OUT_16(\^adr_bram [15]),
        .UNCONN_OUT_17(\^adr_bram [14]),
        .UNCONN_OUT_18(\^adr_bram [13]),
        .UNCONN_OUT_19(\^adr_bram [12]),
        .UNCONN_OUT_2(\^adr_bram [29]),
        .UNCONN_OUT_20(\^adr_bram [11]),
        .UNCONN_OUT_21(\^adr_bram [10]),
        .UNCONN_OUT_22(\^adr_bram [9]),
        .UNCONN_OUT_23(\^adr_bram [8]),
        .UNCONN_OUT_24(\^adr_bram [7]),
        .UNCONN_OUT_25(\^adr_bram [6]),
        .UNCONN_OUT_26(\^adr_bram [5]),
        .UNCONN_OUT_27(\^adr_bram [4]),
        .UNCONN_OUT_28(\^adr_bram [3]),
        .UNCONN_OUT_29(\^adr_bram [2]),
        .UNCONN_OUT_3(\^adr_bram [28]),
        .UNCONN_OUT_30(\^adr_bram [1]),
        .UNCONN_OUT_4(\^adr_bram [27]),
        .UNCONN_OUT_5(\^adr_bram [26]),
        .UNCONN_OUT_6(\^adr_bram [25]),
        .UNCONN_OUT_7(\^adr_bram [24]),
        .UNCONN_OUT_8(\^adr_bram [23]),
        .UNCONN_OUT_9(\^adr_bram [22]),
        .clk_100MHz(clk_100MHz),
        .clk_100Mhz_AXI_send(clk_100Mhz_AXI_send),
        .clk_5MHz(clk_5MHz),
        .clk_5MHzWR_100MhzRD(clk_5MHzWR_100MhzRD),
        .s00_axi_aresetn(s00_axi_aresetn),
        .write_reg_reg_0(write));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif
