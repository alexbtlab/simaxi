-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Jun  6 22:15:45 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_imitation_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  port (
    UNCONN_OUT : out STD_LOGIC;
    clk_100Mhz_AXI_send : out STD_LOGIC;
    write_reg_reg_0 : out STD_LOGIC;
    UNCONN_OUT_0 : out STD_LOGIC;
    UNCONN_OUT_1 : out STD_LOGIC;
    UNCONN_OUT_2 : out STD_LOGIC;
    UNCONN_OUT_3 : out STD_LOGIC;
    UNCONN_OUT_4 : out STD_LOGIC;
    UNCONN_OUT_5 : out STD_LOGIC;
    UNCONN_OUT_6 : out STD_LOGIC;
    UNCONN_OUT_7 : out STD_LOGIC;
    UNCONN_OUT_8 : out STD_LOGIC;
    UNCONN_OUT_9 : out STD_LOGIC;
    UNCONN_OUT_10 : out STD_LOGIC;
    UNCONN_OUT_11 : out STD_LOGIC;
    UNCONN_OUT_12 : out STD_LOGIC;
    UNCONN_OUT_13 : out STD_LOGIC;
    UNCONN_OUT_14 : out STD_LOGIC;
    UNCONN_OUT_15 : out STD_LOGIC;
    UNCONN_OUT_16 : out STD_LOGIC;
    UNCONN_OUT_17 : out STD_LOGIC;
    UNCONN_OUT_18 : out STD_LOGIC;
    UNCONN_OUT_19 : out STD_LOGIC;
    UNCONN_OUT_20 : out STD_LOGIC;
    UNCONN_OUT_21 : out STD_LOGIC;
    UNCONN_OUT_22 : out STD_LOGIC;
    UNCONN_OUT_23 : out STD_LOGIC;
    UNCONN_OUT_24 : out STD_LOGIC;
    UNCONN_OUT_25 : out STD_LOGIC;
    UNCONN_OUT_26 : out STD_LOGIC;
    UNCONN_OUT_27 : out STD_LOGIC;
    UNCONN_OUT_28 : out STD_LOGIC;
    UNCONN_OUT_29 : out STD_LOGIC;
    UNCONN_OUT_30 : out STD_LOGIC;
    clk_5MHzWR_100MhzRD : out STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    clk_5MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  signal FLAG_state : STD_LOGIC;
  signal FLAG_state_i_1_n_0 : STD_LOGIC;
  signal FLAG_state_reg_n_0 : STD_LOGIC;
  signal \^unconn_out\ : STD_LOGIC;
  signal \^unconn_out_0\ : STD_LOGIC;
  signal \^unconn_out_1\ : STD_LOGIC;
  signal \^unconn_out_10\ : STD_LOGIC;
  signal \^unconn_out_11\ : STD_LOGIC;
  signal \^unconn_out_12\ : STD_LOGIC;
  signal \^unconn_out_13\ : STD_LOGIC;
  signal \^unconn_out_14\ : STD_LOGIC;
  signal \^unconn_out_15\ : STD_LOGIC;
  signal \^unconn_out_16\ : STD_LOGIC;
  signal \^unconn_out_17\ : STD_LOGIC;
  signal \^unconn_out_18\ : STD_LOGIC;
  signal \^unconn_out_19\ : STD_LOGIC;
  signal \^unconn_out_2\ : STD_LOGIC;
  signal \^unconn_out_20\ : STD_LOGIC;
  signal \^unconn_out_21\ : STD_LOGIC;
  signal \^unconn_out_22\ : STD_LOGIC;
  signal \^unconn_out_23\ : STD_LOGIC;
  signal \^unconn_out_24\ : STD_LOGIC;
  signal \^unconn_out_25\ : STD_LOGIC;
  signal \^unconn_out_26\ : STD_LOGIC;
  signal \^unconn_out_27\ : STD_LOGIC;
  signal \^unconn_out_28\ : STD_LOGIC;
  signal \^unconn_out_29\ : STD_LOGIC;
  signal \^unconn_out_3\ : STD_LOGIC;
  signal \^unconn_out_30\ : STD_LOGIC;
  signal \^unconn_out_4\ : STD_LOGIC;
  signal \^unconn_out_5\ : STD_LOGIC;
  signal \^unconn_out_6\ : STD_LOGIC;
  signal \^unconn_out_7\ : STD_LOGIC;
  signal \^unconn_out_8\ : STD_LOGIC;
  signal \^unconn_out_9\ : STD_LOGIC;
  signal \count_clk[31]_i_10_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_11_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_3_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_4_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_5_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_6_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_7_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_8_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_9_n_0\ : STD_LOGIC;
  signal \count_clk[3]_i_2_n_0\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_2_n_1\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal write_reg_i_1_n_0 : STD_LOGIC;
  signal \^write_reg_reg_0\ : STD_LOGIC;
  signal \NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of clk_100Mhz_AXI_send_INST_0 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of clk_5MHzWR_100MhzRD_INST_0 : label is "soft_lutpair0";
begin
  UNCONN_OUT <= \^unconn_out\;
  UNCONN_OUT_0 <= \^unconn_out_0\;
  UNCONN_OUT_1 <= \^unconn_out_1\;
  UNCONN_OUT_10 <= \^unconn_out_10\;
  UNCONN_OUT_11 <= \^unconn_out_11\;
  UNCONN_OUT_12 <= \^unconn_out_12\;
  UNCONN_OUT_13 <= \^unconn_out_13\;
  UNCONN_OUT_14 <= \^unconn_out_14\;
  UNCONN_OUT_15 <= \^unconn_out_15\;
  UNCONN_OUT_16 <= \^unconn_out_16\;
  UNCONN_OUT_17 <= \^unconn_out_17\;
  UNCONN_OUT_18 <= \^unconn_out_18\;
  UNCONN_OUT_19 <= \^unconn_out_19\;
  UNCONN_OUT_2 <= \^unconn_out_2\;
  UNCONN_OUT_20 <= \^unconn_out_20\;
  UNCONN_OUT_21 <= \^unconn_out_21\;
  UNCONN_OUT_22 <= \^unconn_out_22\;
  UNCONN_OUT_23 <= \^unconn_out_23\;
  UNCONN_OUT_24 <= \^unconn_out_24\;
  UNCONN_OUT_25 <= \^unconn_out_25\;
  UNCONN_OUT_26 <= \^unconn_out_26\;
  UNCONN_OUT_27 <= \^unconn_out_27\;
  UNCONN_OUT_28 <= \^unconn_out_28\;
  UNCONN_OUT_29 <= \^unconn_out_29\;
  UNCONN_OUT_3 <= \^unconn_out_3\;
  UNCONN_OUT_30 <= \^unconn_out_30\;
  UNCONN_OUT_4 <= \^unconn_out_4\;
  UNCONN_OUT_5 <= \^unconn_out_5\;
  UNCONN_OUT_6 <= \^unconn_out_6\;
  UNCONN_OUT_7 <= \^unconn_out_7\;
  UNCONN_OUT_8 <= \^unconn_out_8\;
  UNCONN_OUT_9 <= \^unconn_out_9\;
  write_reg_reg_0 <= \^write_reg_reg_0\;
FLAG_state_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => \count_clk[31]_i_4_n_0\,
      I1 => \count_clk[31]_i_5_n_0\,
      I2 => \count_clk[31]_i_6_n_0\,
      I3 => \count_clk[31]_i_7_n_0\,
      I4 => FLAG_state_reg_n_0,
      O => FLAG_state_i_1_n_0
    );
FLAG_state_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => FLAG_state_i_1_n_0,
      Q => FLAG_state_reg_n_0,
      R => '0'
    );
clk_100Mhz_AXI_send_INST_0: unisim.vcomponents.LUT2
    generic map(
      INIT => X"B"
    )
        port map (
      I0 => \^write_reg_reg_0\,
      I1 => clk_100MHz,
      O => clk_100Mhz_AXI_send
    );
clk_5MHzWR_100MhzRD_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => clk_5MHz,
      I1 => \^write_reg_reg_0\,
      I2 => clk_100MHz,
      O => clk_5MHzWR_100MhzRD
    );
\count_clk[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \count_clk[31]_i_4_n_0\,
      I1 => \count_clk[31]_i_5_n_0\,
      I2 => \count_clk[31]_i_6_n_0\,
      I3 => \count_clk[31]_i_7_n_0\,
      O => FLAG_state
    );
\count_clk[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \^unconn_out_26\,
      I1 => \^unconn_out_27\,
      I2 => \^unconn_out_24\,
      I3 => \^unconn_out_25\,
      O => \count_clk[31]_i_10_n_0\
    );
\count_clk[31]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^unconn_out_18\,
      I1 => \^unconn_out_19\,
      I2 => \^unconn_out_16\,
      I3 => \^unconn_out_17\,
      O => \count_clk[31]_i_11_n_0\
    );
\count_clk[31]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"E200"
    )
        port map (
      I0 => clk_100MHz,
      I1 => \^write_reg_reg_0\,
      I2 => clk_5MHz,
      I3 => s00_axi_aresetn,
      O => \count_clk[31]_i_3_n_0\
    );
\count_clk[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^unconn_out_13\,
      I1 => \^unconn_out_12\,
      I2 => \^unconn_out_15\,
      I3 => \^unconn_out_14\,
      I4 => \count_clk[31]_i_8_n_0\,
      O => \count_clk[31]_i_4_n_0\
    );
\count_clk[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^unconn_out_5\,
      I1 => \^unconn_out_4\,
      I2 => \^unconn_out_7\,
      I3 => \^unconn_out_6\,
      I4 => \count_clk[31]_i_9_n_0\,
      O => \count_clk[31]_i_5_n_0\
    );
\count_clk[31]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \^unconn_out_29\,
      I1 => \^unconn_out_28\,
      I2 => \^unconn_out\,
      I3 => \^unconn_out_30\,
      I4 => \count_clk[31]_i_10_n_0\,
      O => \count_clk[31]_i_6_n_0\
    );
\count_clk[31]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^unconn_out_21\,
      I1 => \^unconn_out_20\,
      I2 => \^unconn_out_23\,
      I3 => \^unconn_out_22\,
      I4 => \count_clk[31]_i_11_n_0\,
      O => \count_clk[31]_i_7_n_0\
    );
\count_clk[31]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^unconn_out_10\,
      I1 => \^unconn_out_11\,
      I2 => \^unconn_out_8\,
      I3 => \^unconn_out_9\,
      O => \count_clk[31]_i_8_n_0\
    );
\count_clk[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^unconn_out_2\,
      I1 => \^unconn_out_3\,
      I2 => \^unconn_out_0\,
      I3 => \^unconn_out_1\,
      O => \count_clk[31]_i_9_n_0\
    );
\count_clk[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^unconn_out\,
      O => \count_clk[3]_i_2_n_0\
    );
\count_clk_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(0),
      Q => \^unconn_out\,
      R => FLAG_state
    );
\count_clk_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(10),
      Q => \^unconn_out_21\,
      R => FLAG_state
    );
\count_clk_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(11),
      Q => \^unconn_out_20\,
      R => FLAG_state
    );
\count_clk_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[7]_i_1_n_0\,
      CO(3) => \count_clk_reg[11]_i_1_n_0\,
      CO(2) => \count_clk_reg[11]_i_1_n_1\,
      CO(1) => \count_clk_reg[11]_i_1_n_2\,
      CO(0) => \count_clk_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(11 downto 8),
      S(3) => \^unconn_out_20\,
      S(2) => \^unconn_out_21\,
      S(1) => \^unconn_out_22\,
      S(0) => \^unconn_out_23\
    );
\count_clk_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(12),
      Q => \^unconn_out_19\,
      R => FLAG_state
    );
\count_clk_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(13),
      Q => \^unconn_out_18\,
      R => FLAG_state
    );
\count_clk_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(14),
      Q => \^unconn_out_17\,
      R => FLAG_state
    );
\count_clk_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(15),
      Q => \^unconn_out_16\,
      R => FLAG_state
    );
\count_clk_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[11]_i_1_n_0\,
      CO(3) => \count_clk_reg[15]_i_1_n_0\,
      CO(2) => \count_clk_reg[15]_i_1_n_1\,
      CO(1) => \count_clk_reg[15]_i_1_n_2\,
      CO(0) => \count_clk_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(15 downto 12),
      S(3) => \^unconn_out_16\,
      S(2) => \^unconn_out_17\,
      S(1) => \^unconn_out_18\,
      S(0) => \^unconn_out_19\
    );
\count_clk_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(16),
      Q => \^unconn_out_15\,
      R => FLAG_state
    );
\count_clk_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(17),
      Q => \^unconn_out_14\,
      R => FLAG_state
    );
\count_clk_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(18),
      Q => \^unconn_out_13\,
      R => FLAG_state
    );
\count_clk_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(19),
      Q => \^unconn_out_12\,
      R => FLAG_state
    );
\count_clk_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[15]_i_1_n_0\,
      CO(3) => \count_clk_reg[19]_i_1_n_0\,
      CO(2) => \count_clk_reg[19]_i_1_n_1\,
      CO(1) => \count_clk_reg[19]_i_1_n_2\,
      CO(0) => \count_clk_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(19 downto 16),
      S(3) => \^unconn_out_12\,
      S(2) => \^unconn_out_13\,
      S(1) => \^unconn_out_14\,
      S(0) => \^unconn_out_15\
    );
\count_clk_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(1),
      Q => \^unconn_out_30\,
      R => FLAG_state
    );
\count_clk_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(20),
      Q => \^unconn_out_11\,
      R => FLAG_state
    );
\count_clk_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(21),
      Q => \^unconn_out_10\,
      R => FLAG_state
    );
\count_clk_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(22),
      Q => \^unconn_out_9\,
      R => FLAG_state
    );
\count_clk_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(23),
      Q => \^unconn_out_8\,
      R => FLAG_state
    );
\count_clk_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[19]_i_1_n_0\,
      CO(3) => \count_clk_reg[23]_i_1_n_0\,
      CO(2) => \count_clk_reg[23]_i_1_n_1\,
      CO(1) => \count_clk_reg[23]_i_1_n_2\,
      CO(0) => \count_clk_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(23 downto 20),
      S(3) => \^unconn_out_8\,
      S(2) => \^unconn_out_9\,
      S(1) => \^unconn_out_10\,
      S(0) => \^unconn_out_11\
    );
\count_clk_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(24),
      Q => \^unconn_out_7\,
      R => FLAG_state
    );
\count_clk_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(25),
      Q => \^unconn_out_6\,
      R => FLAG_state
    );
\count_clk_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(26),
      Q => \^unconn_out_5\,
      R => FLAG_state
    );
\count_clk_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(27),
      Q => \^unconn_out_4\,
      R => FLAG_state
    );
\count_clk_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[23]_i_1_n_0\,
      CO(3) => \count_clk_reg[27]_i_1_n_0\,
      CO(2) => \count_clk_reg[27]_i_1_n_1\,
      CO(1) => \count_clk_reg[27]_i_1_n_2\,
      CO(0) => \count_clk_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(27 downto 24),
      S(3) => \^unconn_out_4\,
      S(2) => \^unconn_out_5\,
      S(1) => \^unconn_out_6\,
      S(0) => \^unconn_out_7\
    );
\count_clk_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(28),
      Q => \^unconn_out_3\,
      R => FLAG_state
    );
\count_clk_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(29),
      Q => \^unconn_out_2\,
      R => FLAG_state
    );
\count_clk_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(2),
      Q => \^unconn_out_29\,
      R => FLAG_state
    );
\count_clk_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(30),
      Q => \^unconn_out_1\,
      R => FLAG_state
    );
\count_clk_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(31),
      Q => \^unconn_out_0\,
      R => FLAG_state
    );
\count_clk_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[27]_i_1_n_0\,
      CO(3) => \NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \count_clk_reg[31]_i_2_n_1\,
      CO(1) => \count_clk_reg[31]_i_2_n_2\,
      CO(0) => \count_clk_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(31 downto 28),
      S(3) => \^unconn_out_0\,
      S(2) => \^unconn_out_1\,
      S(1) => \^unconn_out_2\,
      S(0) => \^unconn_out_3\
    );
\count_clk_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(3),
      Q => \^unconn_out_28\,
      R => FLAG_state
    );
\count_clk_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_clk_reg[3]_i_1_n_0\,
      CO(2) => \count_clk_reg[3]_i_1_n_1\,
      CO(1) => \count_clk_reg[3]_i_1_n_2\,
      CO(0) => \count_clk_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 1) => B"000",
      DI(0) => \^unconn_out\,
      O(3 downto 0) => data0(3 downto 0),
      S(3) => \^unconn_out_28\,
      S(2) => \^unconn_out_29\,
      S(1) => \^unconn_out_30\,
      S(0) => \count_clk[3]_i_2_n_0\
    );
\count_clk_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(4),
      Q => \^unconn_out_27\,
      R => FLAG_state
    );
\count_clk_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(5),
      Q => \^unconn_out_26\,
      R => FLAG_state
    );
\count_clk_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(6),
      Q => \^unconn_out_25\,
      R => FLAG_state
    );
\count_clk_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(7),
      Q => \^unconn_out_24\,
      R => FLAG_state
    );
\count_clk_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[3]_i_1_n_0\,
      CO(3) => \count_clk_reg[7]_i_1_n_0\,
      CO(2) => \count_clk_reg[7]_i_1_n_1\,
      CO(1) => \count_clk_reg[7]_i_1_n_2\,
      CO(0) => \count_clk_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(7 downto 4),
      S(3) => \^unconn_out_24\,
      S(2) => \^unconn_out_25\,
      S(1) => \^unconn_out_26\,
      S(0) => \^unconn_out_27\
    );
\count_clk_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(8),
      Q => \^unconn_out_23\,
      R => FLAG_state
    );
\count_clk_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(9),
      Q => \^unconn_out_22\,
      R => FLAG_state
    );
\data[0]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"D"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out\,
      O => \^unconn_out\
    );
\data[10]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_21\,
      O => \^unconn_out_21\
    );
\data[11]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_20\,
      O => \^unconn_out_20\
    );
\data[12]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_19\,
      O => \^unconn_out_19\
    );
\data[13]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_18\,
      O => \^unconn_out_18\
    );
\data[14]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_17\,
      O => \^unconn_out_17\
    );
\data[15]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_16\,
      O => \^unconn_out_16\
    );
\data[16]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_15\,
      O => \^unconn_out_15\
    );
\data[17]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_14\,
      O => \^unconn_out_14\
    );
\data[18]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_13\,
      O => \^unconn_out_13\
    );
\data[19]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_12\,
      O => \^unconn_out_12\
    );
\data[1]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_30\,
      O => \^unconn_out_30\
    );
\data[20]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_11\,
      O => \^unconn_out_11\
    );
\data[21]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_10\,
      O => \^unconn_out_10\
    );
\data[22]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_9\,
      O => \^unconn_out_9\
    );
\data[23]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_8\,
      O => \^unconn_out_8\
    );
\data[24]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_7\,
      O => \^unconn_out_7\
    );
\data[25]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_6\,
      O => \^unconn_out_6\
    );
\data[26]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_5\,
      O => \^unconn_out_5\
    );
\data[27]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_4\,
      O => \^unconn_out_4\
    );
\data[28]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_3\,
      O => \^unconn_out_3\
    );
\data[29]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_2\,
      O => \^unconn_out_2\
    );
\data[2]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_29\,
      O => \^unconn_out_29\
    );
\data[30]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_1\,
      O => \^unconn_out_1\
    );
\data[31]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_0\,
      O => \^unconn_out_0\
    );
\data[3]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_28\,
      O => \^unconn_out_28\
    );
\data[4]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_27\,
      O => \^unconn_out_27\
    );
\data[5]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_26\,
      O => \^unconn_out_26\
    );
\data[6]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_25\,
      O => \^unconn_out_25\
    );
\data[7]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_24\,
      O => \^unconn_out_24\
    );
\data[8]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_23\,
      O => \^unconn_out_23\
    );
\data[9]_INST_0\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \^unconn_out_22\,
      O => \^unconn_out_22\
    );
write_reg_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFD00000001"
    )
        port map (
      I0 => FLAG_state_reg_n_0,
      I1 => \count_clk[31]_i_7_n_0\,
      I2 => \count_clk[31]_i_6_n_0\,
      I3 => \count_clk[31]_i_5_n_0\,
      I4 => \count_clk[31]_i_4_n_0\,
      I5 => \^write_reg_reg_0\,
      O => write_reg_i_1_n_0
    );
write_reg_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '1',
      IS_C_INVERTED => '1'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => write_reg_i_1_n_0,
      Q => \^write_reg_reg_0\,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_5MHz : in STD_LOGIC;
    clk_100MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    last_word_transmit : in STD_LOGIC;
    adr_read_ram : in STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_5MHzWR_100MhzRD : out STD_LOGIC;
    clk_100Mhz_AXI_send : out STD_LOGIC;
    adr_bram : out STD_LOGIC_VECTOR ( 31 downto 0 );
    data : out STD_LOGIC_VECTOR ( 31 downto 0 );
    write : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_imitation_0_0,ADC_imitation,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_imitation,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \^adr_bram\ : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \^data\ : STD_LOGIC_VECTOR ( 0 to 0 );
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of s00_axi_aresetn : signal is "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of s00_axi_aresetn : signal is "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
begin
  adr_bram(31 downto 1) <= \^adr_bram\(31 downto 1);
  adr_bram(0) <= \^data\(0);
  data(31 downto 1) <= \^adr_bram\(31 downto 1);
  data(0) <= \^data\(0);
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
     port map (
      UNCONN_OUT => \^data\(0),
      UNCONN_OUT_0 => \^adr_bram\(31),
      UNCONN_OUT_1 => \^adr_bram\(30),
      UNCONN_OUT_10 => \^adr_bram\(21),
      UNCONN_OUT_11 => \^adr_bram\(20),
      UNCONN_OUT_12 => \^adr_bram\(19),
      UNCONN_OUT_13 => \^adr_bram\(18),
      UNCONN_OUT_14 => \^adr_bram\(17),
      UNCONN_OUT_15 => \^adr_bram\(16),
      UNCONN_OUT_16 => \^adr_bram\(15),
      UNCONN_OUT_17 => \^adr_bram\(14),
      UNCONN_OUT_18 => \^adr_bram\(13),
      UNCONN_OUT_19 => \^adr_bram\(12),
      UNCONN_OUT_2 => \^adr_bram\(29),
      UNCONN_OUT_20 => \^adr_bram\(11),
      UNCONN_OUT_21 => \^adr_bram\(10),
      UNCONN_OUT_22 => \^adr_bram\(9),
      UNCONN_OUT_23 => \^adr_bram\(8),
      UNCONN_OUT_24 => \^adr_bram\(7),
      UNCONN_OUT_25 => \^adr_bram\(6),
      UNCONN_OUT_26 => \^adr_bram\(5),
      UNCONN_OUT_27 => \^adr_bram\(4),
      UNCONN_OUT_28 => \^adr_bram\(3),
      UNCONN_OUT_29 => \^adr_bram\(2),
      UNCONN_OUT_3 => \^adr_bram\(28),
      UNCONN_OUT_30 => \^adr_bram\(1),
      UNCONN_OUT_4 => \^adr_bram\(27),
      UNCONN_OUT_5 => \^adr_bram\(26),
      UNCONN_OUT_6 => \^adr_bram\(25),
      UNCONN_OUT_7 => \^adr_bram\(24),
      UNCONN_OUT_8 => \^adr_bram\(23),
      UNCONN_OUT_9 => \^adr_bram\(22),
      clk_100MHz => clk_100MHz,
      clk_100Mhz_AXI_send => clk_100Mhz_AXI_send,
      clk_5MHz => clk_5MHz,
      clk_5MHzWR_100MhzRD => clk_5MHzWR_100MhzRD,
      s00_axi_aresetn => s00_axi_aresetn,
      write_reg_reg_0 => write
    );
end STRUCTURE;
