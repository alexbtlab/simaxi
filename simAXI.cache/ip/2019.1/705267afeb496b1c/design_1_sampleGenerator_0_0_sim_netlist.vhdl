-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Jun  3 20:57:37 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_sampleGenerator_0_0_sim_netlist.vhdl
-- Design      : design_1_sampleGenerator_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    tValidR_reg_0 : out STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS is
  signal \afterResetCycleCounterR[7]_i_2_n_0\ : STD_LOGIC;
  signal afterResetCycleCounterR_reg : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal clear : STD_LOGIC;
  signal \counterR[3]_i_2_n_0\ : STD_LOGIC;
  signal \counterR_reg[11]_i_1_n_0\ : STD_LOGIC;
  signal \counterR_reg[11]_i_1_n_1\ : STD_LOGIC;
  signal \counterR_reg[11]_i_1_n_2\ : STD_LOGIC;
  signal \counterR_reg[11]_i_1_n_3\ : STD_LOGIC;
  signal \counterR_reg[11]_i_1_n_4\ : STD_LOGIC;
  signal \counterR_reg[11]_i_1_n_5\ : STD_LOGIC;
  signal \counterR_reg[11]_i_1_n_6\ : STD_LOGIC;
  signal \counterR_reg[11]_i_1_n_7\ : STD_LOGIC;
  signal \counterR_reg[15]_i_1_n_0\ : STD_LOGIC;
  signal \counterR_reg[15]_i_1_n_1\ : STD_LOGIC;
  signal \counterR_reg[15]_i_1_n_2\ : STD_LOGIC;
  signal \counterR_reg[15]_i_1_n_3\ : STD_LOGIC;
  signal \counterR_reg[15]_i_1_n_4\ : STD_LOGIC;
  signal \counterR_reg[15]_i_1_n_5\ : STD_LOGIC;
  signal \counterR_reg[15]_i_1_n_6\ : STD_LOGIC;
  signal \counterR_reg[15]_i_1_n_7\ : STD_LOGIC;
  signal \counterR_reg[19]_i_1_n_0\ : STD_LOGIC;
  signal \counterR_reg[19]_i_1_n_1\ : STD_LOGIC;
  signal \counterR_reg[19]_i_1_n_2\ : STD_LOGIC;
  signal \counterR_reg[19]_i_1_n_3\ : STD_LOGIC;
  signal \counterR_reg[19]_i_1_n_4\ : STD_LOGIC;
  signal \counterR_reg[19]_i_1_n_5\ : STD_LOGIC;
  signal \counterR_reg[19]_i_1_n_6\ : STD_LOGIC;
  signal \counterR_reg[19]_i_1_n_7\ : STD_LOGIC;
  signal \counterR_reg[23]_i_1_n_0\ : STD_LOGIC;
  signal \counterR_reg[23]_i_1_n_1\ : STD_LOGIC;
  signal \counterR_reg[23]_i_1_n_2\ : STD_LOGIC;
  signal \counterR_reg[23]_i_1_n_3\ : STD_LOGIC;
  signal \counterR_reg[23]_i_1_n_4\ : STD_LOGIC;
  signal \counterR_reg[23]_i_1_n_5\ : STD_LOGIC;
  signal \counterR_reg[23]_i_1_n_6\ : STD_LOGIC;
  signal \counterR_reg[23]_i_1_n_7\ : STD_LOGIC;
  signal \counterR_reg[27]_i_1_n_0\ : STD_LOGIC;
  signal \counterR_reg[27]_i_1_n_1\ : STD_LOGIC;
  signal \counterR_reg[27]_i_1_n_2\ : STD_LOGIC;
  signal \counterR_reg[27]_i_1_n_3\ : STD_LOGIC;
  signal \counterR_reg[27]_i_1_n_4\ : STD_LOGIC;
  signal \counterR_reg[27]_i_1_n_5\ : STD_LOGIC;
  signal \counterR_reg[27]_i_1_n_6\ : STD_LOGIC;
  signal \counterR_reg[27]_i_1_n_7\ : STD_LOGIC;
  signal \counterR_reg[31]_i_2_n_1\ : STD_LOGIC;
  signal \counterR_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \counterR_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \counterR_reg[31]_i_2_n_4\ : STD_LOGIC;
  signal \counterR_reg[31]_i_2_n_5\ : STD_LOGIC;
  signal \counterR_reg[31]_i_2_n_6\ : STD_LOGIC;
  signal \counterR_reg[31]_i_2_n_7\ : STD_LOGIC;
  signal \counterR_reg[3]_i_1_n_0\ : STD_LOGIC;
  signal \counterR_reg[3]_i_1_n_1\ : STD_LOGIC;
  signal \counterR_reg[3]_i_1_n_2\ : STD_LOGIC;
  signal \counterR_reg[3]_i_1_n_3\ : STD_LOGIC;
  signal \counterR_reg[3]_i_1_n_4\ : STD_LOGIC;
  signal \counterR_reg[3]_i_1_n_5\ : STD_LOGIC;
  signal \counterR_reg[3]_i_1_n_6\ : STD_LOGIC;
  signal \counterR_reg[3]_i_1_n_7\ : STD_LOGIC;
  signal \counterR_reg[7]_i_1_n_0\ : STD_LOGIC;
  signal \counterR_reg[7]_i_1_n_1\ : STD_LOGIC;
  signal \counterR_reg[7]_i_1_n_2\ : STD_LOGIC;
  signal \counterR_reg[7]_i_1_n_3\ : STD_LOGIC;
  signal \counterR_reg[7]_i_1_n_4\ : STD_LOGIC;
  signal \counterR_reg[7]_i_1_n_5\ : STD_LOGIC;
  signal \counterR_reg[7]_i_1_n_6\ : STD_LOGIC;
  signal \counterR_reg[7]_i_1_n_7\ : STD_LOGIC;
  signal \^m00_axis_tdata\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal p_0_in : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal sampleGeneratorEnR : STD_LOGIC;
  signal sampleGeneratorEnR_i_1_n_0 : STD_LOGIC;
  signal sampleGeneratorEnR_i_2_n_0 : STD_LOGIC;
  signal tValidR_i_1_n_0 : STD_LOGIC;
  signal \^tvalidr_reg_0\ : STD_LOGIC;
  signal \NLW_counterR_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \afterResetCycleCounterR[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \afterResetCycleCounterR[1]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \afterResetCycleCounterR[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \afterResetCycleCounterR[3]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \afterResetCycleCounterR[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \afterResetCycleCounterR[6]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \afterResetCycleCounterR[7]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of sampleGeneratorEnR_i_2 : label is "soft_lutpair1";
begin
  m00_axis_tdata(31 downto 0) <= \^m00_axis_tdata\(31 downto 0);
  tValidR_reg_0 <= \^tvalidr_reg_0\;
\afterResetCycleCounterR[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => afterResetCycleCounterR_reg(0),
      O => p_0_in(0)
    );
\afterResetCycleCounterR[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => afterResetCycleCounterR_reg(0),
      I1 => afterResetCycleCounterR_reg(1),
      O => p_0_in(1)
    );
\afterResetCycleCounterR[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => afterResetCycleCounterR_reg(0),
      I1 => afterResetCycleCounterR_reg(1),
      I2 => afterResetCycleCounterR_reg(2),
      O => p_0_in(2)
    );
\afterResetCycleCounterR[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => afterResetCycleCounterR_reg(1),
      I1 => afterResetCycleCounterR_reg(0),
      I2 => afterResetCycleCounterR_reg(2),
      I3 => afterResetCycleCounterR_reg(3),
      O => p_0_in(3)
    );
\afterResetCycleCounterR[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"7FFF8000"
    )
        port map (
      I0 => afterResetCycleCounterR_reg(2),
      I1 => afterResetCycleCounterR_reg(0),
      I2 => afterResetCycleCounterR_reg(1),
      I3 => afterResetCycleCounterR_reg(3),
      I4 => afterResetCycleCounterR_reg(4),
      O => p_0_in(4)
    );
\afterResetCycleCounterR[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFF80000000"
    )
        port map (
      I0 => afterResetCycleCounterR_reg(3),
      I1 => afterResetCycleCounterR_reg(1),
      I2 => afterResetCycleCounterR_reg(0),
      I3 => afterResetCycleCounterR_reg(2),
      I4 => afterResetCycleCounterR_reg(4),
      I5 => afterResetCycleCounterR_reg(5),
      O => p_0_in(5)
    );
\afterResetCycleCounterR[6]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \afterResetCycleCounterR[7]_i_2_n_0\,
      I1 => afterResetCycleCounterR_reg(6),
      O => p_0_in(6)
    );
\afterResetCycleCounterR[7]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"78"
    )
        port map (
      I0 => \afterResetCycleCounterR[7]_i_2_n_0\,
      I1 => afterResetCycleCounterR_reg(6),
      I2 => afterResetCycleCounterR_reg(7),
      O => p_0_in(7)
    );
\afterResetCycleCounterR[7]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000000000000"
    )
        port map (
      I0 => afterResetCycleCounterR_reg(5),
      I1 => afterResetCycleCounterR_reg(3),
      I2 => afterResetCycleCounterR_reg(1),
      I3 => afterResetCycleCounterR_reg(0),
      I4 => afterResetCycleCounterR_reg(2),
      I5 => afterResetCycleCounterR_reg(4),
      O => \afterResetCycleCounterR[7]_i_2_n_0\
    );
\afterResetCycleCounterR_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_0_in(0),
      Q => afterResetCycleCounterR_reg(0),
      R => clear
    );
\afterResetCycleCounterR_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_0_in(1),
      Q => afterResetCycleCounterR_reg(1),
      R => clear
    );
\afterResetCycleCounterR_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_0_in(2),
      Q => afterResetCycleCounterR_reg(2),
      R => clear
    );
\afterResetCycleCounterR_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_0_in(3),
      Q => afterResetCycleCounterR_reg(3),
      R => clear
    );
\afterResetCycleCounterR_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_0_in(4),
      Q => afterResetCycleCounterR_reg(4),
      R => clear
    );
\afterResetCycleCounterR_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_0_in(5),
      Q => afterResetCycleCounterR_reg(5),
      R => clear
    );
\afterResetCycleCounterR_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_0_in(6),
      Q => afterResetCycleCounterR_reg(6),
      R => clear
    );
\afterResetCycleCounterR_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => p_0_in(7),
      Q => afterResetCycleCounterR_reg(7),
      R => clear
    );
\counterR[31]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => m00_axis_aresetn,
      O => clear
    );
\counterR[3]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^m00_axis_tdata\(0),
      O => \counterR[3]_i_2_n_0\
    );
\counterR_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[3]_i_1_n_7\,
      Q => \^m00_axis_tdata\(0),
      R => clear
    );
\counterR_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[11]_i_1_n_5\,
      Q => \^m00_axis_tdata\(10),
      R => clear
    );
\counterR_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[11]_i_1_n_4\,
      Q => \^m00_axis_tdata\(11),
      R => clear
    );
\counterR_reg[11]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counterR_reg[7]_i_1_n_0\,
      CO(3) => \counterR_reg[11]_i_1_n_0\,
      CO(2) => \counterR_reg[11]_i_1_n_1\,
      CO(1) => \counterR_reg[11]_i_1_n_2\,
      CO(0) => \counterR_reg[11]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counterR_reg[11]_i_1_n_4\,
      O(2) => \counterR_reg[11]_i_1_n_5\,
      O(1) => \counterR_reg[11]_i_1_n_6\,
      O(0) => \counterR_reg[11]_i_1_n_7\,
      S(3 downto 0) => \^m00_axis_tdata\(11 downto 8)
    );
\counterR_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[15]_i_1_n_7\,
      Q => \^m00_axis_tdata\(12),
      R => clear
    );
\counterR_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[15]_i_1_n_6\,
      Q => \^m00_axis_tdata\(13),
      R => clear
    );
\counterR_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[15]_i_1_n_5\,
      Q => \^m00_axis_tdata\(14),
      R => clear
    );
\counterR_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[15]_i_1_n_4\,
      Q => \^m00_axis_tdata\(15),
      R => clear
    );
\counterR_reg[15]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counterR_reg[11]_i_1_n_0\,
      CO(3) => \counterR_reg[15]_i_1_n_0\,
      CO(2) => \counterR_reg[15]_i_1_n_1\,
      CO(1) => \counterR_reg[15]_i_1_n_2\,
      CO(0) => \counterR_reg[15]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counterR_reg[15]_i_1_n_4\,
      O(2) => \counterR_reg[15]_i_1_n_5\,
      O(1) => \counterR_reg[15]_i_1_n_6\,
      O(0) => \counterR_reg[15]_i_1_n_7\,
      S(3 downto 0) => \^m00_axis_tdata\(15 downto 12)
    );
\counterR_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[19]_i_1_n_7\,
      Q => \^m00_axis_tdata\(16),
      R => clear
    );
\counterR_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[19]_i_1_n_6\,
      Q => \^m00_axis_tdata\(17),
      R => clear
    );
\counterR_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[19]_i_1_n_5\,
      Q => \^m00_axis_tdata\(18),
      R => clear
    );
\counterR_reg[19]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[19]_i_1_n_4\,
      Q => \^m00_axis_tdata\(19),
      R => clear
    );
\counterR_reg[19]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counterR_reg[15]_i_1_n_0\,
      CO(3) => \counterR_reg[19]_i_1_n_0\,
      CO(2) => \counterR_reg[19]_i_1_n_1\,
      CO(1) => \counterR_reg[19]_i_1_n_2\,
      CO(0) => \counterR_reg[19]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counterR_reg[19]_i_1_n_4\,
      O(2) => \counterR_reg[19]_i_1_n_5\,
      O(1) => \counterR_reg[19]_i_1_n_6\,
      O(0) => \counterR_reg[19]_i_1_n_7\,
      S(3 downto 0) => \^m00_axis_tdata\(19 downto 16)
    );
\counterR_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[3]_i_1_n_6\,
      Q => \^m00_axis_tdata\(1),
      R => clear
    );
\counterR_reg[20]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[23]_i_1_n_7\,
      Q => \^m00_axis_tdata\(20),
      R => clear
    );
\counterR_reg[21]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[23]_i_1_n_6\,
      Q => \^m00_axis_tdata\(21),
      R => clear
    );
\counterR_reg[22]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[23]_i_1_n_5\,
      Q => \^m00_axis_tdata\(22),
      R => clear
    );
\counterR_reg[23]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[23]_i_1_n_4\,
      Q => \^m00_axis_tdata\(23),
      R => clear
    );
\counterR_reg[23]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counterR_reg[19]_i_1_n_0\,
      CO(3) => \counterR_reg[23]_i_1_n_0\,
      CO(2) => \counterR_reg[23]_i_1_n_1\,
      CO(1) => \counterR_reg[23]_i_1_n_2\,
      CO(0) => \counterR_reg[23]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counterR_reg[23]_i_1_n_4\,
      O(2) => \counterR_reg[23]_i_1_n_5\,
      O(1) => \counterR_reg[23]_i_1_n_6\,
      O(0) => \counterR_reg[23]_i_1_n_7\,
      S(3 downto 0) => \^m00_axis_tdata\(23 downto 20)
    );
\counterR_reg[24]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[27]_i_1_n_7\,
      Q => \^m00_axis_tdata\(24),
      R => clear
    );
\counterR_reg[25]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[27]_i_1_n_6\,
      Q => \^m00_axis_tdata\(25),
      R => clear
    );
\counterR_reg[26]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[27]_i_1_n_5\,
      Q => \^m00_axis_tdata\(26),
      R => clear
    );
\counterR_reg[27]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[27]_i_1_n_4\,
      Q => \^m00_axis_tdata\(27),
      R => clear
    );
\counterR_reg[27]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counterR_reg[23]_i_1_n_0\,
      CO(3) => \counterR_reg[27]_i_1_n_0\,
      CO(2) => \counterR_reg[27]_i_1_n_1\,
      CO(1) => \counterR_reg[27]_i_1_n_2\,
      CO(0) => \counterR_reg[27]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counterR_reg[27]_i_1_n_4\,
      O(2) => \counterR_reg[27]_i_1_n_5\,
      O(1) => \counterR_reg[27]_i_1_n_6\,
      O(0) => \counterR_reg[27]_i_1_n_7\,
      S(3 downto 0) => \^m00_axis_tdata\(27 downto 24)
    );
\counterR_reg[28]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[31]_i_2_n_7\,
      Q => \^m00_axis_tdata\(28),
      R => clear
    );
\counterR_reg[29]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[31]_i_2_n_6\,
      Q => \^m00_axis_tdata\(29),
      R => clear
    );
\counterR_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[3]_i_1_n_5\,
      Q => \^m00_axis_tdata\(2),
      R => clear
    );
\counterR_reg[30]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[31]_i_2_n_5\,
      Q => \^m00_axis_tdata\(30),
      R => clear
    );
\counterR_reg[31]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[31]_i_2_n_4\,
      Q => \^m00_axis_tdata\(31),
      R => clear
    );
\counterR_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \counterR_reg[27]_i_1_n_0\,
      CO(3) => \NLW_counterR_reg[31]_i_2_CO_UNCONNECTED\(3),
      CO(2) => \counterR_reg[31]_i_2_n_1\,
      CO(1) => \counterR_reg[31]_i_2_n_2\,
      CO(0) => \counterR_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counterR_reg[31]_i_2_n_4\,
      O(2) => \counterR_reg[31]_i_2_n_5\,
      O(1) => \counterR_reg[31]_i_2_n_6\,
      O(0) => \counterR_reg[31]_i_2_n_7\,
      S(3 downto 0) => \^m00_axis_tdata\(31 downto 28)
    );
\counterR_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[3]_i_1_n_4\,
      Q => \^m00_axis_tdata\(3),
      R => clear
    );
\counterR_reg[3]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \counterR_reg[3]_i_1_n_0\,
      CO(2) => \counterR_reg[3]_i_1_n_1\,
      CO(1) => \counterR_reg[3]_i_1_n_2\,
      CO(0) => \counterR_reg[3]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \counterR_reg[3]_i_1_n_4\,
      O(2) => \counterR_reg[3]_i_1_n_5\,
      O(1) => \counterR_reg[3]_i_1_n_6\,
      O(0) => \counterR_reg[3]_i_1_n_7\,
      S(3 downto 1) => \^m00_axis_tdata\(3 downto 1),
      S(0) => \counterR[3]_i_2_n_0\
    );
\counterR_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[7]_i_1_n_7\,
      Q => \^m00_axis_tdata\(4),
      R => clear
    );
\counterR_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[7]_i_1_n_6\,
      Q => \^m00_axis_tdata\(5),
      R => clear
    );
\counterR_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[7]_i_1_n_5\,
      Q => \^m00_axis_tdata\(6),
      R => clear
    );
\counterR_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[7]_i_1_n_4\,
      Q => \^m00_axis_tdata\(7),
      R => clear
    );
\counterR_reg[7]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \counterR_reg[3]_i_1_n_0\,
      CO(3) => \counterR_reg[7]_i_1_n_0\,
      CO(2) => \counterR_reg[7]_i_1_n_1\,
      CO(1) => \counterR_reg[7]_i_1_n_2\,
      CO(0) => \counterR_reg[7]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \counterR_reg[7]_i_1_n_4\,
      O(2) => \counterR_reg[7]_i_1_n_5\,
      O(1) => \counterR_reg[7]_i_1_n_6\,
      O(0) => \counterR_reg[7]_i_1_n_7\,
      S(3 downto 0) => \^m00_axis_tdata\(7 downto 4)
    );
\counterR_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[11]_i_1_n_7\,
      Q => \^m00_axis_tdata\(8),
      R => clear
    );
\counterR_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => \^tvalidr_reg_0\,
      D => \counterR_reg[11]_i_1_n_6\,
      Q => \^m00_axis_tdata\(9),
      R => clear
    );
sampleGeneratorEnR_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFF00000001"
    )
        port map (
      I0 => sampleGeneratorEnR_i_2_n_0,
      I1 => afterResetCycleCounterR_reg(7),
      I2 => afterResetCycleCounterR_reg(6),
      I3 => afterResetCycleCounterR_reg(4),
      I4 => afterResetCycleCounterR_reg(0),
      I5 => sampleGeneratorEnR,
      O => sampleGeneratorEnR_i_1_n_0
    );
sampleGeneratorEnR_i_2: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFEF"
    )
        port map (
      I0 => afterResetCycleCounterR_reg(2),
      I1 => afterResetCycleCounterR_reg(3),
      I2 => afterResetCycleCounterR_reg(5),
      I3 => afterResetCycleCounterR_reg(1),
      O => sampleGeneratorEnR_i_2_n_0
    );
sampleGeneratorEnR_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => sampleGeneratorEnR_i_1_n_0,
      Q => sampleGeneratorEnR,
      R => clear
    );
tValidR_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => sampleGeneratorEnR,
      I1 => \^tvalidr_reg_0\,
      O => tValidR_i_1_n_0
    );
tValidR_reg: unisim.vcomponents.FDRE
     port map (
      C => m00_axis_aclk,
      CE => '1',
      D => tValidR_i_1_n_0,
      Q => \^tvalidr_reg_0\,
      R => clear
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0 is
begin
samplGeneraror_v1_0_M00_AXIS_inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGeneraror_v1_0_M00_AXIS
     port map (
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      tValidR_reg_0 => m00_axis_tvalid
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    m00_axis_tdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    m00_axis_tstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
    m00_axis_tlast : out STD_LOGIC;
    m00_axis_tvalid : out STD_LOGIC;
    m00_axis_tready : in STD_LOGIC;
    m00_axis_aclk : in STD_LOGIC;
    m00_axis_aresetn : in STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_sampleGenerator_0_0,sampleGenerator_v1_0,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "sampleGenerator_v1_0,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal \<const1>\ : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of m00_axis_aclk : signal is "xilinx.com:signal:clock:1.0 m00_axis_aclk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of m00_axis_aclk : signal is "XIL_INTERFACENAME m00_axis_aclk, ASSOCIATED_BUSIF M00_AXIS, ASSOCIATED_RESET m00_axis_aresetn, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_m00_axis_aclk_0, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_aresetn : signal is "xilinx.com:signal:reset:1.0 m00_axis_aresetn RST";
  attribute X_INTERFACE_PARAMETER of m00_axis_aresetn : signal is "XIL_INTERFACENAME m00_axis_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tlast : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TLAST";
  attribute X_INTERFACE_INFO of m00_axis_tready : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TREADY";
  attribute X_INTERFACE_PARAMETER of m00_axis_tready : signal is "XIL_INTERFACENAME M00_AXIS, WIZ_DATA_WIDTH 32, TDATA_NUM_BYTES 4, TDEST_WIDTH 0, TID_WIDTH 0, TUSER_WIDTH 0, HAS_TREADY 1, HAS_TSTRB 1, HAS_TKEEP 0, HAS_TLAST 1, FREQ_HZ 100000000, PHASE 0.000, CLK_DOMAIN design_1_m00_axis_aclk_0, LAYERED_METADATA undef, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of m00_axis_tvalid : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TVALID";
  attribute X_INTERFACE_INFO of m00_axis_tdata : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TDATA";
  attribute X_INTERFACE_INFO of m00_axis_tstrb : signal is "xilinx.com:interface:axis:1.0 M00_AXIS TSTRB";
begin
  m00_axis_tlast <= \<const0>\;
  m00_axis_tstrb(3) <= \<const1>\;
  m00_axis_tstrb(2) <= \<const1>\;
  m00_axis_tstrb(1) <= \<const1>\;
  m00_axis_tstrb(0) <= \<const1>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
VCC: unisim.vcomponents.VCC
     port map (
      P => \<const1>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sampleGenerator_v1_0
     port map (
      m00_axis_aclk => m00_axis_aclk,
      m00_axis_aresetn => m00_axis_aresetn,
      m00_axis_tdata(31 downto 0) => m00_axis_tdata(31 downto 0),
      m00_axis_tvalid => m00_axis_tvalid
    );
end STRUCTURE;
