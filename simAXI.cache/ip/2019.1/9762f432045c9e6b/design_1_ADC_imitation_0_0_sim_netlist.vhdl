-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Wed Jun  3 21:24:31 2020
-- Host        : DESKTOPAEV67KM running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ADC_imitation_0_0_sim_netlist.vhdl
-- Design      : design_1_ADC_imitation_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tfgg484-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  port (
    data_adc_deserial : out STD_LOGIC_VECTOR ( 31 downto 0 );
    clk_5MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation is
  signal \count_clk[0]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_10_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_11_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_3_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_4_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_5_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_6_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_7_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_8_n_0\ : STD_LOGIC;
  signal \count_clk[31]_i_9_n_0\ : STD_LOGIC;
  signal \count_clk_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[16]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[16]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[20]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[20]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[20]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[20]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[24]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[24]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[24]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[24]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[28]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[28]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[28]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[28]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_2_n_2\ : STD_LOGIC;
  signal \count_clk_reg[31]_i_2_n_3\ : STD_LOGIC;
  signal \count_clk_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \count_clk_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \count_clk_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \count_clk_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \count_clk_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \^data_adc_deserial\ : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_count_clk_reg[31]_i_2_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \count_clk[0]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \count_clk[31]_i_5\ : label is "soft_lutpair0";
begin
  data_adc_deserial(31 downto 0) <= \^data_adc_deserial\(31 downto 0);
\count_clk[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \^data_adc_deserial\(0),
      O => \count_clk[0]_i_1_n_0\
    );
\count_clk[31]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \count_clk[31]_i_4_n_0\,
      I1 => \count_clk[31]_i_5_n_0\,
      I2 => \count_clk[31]_i_6_n_0\,
      I3 => \count_clk[31]_i_7_n_0\,
      O => \count_clk[31]_i_1_n_0\
    );
\count_clk[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^data_adc_deserial\(28),
      I1 => \^data_adc_deserial\(27),
      I2 => \^data_adc_deserial\(31),
      I3 => \^data_adc_deserial\(29),
      O => \count_clk[31]_i_10_n_0\
    );
\count_clk[31]_i_11\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^data_adc_deserial\(10),
      I1 => \^data_adc_deserial\(11),
      I2 => \^data_adc_deserial\(8),
      I3 => \^data_adc_deserial\(9),
      O => \count_clk[31]_i_11_n_0\
    );
\count_clk[31]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => clk_5MHz,
      I1 => s00_axi_aresetn,
      O => \count_clk[31]_i_3_n_0\
    );
\count_clk[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^data_adc_deserial\(21),
      I1 => \^data_adc_deserial\(20),
      I2 => \^data_adc_deserial\(23),
      I3 => \^data_adc_deserial\(22),
      I4 => \count_clk[31]_i_8_n_0\,
      O => \count_clk[31]_i_4_n_0\
    );
\count_clk[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF7FFF"
    )
        port map (
      I0 => \^data_adc_deserial\(1),
      I1 => \^data_adc_deserial\(2),
      I2 => \^data_adc_deserial\(4),
      I3 => \^data_adc_deserial\(0),
      I4 => \count_clk[31]_i_9_n_0\,
      O => \count_clk[31]_i_5_n_0\
    );
\count_clk[31]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^data_adc_deserial\(5),
      I1 => \^data_adc_deserial\(26),
      I2 => \^data_adc_deserial\(7),
      I3 => \^data_adc_deserial\(6),
      I4 => \count_clk[31]_i_10_n_0\,
      O => \count_clk[31]_i_6_n_0\
    );
\count_clk[31]_i_7\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \^data_adc_deserial\(13),
      I1 => \^data_adc_deserial\(12),
      I2 => \^data_adc_deserial\(15),
      I3 => \^data_adc_deserial\(14),
      I4 => \count_clk[31]_i_11_n_0\,
      O => \count_clk[31]_i_7_n_0\
    );
\count_clk[31]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => \^data_adc_deserial\(18),
      I1 => \^data_adc_deserial\(19),
      I2 => \^data_adc_deserial\(16),
      I3 => \^data_adc_deserial\(17),
      O => \count_clk[31]_i_8_n_0\
    );
\count_clk[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => \^data_adc_deserial\(3),
      I1 => \^data_adc_deserial\(30),
      I2 => \^data_adc_deserial\(24),
      I3 => \^data_adc_deserial\(25),
      O => \count_clk[31]_i_9_n_0\
    );
\count_clk_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => \count_clk[0]_i_1_n_0\,
      Q => \^data_adc_deserial\(0),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[10]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(10),
      Q => \^data_adc_deserial\(10),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[11]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(11),
      Q => \^data_adc_deserial\(11),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[12]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(12),
      Q => \^data_adc_deserial\(12),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[8]_i_1_n_0\,
      CO(3) => \count_clk_reg[12]_i_1_n_0\,
      CO(2) => \count_clk_reg[12]_i_1_n_1\,
      CO(1) => \count_clk_reg[12]_i_1_n_2\,
      CO(0) => \count_clk_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => \^data_adc_deserial\(12 downto 9)
    );
\count_clk_reg[13]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(13),
      Q => \^data_adc_deserial\(13),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[14]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(14),
      Q => \^data_adc_deserial\(14),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[15]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(15),
      Q => \^data_adc_deserial\(15),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[16]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(16),
      Q => \^data_adc_deserial\(16),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[12]_i_1_n_0\,
      CO(3) => \count_clk_reg[16]_i_1_n_0\,
      CO(2) => \count_clk_reg[16]_i_1_n_1\,
      CO(1) => \count_clk_reg[16]_i_1_n_2\,
      CO(0) => \count_clk_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3 downto 0) => \^data_adc_deserial\(16 downto 13)
    );
\count_clk_reg[17]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(17),
      Q => \^data_adc_deserial\(17),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[18]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(18),
      Q => \^data_adc_deserial\(18),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[19]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(19),
      Q => \^data_adc_deserial\(19),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(1),
      Q => \^data_adc_deserial\(1),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[20]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(20),
      Q => \^data_adc_deserial\(20),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[20]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[16]_i_1_n_0\,
      CO(3) => \count_clk_reg[20]_i_1_n_0\,
      CO(2) => \count_clk_reg[20]_i_1_n_1\,
      CO(1) => \count_clk_reg[20]_i_1_n_2\,
      CO(0) => \count_clk_reg[20]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(20 downto 17),
      S(3 downto 0) => \^data_adc_deserial\(20 downto 17)
    );
\count_clk_reg[21]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(21),
      Q => \^data_adc_deserial\(21),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[22]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(22),
      Q => \^data_adc_deserial\(22),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[23]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(23),
      Q => \^data_adc_deserial\(23),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[24]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(24),
      Q => \^data_adc_deserial\(24),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[24]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[20]_i_1_n_0\,
      CO(3) => \count_clk_reg[24]_i_1_n_0\,
      CO(2) => \count_clk_reg[24]_i_1_n_1\,
      CO(1) => \count_clk_reg[24]_i_1_n_2\,
      CO(0) => \count_clk_reg[24]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(24 downto 21),
      S(3 downto 0) => \^data_adc_deserial\(24 downto 21)
    );
\count_clk_reg[25]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(25),
      Q => \^data_adc_deserial\(25),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[26]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(26),
      Q => \^data_adc_deserial\(26),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[27]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(27),
      Q => \^data_adc_deserial\(27),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[28]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(28),
      Q => \^data_adc_deserial\(28),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[28]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[24]_i_1_n_0\,
      CO(3) => \count_clk_reg[28]_i_1_n_0\,
      CO(2) => \count_clk_reg[28]_i_1_n_1\,
      CO(1) => \count_clk_reg[28]_i_1_n_2\,
      CO(0) => \count_clk_reg[28]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(28 downto 25),
      S(3 downto 0) => \^data_adc_deserial\(28 downto 25)
    );
\count_clk_reg[29]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(29),
      Q => \^data_adc_deserial\(29),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(2),
      Q => \^data_adc_deserial\(2),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[30]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(30),
      Q => \^data_adc_deserial\(30),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[31]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(31),
      Q => \^data_adc_deserial\(31),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[31]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[28]_i_1_n_0\,
      CO(3 downto 2) => \NLW_count_clk_reg[31]_i_2_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \count_clk_reg[31]_i_2_n_2\,
      CO(0) => \count_clk_reg[31]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_count_clk_reg[31]_i_2_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => \^data_adc_deserial\(31 downto 29)
    );
\count_clk_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(3),
      Q => \^data_adc_deserial\(3),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(4),
      Q => \^data_adc_deserial\(4),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \count_clk_reg[4]_i_1_n_0\,
      CO(2) => \count_clk_reg[4]_i_1_n_1\,
      CO(1) => \count_clk_reg[4]_i_1_n_2\,
      CO(0) => \count_clk_reg[4]_i_1_n_3\,
      CYINIT => \^data_adc_deserial\(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => \^data_adc_deserial\(4 downto 1)
    );
\count_clk_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(5),
      Q => \^data_adc_deserial\(5),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(6),
      Q => \^data_adc_deserial\(6),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[7]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(7),
      Q => \^data_adc_deserial\(7),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[8]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(8),
      Q => \^data_adc_deserial\(8),
      R => \count_clk[31]_i_1_n_0\
    );
\count_clk_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \count_clk_reg[4]_i_1_n_0\,
      CO(3) => \count_clk_reg[8]_i_1_n_0\,
      CO(2) => \count_clk_reg[8]_i_1_n_1\,
      CO(1) => \count_clk_reg[8]_i_1_n_2\,
      CO(0) => \count_clk_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => \^data_adc_deserial\(8 downto 5)
    );
\count_clk_reg[9]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => \count_clk[31]_i_3_n_0\,
      CE => '1',
      D => data0(9),
      Q => \^data_adc_deserial\(9),
      R => \count_clk[31]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk_5MHz : in STD_LOGIC;
    s00_axi_aresetn : in STD_LOGIC;
    data_adc_deserial : out STD_LOGIC_VECTOR ( 31 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ADC_imitation_0_0,ADC_imitation,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ADC_imitation,Vivado 2019.1";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
begin
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ADC_imitation
     port map (
      clk_5MHz => clk_5MHz,
      data_adc_deserial(31 downto 0) => data_adc_deserial(31 downto 0),
      s00_axi_aresetn => s00_axi_aresetn
    );
end STRUCTURE;
