// (c) Copyright 1995-2020 Xilinx, Inc. All rights reserved.
// 
// This file contains confidential and proprietary information
// of Xilinx, Inc. and is protected under U.S. and
// international copyright and other intellectual property
// laws.
// 
// DISCLAIMER
// This disclaimer is not a license and does not grant any
// rights to the materials distributed herewith. Except as
// otherwise provided in a valid license issued to you by
// Xilinx, and to the maximum extent permitted by applicable
// law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
// WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
// AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
// BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
// INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
// (2) Xilinx shall not be liable (whether in contract or tort,
// including negligence, or under any other theory of
// liability) for any loss or damage of any kind or nature
// related to, arising under or in connection with these
// materials, including for any direct, or any indirect,
// special, incidental, or consequential loss or damage
// (including loss of data, profits, goodwill, or any type of
// loss or damage suffered as a result of any action brought
// by a third party) even if such damage or loss was
// reasonably foreseeable or Xilinx had been advised of the
// possibility of the same.
// 
// CRITICAL APPLICATIONS
// Xilinx products are not designed or intended to be fail-
// safe, or for use in any application requiring fail-safe
// performance, such as life-support or safety devices or
// systems, Class III medical devices, nuclear facilities,
// applications related to the deployment of airbags, or any
// other applications that could lead to death, personal
// injury, or severe property or environmental damage
// (individually and collectively, "Critical
// Applications"). Customer assumes the sole risk and
// liability of any use of Xilinx products in Critical
// Applications, subject only to applicable laws and
// regulations governing limitations on product liability.
// 
// THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
// PART OF THIS FILE AT ALL TIMES.
// 
// DO NOT MODIFY THIS FILE.


// IP VLNV: xilinx.com:user:ADC_imitation:1.0
// IP Revision: 80

(* X_CORE_INFO = "ADC_imitation,Vivado 2019.1" *)
(* CHECK_LICENSE_TYPE = "design_1_ADC_imitation_0_0,ADC_imitation,{}" *)
(* CORE_GENERATION_INFO = "design_1_ADC_imitation_0_0,ADC_imitation,{x_ipProduct=Vivado 2019.1,x_ipVendor=xilinx.com,x_ipLibrary=user,x_ipName=ADC_imitation,x_ipVersion=1.0,x_ipCoreRevision=80,x_ipLanguage=VERILOG,x_ipSimLanguage=MIXED}" *)
(* DowngradeIPIdentifiedWarnings = "yes" *)
module design_1_ADC_imitation_0_0 (
  clk_5MHz,
  clk_100MHz,
  s00_axi_aresetn,
  last_word_transmit,
  adr_read_ram,
  FrameSize,
  clk_5MHzWR_100MhzRD,
  clk_100Mhz_AXI_send,
  adr_bram,
  data,
  write
);

input wire clk_5MHz;
input wire clk_100MHz;
(* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME s00_axi_aresetn, POLARITY ACTIVE_LOW, INSERT_VIP 0" *)
(* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 s00_axi_aresetn RST" *)
input wire s00_axi_aresetn;
input wire last_word_transmit;
input wire [31 : 0] adr_read_ram;
input wire [15 : 0] FrameSize;
output wire clk_5MHzWR_100MhzRD;
output wire clk_100Mhz_AXI_send;
output wire [31 : 0] adr_bram;
output wire [31 : 0] data;
output wire write;

  ADC_imitation inst (
    .clk_5MHz(clk_5MHz),
    .clk_100MHz(clk_100MHz),
    .s00_axi_aresetn(s00_axi_aresetn),
    .last_word_transmit(last_word_transmit),
    .adr_read_ram(adr_read_ram),
    .FrameSize(FrameSize),
    .clk_5MHzWR_100MhzRD(clk_5MHzWR_100MhzRD),
    .clk_100Mhz_AXI_send(clk_100Mhz_AXI_send),
    .adr_bram(adr_bram),
    .data(data),
    .write(write)
  );
endmodule
