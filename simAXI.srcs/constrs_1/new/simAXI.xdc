set_property PACKAGE_PIN V4 [get_ports m00_axis_aclk]
set_property IOSTANDARD LVCMOS33 [get_ports m00_axis_aclk]

set_property PACKAGE_PIN V17 [get_ports uart_rtl_0_txd]
set_property PACKAGE_PIN U17 [get_ports uart_rtl_0_rxd]
set_property IOSTANDARD LVCMOS33 [get_ports uart_rtl_0_rxd]
set_property IOSTANDARD LVCMOS33 [get_ports uart_rtl_0_txd]

set_property PACKAGE_PIN A13 [get_ports {PAMP_EN[0]}]
set_property IOSTANDARD LVCMOS33 [get_ports {PAMP_EN[0]}]
